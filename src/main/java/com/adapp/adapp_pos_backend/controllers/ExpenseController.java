package com.adapp.adapp_pos_backend.controllers;

import com.adapp.adapp_pos_backend.dao.BaseDao;
import com.adapp.adapp_pos_backend.services.ExpenseService;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.expenses.Expense;
import com.adapp.adapp_pos_models.utils.SecurityCheckUtility;

import com.adapp.security.models.Permission;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.adapp.adapp_pos_models.enums.Resource.EXPENSES;
import static com.adapp.security.models.Permission.Action.UPDATE;
import static javax.ws.rs.core.Response.Status.*;

@Path("expenses")
@Produces(MediaType.APPLICATION_JSON)
@Component
public class ExpenseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExpenseController.class);

    ExpenseService expenseService;

    @Autowired
    public ExpenseController(ExpenseService expenseService) {
        this.expenseService = expenseService;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllExpenses() {
        try {
            SecurityCheckUtility.checkPermission("*", "EXPENSES:READ");
            return Response.ok(expenseService.getAllExpenses()).build();
        } catch (UnauthorizedException unauthorizedException) {
            return Response
                    .status((Response.Status.UNAUTHORIZED))
                    .entity(unauthorizedException.getMessage())
                    .build();
        } catch (Exception e) {
            throw new ServerException(e.getMessage(), e);
        }
    }

    @GET
    @Path("expense")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getExpense(@QueryParam("id") String id, @DefaultValue("*") @QueryParam("branch-id") String branchId) {
        try {
            SecurityCheckUtility.checkPermission(branchId, "EXPENSES:READ");
            Expense expense = expenseService.getExpense(id);
            if (expense == null) {
                return Response.status(Response.Status.NOT_FOUND).entity(String.format("Expense with ObjectId:%s, Not Found", id)).build();
            }
            return Response.ok(expense).build();
        } catch (UnauthorizedException unauthorizedException) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ServerException(e.getMessage(), e);
        }
    }

    public Response save(Expense expense, @QueryParam("branch-Id") String branchId) {
        try {
            SecurityCheckUtility.checkPermission(branchId, "EXPENSE:READ");
            expenseService.save(expense);
            return Response.ok().build();
        } catch (UnauthorizedException unauthorizedException) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        } catch (BadRequestException badRequestException) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ServerException(e.getMessage(), e);
        }
    }

    @POST
    @Path("query")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getExpensesBy(@QueryParam("branch-id") String branchId, BaseDao.QueryParameters queryParameters) {
        try {
            SecurityCheckUtility.checkPermission(branchId, "EXPENSE:READ");
            return Response.ok(expenseService.getExpensesBy(queryParameters)).build();
        } catch (UnauthorizedException unauthorizedException) {
            return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ServerException(e.getMessage(), e);
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(Expense expense) {
        try {
            if (expense.getId() == null) {
                return Response.status(BAD_REQUEST).entity("Cannot update expense with null ID").build();
            }
            if (expense.getAmount() == null) {
                return Response.status(BAD_REQUEST).entity("Cannot update expense with null Amount").build();
            }
            SecurityCheckUtility
                    .checkPermission(
                            expense.getExpenseId(),
                            String.format("%s:%s", EXPENSES, UPDATE));
            expenseService.update(expense);
            return Response.ok().build();
        } catch (BadRequestException badRequestException) {
            return Response.status(Response.Status.BAD_REQUEST).entity(badRequestException.getMessage()).build();
        } catch (UnauthorizedException unauthorizedException) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
        } catch (Exception e) {
            throw new ServerException(e.getMessage());
        }
    }

    @DELETE
    @Path("{expense-id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("expense-id") @NotNull String id, @QueryParam("branch-id") @DefaultValue("*") String branchId) {
        try {
            SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", EXPENSES, Permission.Action.DELETE));
            expenseService.remove(id);
            return Response.ok().build();
        } catch (UnauthorizedException unauthorizedException) {
            return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
        } catch (NotFoundException notFoundException) {
            return Response.status(NOT_FOUND).entity(notFoundException.getMessage()).build();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ServerException(e.getMessage(), e);
        }
    }
}