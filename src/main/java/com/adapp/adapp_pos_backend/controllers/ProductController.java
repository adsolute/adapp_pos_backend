package com.adapp.adapp_pos_backend.controllers;

import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.services.ProductService;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.products.Product;
import com.adapp.adapp_pos_models.utils.SecurityCheckUtility;
import com.adapp.security.models.Permission.Action;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.adapp.adapp_pos_models.enums.Resource.PRODUCTS;
import static com.adapp.security.models.Permission.Action.CREATE;
import static com.adapp.security.models.Permission.Action.UPDATE;
import static javax.ws.rs.core.Response.Status.*;

@Path("products")
@Produces(MediaType.APPLICATION_JSON)
@Component
public class ProductController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

	ProductService productService;

	@Autowired
	public ProductController(ProductService productService) {
		this.productService = productService;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllProducts() {
		try {
			return Response.ok(productService.getAllProducts()).build();
		}catch (Exception e){
			throw new ServerException(e.getMessage(), e);
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(@QueryParam("branch-id") @DefaultValue("*") String branchId, Product product) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", PRODUCTS, CREATE));
			productService.save(product);
			return Response.ok().build();
		} catch (UnauthorizedException unauthorizedException) {
			LOGGER.error(unauthorizedException.getMessage(), unauthorizedException);
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@QueryParam("branch-id") @DefaultValue("*") String branchId, Product product) {
		try {
			if (product.getId() == null) {
				return Response.status(BAD_REQUEST).entity("Cannot update product with null ID").build();
			}
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", PRODUCTS, UPDATE));
			productService.update(product);
			return Response.ok().build();
		} catch (UnauthorizedException unauthorizedException) {
			LOGGER.error(unauthorizedException.getMessage(), unauthorizedException);
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@GET
	@Path("{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProducts(@PathParam("name") String name) {
		try {
			return Response.ok(productService.getProducts(name)).build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@POST
	@Path("query")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProductsBy(@QueryParam("branch-id") String branchId, QueryParameters queryParameters) {
		try {
			SecurityCheckUtility.checkPermission(branchId, "PRODUCTS:READ");
			return Response.ok(productService.getProductsBy(queryParameters)).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@GET
	@Path("product")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProduct(@QueryParam("id") String id, @DefaultValue("*") @QueryParam("branch-id") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, "PRODUCTS:READ");
			Product product = productService.getProduct(id);
			if (product == null) {
				return Response.status(Response.Status.NOT_FOUND).entity(String.format("Product with ObjectId:%s, Not Found", id)).build();
			}

			return Response.ok(product).build();
		} catch (UnauthorizedException unauthorizedException) {
			LOGGER.error(unauthorizedException.getMessage(), unauthorizedException);
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@DELETE
	@Path("{product-id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("product-id") @NotNull String id, @QueryParam("branch-id") @DefaultValue("*") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", PRODUCTS, Action.DELETE));
			productService.remove(id);
			return Response.ok().build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (NotFoundException notFoundException) {
			return Response.status(NOT_FOUND).entity(notFoundException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}
}
