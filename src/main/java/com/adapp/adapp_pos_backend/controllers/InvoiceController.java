package com.adapp.adapp_pos_backend.controllers;

import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.services.InvoiceService;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.invoices.Invoice;
import com.adapp.adapp_pos_models.invoices.Invoice.DeliveryStatus;
import com.adapp.adapp_pos_models.invoices.Invoice.RiderDetails;
import com.adapp.adapp_pos_models.invoices.Invoice.TransactionType;
import com.adapp.adapp_pos_models.payments.PaymentDetails;
import com.adapp.adapp_pos_models.utils.SecurityCheckUtility;
import com.adapp.security.models.Permission;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.UnauthorizedException;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.adapp.adapp_pos_models.enums.Resource.*;
import static com.adapp.security.models.Permission.Action.READ;
import static com.adapp.security.models.Permission.Action.UPDATE;
import static javax.ws.rs.core.Response.Status.*;

@Path("invoices")
@Component
public class InvoiceController {

	private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceController.class);
	public static final String BRANCH_ID = "branchId";
	public static final String PERMISSION_FORMAT = "%s:%s";
	public static final String DELIVERY_STATUS = "deliveryStatus";

	InvoiceService invoiceService;

	@Autowired
	InvoiceController(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllInvoices() {
		try {
			SecurityCheckUtility.checkPermission("*", "INVOICES:READ");
			return Response.ok(invoiceService.getInvoiceDTOList()).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response
					.status(Response.Status.UNAUTHORIZED)
					.entity(unauthorizedException.getMessage())
					.build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("invoice")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInvoice(@QueryParam("id") String id, @DefaultValue("*") @QueryParam("branch-id") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format(PERMISSION_FORMAT, INVOICES, READ));
			Invoice invoice = invoiceService.getInvoice(id);
			if (invoice == null) {
				return Response.status(Response.Status.NOT_FOUND).entity(String.format("Invoice with ObjectId:%s, Not Found", id)).build();
			}
			SecurityCheckUtility.checkPermission(invoice.getBranchId(), String.format(PERMISSION_FORMAT, INVOICES, READ));
			return Response.ok(invoice).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response
					.status(Response.Status.UNAUTHORIZED)
					.entity(unauthorizedException.getMessage())
					.build();
		} catch (IllegalArgumentException illegalArgumentException) {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity(String.format("Bad Request, Illegal Invoice ObjectId:%s", id))
					.build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(Invoice invoice) {
		try {
			if (invoice.getBranchId() == null) {
				return Response.status(BAD_REQUEST).entity("Invoice must have a Branch Id").build();
			}
			SecurityCheckUtility.checkPermission(invoice.getBranchId(), "INVOICES:CREATE");
			invoiceService.save(invoice);
			return Response.ok().build();
		} catch (BadRequestException badRequestException) {
			return Response.status(BAD_REQUEST).entity(badRequestException.getMessage()).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(Response.Status.UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(Invoice invoice) {
		try {
			if (invoice.getBranchId() == null) {
				return Response.status(BAD_REQUEST).entity("Invoice must have a Branch Id").build();
			}
			if (invoice.getId() == null) {
				return Response.status(BAD_REQUEST).entity("Cannot update invoice with null ID").build();
			}
			SecurityCheckUtility
					.checkPermission(
							invoice.getBranchId(),
							String.format(PERMISSION_FORMAT, INVOICES, UPDATE));
			invoiceService.checkInvoiceIsPastDeliveryDateBeforeUpdating(invoice.getId());
			invoiceService.update(invoice);
			return Response.ok().build();
		} catch (NotAcceptableException notAcceptableException) {
			return Response.status(NOT_ACCEPTABLE).entity(notAcceptableException.getMessage()).build();
		} catch (BadRequestException badRequestException) {
			return Response.status(BAD_REQUEST).entity(badRequestException.getMessage()).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(Response.Status.UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("query")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInvoiceDTOListBy(@QueryParam("branch-id") String branchId, QueryParameters queryParameters) {
		try {
			SecurityCheckUtility.checkPermission(branchId, "INVOICES:READ");
			if (!"*".equals(branchId)) {
				queryParameters.getCriteria().put(BRANCH_ID, branchId);
			}
			return Response.ok(invoiceService.getInvoiceDTOListBy(queryParameters)).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(Response.Status.UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("schedules")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInvoiceSchedules(@QueryParam("branch-id") String branchId, QueryParameters queryParameters) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format(PERMISSION_FORMAT, SCHEDULES, READ));
			if (!"*".equals(branchId)) {
				queryParameters.getCriteria().put(BRANCH_ID, branchId);
			}
			queryParameters.getCriteria().put("transactionType", TransactionType.DELIVERY);
			return Response.ok(invoiceService.getInvoiceSchedules(queryParameters)).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(Response.Status.UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("query/queue")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInvoiceQueue(@QueryParam("branch-id") String branchId, QueryParameters queryParameters) {
		try {
			if(queryParameters.getCriteria().get(DELIVERY_STATUS) == null) {
				return Response.status(BAD_REQUEST).entity("Delivery Status Criteria cannot be null").build();
			}

			if (queryParameters.getCriteria().get(DELIVERY_STATUS).equals(DeliveryStatus.PROCESSING.toString())) {
				SecurityCheckUtility.checkPermission(branchId, String.format(PERMISSION_FORMAT, PROCESSING_QUEUE, READ));
			} else if (queryParameters.getCriteria().get(DELIVERY_STATUS).equals(DeliveryStatus.FOR_PICK_UP.toString())) {
				SecurityCheckUtility.checkPermission(branchId, String.format(PERMISSION_FORMAT, PICKUP_QUEUE, READ));
			} else {
				return Response.status(NOT_FOUND).entity("Delivery Status not found").build();
			}

			if (!"*".equals(branchId)) {
				queryParameters.getCriteria().put(BRANCH_ID, branchId);
			}
			return Response.ok(invoiceService.getInvoiceQueue(queryParameters)).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(Response.Status.UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("queue/{id}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateInvoiceQueue(@PathParam("id") String id, @QueryParam("delivery-status") DeliveryStatus deliveryStatus) {
		try {
			Invoice invoice = invoiceService.getInvoice(id);
			if (invoice.getBranchId() == null) {
				return Response.status(BAD_REQUEST).entity(String.format("Retrieved Invoice with Invoice Id %s, is not assigned to any branch", invoice.getInvoiceId())).build();
			}
			SecurityCheckUtility
					.checkPermission(
							invoice.getBranchId(),
							String.format(PERMISSION_FORMAT, PROCESSING_QUEUE, UPDATE));
			invoice.setDeliveryStatus(deliveryStatus);
			invoiceService.update(invoice);
			return Response.ok().build();
		} catch (NotFoundException notFoundException) {
			return Response.status(NOT_FOUND).entity(notFoundException.getMessage()).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(Response.Status.UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("{id}/delivery-status/{status}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateDeliveryStatus(
			@PathParam("id") String id,
			@PathParam("status") DeliveryStatus deliveryStatus) {
		try {
			Invoice invoice = invoiceService.getInvoice(id);
			if (invoice.getBranchId() == null) {
				return Response.status(BAD_REQUEST).entity(String.format("Retrieved Invoice with Invoice Id %s, is not assigned to any branch", invoice.getInvoiceId())).build();
			}
			SecurityCheckUtility
					.checkPermission(
							invoice.getBranchId(),
							String.format(PERMISSION_FORMAT, Resource.DELIVERY_STATUS, UPDATE));
			invoice.setDeliveryStatus(deliveryStatus);
			invoiceService.update(invoice);
			return Response.ok().build();
		} catch (NotFoundException notFoundException) {
			return Response.status(NOT_FOUND).entity(notFoundException.getMessage()).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(Response.Status.UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@DELETE
	@Path("{invoice-id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("invoice-id") @NotNull String id, @QueryParam("branch-id") @DefaultValue("*") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format(PERMISSION_FORMAT, INVOICES, Permission.Action.DELETE));
			invoiceService.remove(id);
			return Response.ok().build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (NotFoundException notFoundException) {
			return Response.status(NOT_FOUND).entity(notFoundException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@PUT
	@Path("void/{invoice-id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response voidInvoice(@PathParam("invoice-id") @NotNull String id, @QueryParam("branch-id") @DefaultValue("*") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format(PERMISSION_FORMAT, INVOICES_VOID, UPDATE));
			invoiceService.voidInvoice(id);
			return Response.ok().build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (NotFoundException notFoundException) {
			return Response.status(NOT_FOUND).entity(notFoundException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@PUT
	@Path("payment-details/{invoice-id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePaymentDetails(@QueryParam("branch-id") String branchId,
										 @PathParam("invoice-id") String invoiceId,
										 PaymentDetails paymentDetails) {

		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", PAYMENT_DETAILS, UPDATE));
			invoiceService.updatePaymentDetails(invoiceId, paymentDetails);
			return Response.ok().build();
		} catch(UnauthorizedException unauthorizedException) {
			return Response.status(401).entity(unauthorizedException.getMessage()).build();
		} catch(BadRequestException badRequestException) {
			return Response.status(400).entity(badRequestException.getMessage()).build();
		} catch(Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@PUT
	@Path("rider-details/{invoice-id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateRiderDetails(@QueryParam("branch-id") String branchId,
	                                     @PathParam("invoice-id") String invoiceId,
	                                     RiderDetails riderDetails) {

		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", RIDER_DETAILS, UPDATE));
			invoiceService.updateRiderDetails(invoiceId, riderDetails);
			return Response.ok().build();
		} catch(UnauthorizedException unauthorizedException) {
			return Response.status(401).entity(unauthorizedException.getMessage()).build();
		} catch(BadRequestException badRequestException) {
			return Response.status(400).entity(badRequestException.getMessage()).build();
		} catch(Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@GET
	@Path("deliveries/dashboard")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDeliveriesDashboard(@QueryParam("branch-id") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", INVOICES, READ));
			return Response.ok(invoiceService.getInvoiceDeliveriesForDashboard(branchId)).build();
		} catch(UnauthorizedException unauthorizedException) {
			return Response.status(401).entity(unauthorizedException.getMessage()).build();
		} catch(Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}
}
