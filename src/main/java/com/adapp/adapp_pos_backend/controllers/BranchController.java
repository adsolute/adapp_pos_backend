package com.adapp.adapp_pos_backend.controllers;

import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.services.BranchService;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.profiles.Branch;
import com.adapp.adapp_pos_models.utils.SecurityCheckUtility;
import com.adapp.security.models.Permission;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.adapp.adapp_pos_models.enums.Resource.BRANCHES;
import static com.adapp.security.models.Permission.Action.READ;
import static com.adapp.security.models.Permission.Action.UPDATE;
import static javax.ws.rs.core.Response.Status.*;

@Path("branches")
@Produces(MediaType.APPLICATION_JSON)
@Component
public class BranchController {

	private static final Logger LOGGER = LoggerFactory.getLogger(BranchController.class);

	BranchService branchService;

	@Autowired
	public BranchController(BranchService branchService) {
		this.branchService = branchService;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllBranches() {
		try {
			return Response.ok(branchService.getAllBranches()).build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(Branch branch) {
		try {
			branchService.save(branch);
			return Response.ok().build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("query")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBranchDTOPagedResultSetBy(@QueryParam("branch-id") String branchId, QueryParameters queryParameters) {
		try {
			SecurityCheckUtility.checkPermission(branchId, "BRANCHES:READ");
			return Response.ok(branchService.getBranchDTOPagedResultSetBy(queryParameters)).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("list")
	@POST
	public Response getBranchDTOList(@QueryParam("by-field") String field, List<String> inList) {
		try {
			QueryParameters queryParameters = new QueryParameters();
			queryParameters.getCustomCriteria().add(Criteria.where(field).in(inList));
			return Response.ok(branchService.getBranchDTOList(queryParameters)).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@GET
	@Path("branch")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBranch(@QueryParam("id") String id, @DefaultValue("*") @QueryParam("branch-id") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", BRANCHES, READ));
			Branch branch = branchService.getBranch(id);
			if (branch == null) {
				return Response.status(Response.Status.NOT_FOUND).entity(String.format("Branch with ObjectId:%s, Not Found", id)).build();
			}
			return Response.ok(branch).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response
					.status(Response.Status.UNAUTHORIZED)
					.entity(unauthorizedException.getMessage())
					.build();
		} catch (IllegalArgumentException illegalArgumentException) {
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity(String.format("Bad Request, Illegal Branch ObjectId:%s", id))
					.build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@GET
	@Path("{branch-id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBranch(@PathParam("branch-id") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", BRANCHES, READ));
			Branch branch = branchService.getBranchByBranchId(branchId);
			if (branch == null) {
				return Response.status(Response.Status.NOT_FOUND).entity(String.format("Branch with Branch Id:%s, Not Found", branchId)).build();
			}
			return Response.ok(branch).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response
					.status(Response.Status.UNAUTHORIZED)
					.entity(unauthorizedException.getMessage())
					.build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@QueryParam("branch-id") @DefaultValue("*") String branchId, Branch branch) {
		try {
			if (branch.getId() == null) {
				return Response.status(BAD_REQUEST).entity("Cannot update branch with null ID").build();
			}
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", BRANCHES, UPDATE));
			branchService.update(branch);
			return Response.ok().build();
		} catch (UnauthorizedException unauthorizedException) {
			LOGGER.error(unauthorizedException.getMessage(), unauthorizedException);
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@DELETE
	@Path("{branch-id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("branch-id") @NotNull String id, @QueryParam("branch-id") @DefaultValue("*") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", BRANCHES, Permission.Action.DELETE));
			branchService.remove(id);
			return Response.ok().build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (NotFoundException notFoundException) {
			return Response.status(NOT_FOUND).entity(notFoundException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}
}