package com.adapp.adapp_pos_backend.controllers;

import com.adapp.adapp_pos_backend.dao.BaseDao;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.services.ReportService;
import com.adapp.adapp_pos_models.enums.DocType;
import com.adapp.adapp_pos_models.enums.ReportType;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.reports.ReportRequest;
import com.adapp.adapp_pos_models.utils.SecurityCheckUtility;
import com.adapp.security.models.Permission;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.FileNotFoundException;
import java.net.URI;

import static com.adapp.adapp_pos_models.enums.Resource.REPORTS;
import static com.adapp.security.models.Permission.Action.CREATE;
import static com.adapp.security.models.Permission.Action.READ;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

@Path("reports")
@Produces(MediaType.APPLICATION_JSON)
@Component
public class ReportController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);

	ReportService reportService;

	@Autowired
	public ReportController(ReportService reportService) {
		this.reportService = reportService;
	}

	@Path("document")
	@POST
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response saveDocumentData(@Context UriInfo uriInfo, @QueryParam("report-type")ReportType reportType, ReportRequest reportRequest) {
		try {
			String documentDataId = reportService.saveDocumentData(reportRequest, reportType);
			URI uri = uriInfo.getBaseUriBuilder()
					.path(uriInfo.getPath())
					.path(documentDataId)
					.build();
			return Response.ok().location(uri).build();
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("document/{document-data-id}")
	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response generateReportDocument(@PathParam("document-data-id") String documentDataId, @QueryParam("doc-type") DocType docType) {
		try {
			byte[] reportByteArray = reportService.generateReportByteArray(documentDataId, docType);
			return Response.ok(
					(StreamingOutput) outputStream -> {
						try {
							outputStream.write(reportByteArray);
							outputStream.flush();
						} catch (Exception e) {
							throw new ServerException(e.getMessage(), e);
						} finally {
							try {
								outputStream.close();
							} catch (Exception e) {
								LOGGER.warn(e.getMessage(), e);
							}
						}
					}
			).build();
		} catch (NotFoundException notFoundException) {
			return Response.status(NOT_FOUND).entity("Document Template Not Found. Please contact the Admin").build();
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("document/daily-sales")
	@POST
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response saveDailySalesReportDocumentData(@Context UriInfo uriInfo, QueryParameters queryParameters, @QueryParam("branch-id") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", REPORTS, CREATE));
			String documentDataId = reportService.saveDailySalesReportDocumentData(queryParameters);
			URI uri = uriInfo.getBaseUriBuilder()
					.path("reports/document")
					.path(documentDataId)
					.build();
			return Response.ok().location(uri).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@Path("invoice-sales/query")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInvoiceSalesReport(QueryParameters queryParameters, @QueryParam("branch-id") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", REPORTS, READ));
			return Response.ok(reportService.getInvoiceSalesReport(queryParameters)).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}
}
