package com.adapp.adapp_pos_backend.controllers;

import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.services.CustomerService;
import com.adapp.adapp_pos_models.customers.Customer;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.utils.SecurityCheckUtility;
import com.adapp.security.models.Permission;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.adapp.adapp_pos_models.enums.Resource.CUSTOMERS;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

@Path("customers")
@Produces(MediaType.APPLICATION_JSON)
@Component
public class CustomerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

	CustomerService customerService;

	@Autowired
	public CustomerController(CustomerService customerService) {
		this.customerService = customerService;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCustomers() {
		try{
			return Response.ok(customerService.getAllCustomers()).build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(Customer customer) {
		try {
			customerService.save(customer);
			return Response.ok().build();
		}catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@GET
	@Path("{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomers(@PathParam("name") String name) {
		try {
			return Response.ok(customerService.getCustomers(name)).build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@GET
	@Path("customer")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomer(@QueryParam("id") String id,@DefaultValue("*") @QueryParam("branch-id") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, "CUSTOMERS:READ");
			Customer customer = customerService.getCustomer(id);
			if (customer == null) {
				return Response.status(Response.Status.NOT_FOUND).entity(String.format("Customer with ObjectId:%s, Not Found", id)).build();
			}
			return Response.ok(customer).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response
					.status(Response.Status.UNAUTHORIZED)
					.entity(unauthorizedException.getMessage())
					.build();
		} catch (IllegalArgumentException illegalArgumentException){
			return Response
					.status(Response.Status.BAD_REQUEST)
					.entity(String.format("Bad Request, Illegal Customer ObjectId:%s", id))
					.build();
		} catch (Exception e) {
			throw new ServerException(e.getMessage(), e);
		}
	}

	@POST
	@Path("query")
	public Response getCustomerDTOResultSetBy(@QueryParam("branch-id") String branchId, QueryParameters queryParameters) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", Resource.CUSTOMERS, Permission.Action.READ));
			return Response.ok(customerService.getCustomerDTOResultSetBy(queryParameters)).build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(Response.Status.UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}

	@DELETE
	@Path("{customers-id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("customers-id") @NotNull String id, @QueryParam("branch-id") @DefaultValue("*") String branchId) {
		try {
			SecurityCheckUtility.checkPermission(branchId, String.format("%s:%s", CUSTOMERS, Permission.Action.DELETE));
			customerService.remove(id);
			return Response.ok().build();
		} catch (UnauthorizedException unauthorizedException) {
			return Response.status(UNAUTHORIZED).entity(unauthorizedException.getMessage()).build();
		} catch (NotFoundException notFoundException) {
			return Response.status(NOT_FOUND).entity(notFoundException.getMessage()).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ServerException(e.getMessage(), e);
		}
	}
}
