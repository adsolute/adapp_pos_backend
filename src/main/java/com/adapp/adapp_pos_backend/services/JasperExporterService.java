package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_models.enums.DocType;
import com.adapp.adapp_pos_models.reports.DocumentData;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.stereotype.Service;

import javax.ws.rs.InternalServerErrorException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Arrays;

@Service
public class JasperExporterService {

	public void export(
			InputStream inputStream,
			ByteArrayOutputStream byteArrayOutputStream,
			DocumentData documentData,
			DocType docType) {
		try {
			JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint
					= JasperFillManager.fillReport(jasperReport, null, new JRBeanCollectionDataSource(Arrays.asList(documentData)));
			Exporter exporter = new JasperReportExporterFactory().getExporter(docType);
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArrayOutputStream));
			exporter.exportReport();
		} catch (JRException jrException) {
			throw new InternalServerErrorException(jrException.getMessage(), jrException);
		}
	}
}
