package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_backend.builders.ReportDataMapper;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.dao.DocumentDataDao;
import com.adapp.adapp_pos_backend.dao.InvoiceDao;
import com.adapp.adapp_pos_models.enums.DocType;
import com.adapp.adapp_pos_models.enums.ReportType;
import com.adapp.adapp_pos_models.invoices.Invoice;
import com.adapp.adapp_pos_models.reports.DocumentData;
import com.adapp.adapp_pos_models.reports.InvoiceSalesReport;
import com.adapp.adapp_pos_models.reports.ReportRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.io.*;
import java.util.List;
import java.util.UUID;

@Service
public class ReportService {

	@Autowired
	ReportDataMapper reportDataMapper;

	@Autowired
	DocumentDataDao documentDataDao;

	@Autowired
	JasperExporterService jasperExporterService;

	@Autowired
	InvoiceDao invoiceDao;

	public String saveDocumentData(ReportRequest reportRequest, ReportType reportType) {
		String documentDataId = UUID.randomUUID().toString();
		try {
			DocumentData documentData = reportDataMapper.map(reportRequest, reportType);
			documentData.setDocumentDataId(documentDataId);
			documentDataDao.save(documentData);
			return documentDataId;
		} catch (NullPointerException nullPointerException) {
			throw new BadRequestException("There are some problem with the data used for the document. Please contact the Admin");
		}
	}

	public byte[] generateReportByteArray(
			String documentDataId,
			DocType docType) {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		DocumentData documentData = documentDataDao.getByDocumentDataId(documentDataId);
		File jasperTemplate = new File(documentData.getTemplateFilePath());
		try {
			InputStream inputStream = new FileInputStream(jasperTemplate);
			jasperExporterService.export(inputStream, byteArrayOutputStream, documentData, docType);
		} catch (FileNotFoundException fileNotFoundException) {
			throw new NotFoundException();
		}
		return byteArrayOutputStream.toByteArray();
	}

	public String saveDailySalesReportDocumentData(QueryParameters queryParameters) {
		queryParameters.getCustomCriteria().add(Criteria.where("voided").ne(true));
		List<Invoice> invoices = invoiceDao.getInvoicesBy(queryParameters).getResults();
		ReportRequest reportRequest = new ReportRequest();
		reportRequest.setInvoicesForDailyReport(invoices);
		reportRequest.setOrderedProducts(invoiceDao.aggregateInvoiceByOrderedProducts(queryParameters));
		return saveDocumentData(reportRequest, ReportType.DAILY_SALES_REPORT);
	}

	public InvoiceSalesReport getInvoiceSalesReport(QueryParameters queryParameters) {
		queryParameters.getCustomCriteria().add(Criteria.where("voided").ne(true));
		List<InvoiceSalesReport> invoiceSalesReports = invoiceDao.aggregateInvoiceSalesReport(queryParameters);
		if (invoiceSalesReports.isEmpty()) {
			return new InvoiceSalesReport();
		}
		InvoiceSalesReport invoiceSalesReport = invoiceSalesReports.get(0);
		invoiceSalesReport.setTotalInvoiceByHours(invoiceDao.aggregateInvoiceByHourOrdered(queryParameters));
		invoiceDao.aggregateInvoiceByOrderedProducts(queryParameters);
		return invoiceSalesReport;
	}
}
