package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.dao.BranchDao;
import com.adapp.adapp_pos_backend.utils.BackendKafkaAuditSender;
import com.adapp.adapp_pos_models.profiles.Branch;
import com.adapp.adapp_pos_models.profiles.BranchDTO;
import com.adapp.adapp_pos_models.utils.IdGenerationUtility;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import java.util.List;

import static com.adapp.adapp_pos_models.enums.Resource.BRANCHES;
import static java.util.stream.Collectors.toList;

@Service
public class BranchService {

	public static final String BRANCH_NAME = "Branch Name";
	BranchDao branchDao;

	BackendKafkaAuditSender<Branch> backendKafkaAuditSender;

	@Autowired
	public BranchService(BranchDao branchDao, BackendKafkaAuditSender<Branch> backendKafkaAuditSender) {
		this.branchDao = branchDao;
		this.backendKafkaAuditSender = backendKafkaAuditSender;
	}

	public List<BranchDTO> getAllBranches() {
		List<Branch> branches = branchDao.getAllBranches();
		return branches
				.stream()
				.map(this::convertToDTO)
				.collect(toList());
	}

	private BranchDTO convertToDTO(Branch branch) {
		return new BranchDTO(
				branch.getId(),
				branch.getBranchId(),
				branch.getName(),
				branch.getAddress().getCity(),
				branch.getContactInformation().getEmailAddress(),
				branch.getContactInformation().getPhoneNumber(),
				branch.getCreateDate());
	}

	public void save(Branch branch) {
		branch.setCreateDate(new DateTime());
		branch.setBranchId(IdGenerationUtility.generateId(branchDao::exists));
		Branch savedBranch = branchDao.saveThenReturn(branch);
		backendKafkaAuditSender.send(BRANCHES, savedBranch.getId(), branch.getBranchId(), BRANCH_NAME, branch.getName());
	}

	public void update(Branch branch) {
		branch.setUpdateDate(new DateTime());
		Branch branchBeforeUpdate = branchDao.findAndReplace(branch);
		backendKafkaAuditSender.send(BRANCHES, branch.getId(), branch.getBranchId(), BRANCH_NAME, branch.getName(), branchBeforeUpdate, branch);
	}

	public PagedResultSet<BranchDTO> getBranchDTOPagedResultSetBy(QueryParameters queryParameters) {
		PagedResultSet<Branch> branchPagedResultSet = branchDao.getPagedResultSet(queryParameters);
		PagedResultSet<BranchDTO> branchDTOPagedResultSet = new PagedResultSet<>();
		branchDTOPagedResultSet.setPage(branchPagedResultSet.getPage());
		branchDTOPagedResultSet.setSize(branchPagedResultSet.getSize());
		branchDTOPagedResultSet.setTotalCount(branchPagedResultSet.getTotalCount());
		branchDTOPagedResultSet.setLimit(branchPagedResultSet.getLimit());
		branchDTOPagedResultSet.setResults(
				branchPagedResultSet.getResults()
						.stream()
						.map(this::convertToDTO)
						.collect(toList())
		);
		return branchDTOPagedResultSet;
	}

	public List<BranchDTO> getBranchDTOList(QueryParameters queryParameters) {
		List<Branch> branches = branchDao.getPagedResultSet(queryParameters).getResults();
		return branches
				.stream()
				.map(this::convertToDTO)
				.collect(toList());
	}

	public Branch getBranch(String id) {
		return branchDao.getBranch(id);
	}

	public Branch getBranchByBranchId(String branchId) {
		return branchDao.getBranchByBranchId(branchId);
	}

	public void remove(String id) {
		Branch branchToRemove = branchDao.getBranch(id);
		if (branchToRemove == null) {
			throw new NotFoundException(String.format("Cannot find branch to be removed with object id, %s", id));
		}
		branchDao.remove(branchToRemove);
		backendKafkaAuditSender.send(BRANCHES, branchToRemove.getBranchId(), BRANCH_NAME, branchToRemove.getName());
	}
}
