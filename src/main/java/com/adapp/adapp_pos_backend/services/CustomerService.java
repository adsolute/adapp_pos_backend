package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.dao.CustomerDao;
import com.adapp.adapp_pos_backend.utils.BackendKafkaAuditSender;
import com.adapp.adapp_pos_models.customers.Customer;
import com.adapp.adapp_pos_models.customers.CustomerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.stream.Collectors;

import static com.adapp.adapp_pos_models.enums.Resource.CUSTOMERS;

@Service
public class CustomerService {

	public static final String NAME = "Name";
	CustomerDao customerDao;

	BackendKafkaAuditSender<Customer> backendKafkaAuditSender;

	@Autowired
	public CustomerService(CustomerDao customerDao, BackendKafkaAuditSender<Customer> backendKafkaAuditSender) {
		this.customerDao = customerDao;
		this.backendKafkaAuditSender = backendKafkaAuditSender;
	}

	public List<CustomerDTO> getAllCustomers() {
		List<Customer> customers = customerDao.getAllCustomers();
		return customers.stream().map(this::convertCustomerToDTO).collect(Collectors.toList());
	}

	private CustomerDTO convertCustomerToDTO(Customer customer) {
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setName(customer.getName());
		customerDTO.setEmailAddress(customer.getContactInformation().getEmailAddress());
		customerDTO.setPhoneNumber(customer.getContactInformation().getPhoneNumber());
		customerDTO.setId(customer.getId());
		return customerDTO;
	}

	public void save(Customer customer) {
		if(customer.getId() == null) {
			Customer savedCustomer = customerDao.saveAndReturn(customer);
			this.backendKafkaAuditSender.send(CUSTOMERS, savedCustomer.getId(), null, NAME, savedCustomer.getName());
		} else {
			Customer customerBeforeUpdate = customerDao.findAndReplace(customer);
			this.backendKafkaAuditSender
					.send(CUSTOMERS, customerBeforeUpdate.getId(), null, NAME, customer.getName(), customerBeforeUpdate, customer);
		}
	}

	public List<Customer> getCustomers(String name) {
		return customerDao.getCustomers(name);
	}

	public PagedResultSet<CustomerDTO> getCustomerDTOResultSetBy(QueryParameters queryParameters) {
		PagedResultSet<Customer> customerPagedResultSet = customerDao.getCustomersBy(queryParameters);
		PagedResultSet<CustomerDTO> customerDTOPagedResultSet = new PagedResultSet<>();
		customerDTOPagedResultSet.setTotalCount(customerPagedResultSet.getTotalCount());
		customerDTOPagedResultSet.setSize(customerPagedResultSet.getSize());
		customerDTOPagedResultSet.setPage(customerPagedResultSet.getPage());
		customerDTOPagedResultSet.setLimit(customerPagedResultSet.getLimit());
		customerDTOPagedResultSet.setResults(
				customerPagedResultSet.getResults().stream().map(this::convertCustomerToDTO).collect(Collectors.toList())
		);
		return customerDTOPagedResultSet;
	}

	public Customer getCustomer(String id) {
		return customerDao.getCustomer(id);
	}

	public void remove(String id) {
		Customer customerToRemove = customerDao.getCustomer(id);
		if (customerToRemove == null) {
			throw new NotFoundException(String.format("Cannot find customer to be removed with object id, %s", id));
		}
		customerDao.remove(customerToRemove);
		backendKafkaAuditSender.send(CUSTOMERS, null, NAME, customerToRemove.getName());
	}
}
