package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.dao.BranchDao;
import com.adapp.adapp_pos_backend.dao.InvoiceDao;
import com.adapp.adapp_pos_backend.dao.InvoiceNumberTrackerDao;
import com.adapp.adapp_pos_backend.utils.BackendKafkaAuditSender;
import com.adapp.adapp_pos_models.invoices.*;
import com.adapp.adapp_pos_models.payments.PaymentDetails;
import com.adapp.adapp_pos_models.profiles.Branch;
import com.adapp.adapp_pos_models.utils.IdGenerationUtility;
import org.apache.shiro.SecurityUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.NotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import static com.adapp.adapp_pos_models.enums.Resource.INVOICES;
import static com.adapp.adapp_pos_models.invoices.Invoice.TransactionType.DELIVERY;

@Service
public class InvoiceService {

	public static final String WHITE_SPACE = " ";
	public static final int MAX_NUMBER_FOR_PADDING = 9999;
	public static final String ROOT_ADMIN_PERMISSION = "*:*:*";
	public static final String PAST_DELIVERY_DATE_ERROR_MESSAGE = "Non-root admins cannot edit an invoice after 24 hours to prevent fraud";
	public static final String BRANCH_ID = "branchId";
	public static final String INVOICE_NUMBER = "Invoice Number";

	InvoiceDao invoiceDao;

	InvoiceNumberTrackerDao invoiceNumberTrackerDao;

	BranchDao branchDao;

	BackendKafkaAuditSender<Invoice> backendKafkaAuditSender;

	private int deliveryStartTime;

	private int deliveryEndTime;

	@Autowired
	public InvoiceService(@Value("${DELIVERY_START_TIME}") int deliveryStartTime,
                          @Value("${DELIVERY_END_TIME}") int deliveryEndTime,
                          InvoiceDao invoiceDao,
                          InvoiceNumberTrackerDao invoiceNumberTrackerDao,
                          BranchDao branchDao,
                          BackendKafkaAuditSender<Invoice> backendKafkaAuditSender) {
		this.deliveryStartTime = deliveryStartTime;
		this.deliveryEndTime = deliveryEndTime;
		this.invoiceDao = invoiceDao;
		this.invoiceNumberTrackerDao = invoiceNumberTrackerDao;
		this.branchDao = branchDao;
		this.backendKafkaAuditSender = backendKafkaAuditSender;
	}

	public List<Invoice> getAllInvoices() {
		return invoiceDao.getAllInvoices();
	}

	public void save(Invoice invoice) {
		invoice.setInvoiceId(IdGenerationUtility.generateId(invoiceDao::exists));
		DateTime currentDateTime = new DateTime();
		invoice.setCreateDate(currentDateTime);
		invoice.setInvoiceDate(currentDateTime);
		invoice.setDueDate(currentDateTime.plusDays(1));

		if(invoice.getDeliveryStatus() == null) {
			invoice.setDeliveryStatus(Invoice.DeliveryStatus.PROCESSING);
		}

		if (invoice.getTransactionType() == DELIVERY) {
			validateDeliveryTime(invoice.getDeliveryDate());
		}

		if(invoice.getTransactionType() == Invoice.TransactionType.DINE_IN) {
			invoice.setDeliveryDate(DateTime.now().plusMinutes(30));
		}

		StringBuilder invoiceNumberStringBuilder = new StringBuilder();
		Branch branch = branchDao.getBranchByBranchId(invoice.getBranchId());
		Arrays.asList(branch.getName().split(" "))
				.stream()
				.forEach(branchNamePart -> invoiceNumberStringBuilder.append(branchNamePart.charAt(0)));


		InvoiceNumberTracker invoiceNumberTracker = invoiceNumberTrackerDao
				.findAndModifyCurrentNumber(invoice.getBranchId());
		if(invoiceNumberTracker.getCurrentNumber() <= MAX_NUMBER_FOR_PADDING) {
			invoiceNumberStringBuilder.append(String.format("%05d", invoiceNumberTracker.getCurrentNumber()));
		} else {
			invoiceNumberStringBuilder.append(invoiceNumberTracker.getCurrentNumber());
		}
		invoice.setNumber(invoiceNumberStringBuilder.toString());

		Invoice savedInvoice = invoiceDao.saveAndReturn(invoice);
		backendKafkaAuditSender.send(INVOICES, savedInvoice.getId(), invoice.getBranchId(), INVOICE_NUMBER, invoice.getNumber());
	}

	private void validateDeliveryTime(DateTime deliveryDate) {
		DateTime deliveryStartDate = deliveryDate
				.withZone(DateTimeZone.forTimeZone(TimeZone.getTimeZone("GMT+8")))
				.withHourOfDay(deliveryStartTime)
				.withMinuteOfHour(0)
				.withMillisOfSecond(0);
		DateTime deliveryStartEnd = deliveryDate
				.withZone(DateTimeZone.forTimeZone(TimeZone.getTimeZone("GMT+8")))
				.withHourOfDay(deliveryEndTime)
				.withMinuteOfHour(0)
				.withMillisOfSecond(0);
		if (deliveryDate.isBefore(deliveryStartDate) || deliveryDate.isAfter(deliveryStartEnd)) {
			throw new BadRequestException("Delivery date must be within operating hours");
		}
	}

	public void update(Invoice invoice) {
		if (invoice.getId() == null) {
			throw new NullPointerException("Invoice Object Id cannot be null for update");
		}
		if (invoice.getTransactionType() == DELIVERY) {
			validateDeliveryTime(invoice.getDeliveryDate());
		}
		Invoice invoiceBeforeUpdate = invoiceDao.findAndReplace(invoice);

		this.backendKafkaAuditSender.send(INVOICES, invoice.getId(), invoice.getBranchId(), INVOICE_NUMBER, invoice.getNumber(), invoiceBeforeUpdate, invoice);
	}

	public List<InvoiceDTO> getInvoiceDTOList() {
		return invoiceDao.getAllInvoices().stream().map(this::convertToInvoiceDTO).collect(Collectors.toList());
	}

	private InvoiceDTO convertToInvoiceDTO(Invoice invoice) {

		InvoiceDTO invoiceDTO = new InvoiceDTO();
		invoiceDTO.setId(invoice.getId());
		invoiceDTO.setInvoiceId(invoice.getInvoiceId());
		invoiceDTO.setNumber(invoice.getNumber());
		invoiceDTO.setInvoiceId(invoice.getInvoiceId());
		invoiceDTO.setCustomer(invoice.getCustomer().getName());
		invoiceDTO.setAmountDue(invoice.getAmountDue());
		invoiceDTO.setDueDate(invoice.getDueDate());
		invoiceDTO.setInvoiceDate(invoice.getInvoiceDate());
		invoiceDTO.setStatus(invoice.getDeliveryStatus() != null ? invoice.getDeliveryStatus().getStatus() : "");
		invoiceDTO.setTransactionType(invoice.getTransactionType().getTransactionType());
		invoiceDTO.setTableNumber(invoice.getTableNumber());
		invoiceDTO.setBranchId(invoice.getBranchId());
		invoiceDTO.setUpdateDate(invoice.getUpdateDate());
		invoiceDTO.setUpdatedBy(invoice.getUpdatedBy());
		if (invoice.getPaymentDetails() != null) {
			invoiceDTO.setPaymentStatus(invoice.getPaymentDetails().getPaymentStatus());
		} else {
			invoiceDTO.setPaymentStatus(PaymentDetails.PaymentStatus.UNPAID);
		}
		invoiceDTO.setVoided(invoice.isVoided());
		return invoiceDTO;
	}

	public PagedResultSet<InvoiceDTO> getInvoiceDTOListBy(QueryParameters queryParameters) {
		PagedResultSet<Invoice> invoicePagedResultSet = invoiceDao.getInvoicesBy(queryParameters);
		PagedResultSet<InvoiceDTO> invoiceDTOPagedResultSet = new PagedResultSet<>();
		invoiceDTOPagedResultSet.setPage(invoicePagedResultSet.getPage());
		invoiceDTOPagedResultSet.setTotalCount(invoicePagedResultSet.getTotalCount());
		invoiceDTOPagedResultSet.setSize(invoicePagedResultSet.getSize());
		invoiceDTOPagedResultSet.setLimit(invoicePagedResultSet.getLimit());
		invoiceDTOPagedResultSet.setResults(
				invoicePagedResultSet.getResults()
				.stream()
				.map(this::convertToInvoiceDTO)
				.collect(Collectors.toList())
		);
		return invoiceDTOPagedResultSet;
	}

	public List<InvoiceScheduleDTO> getInvoiceSchedules(QueryParameters queryParameters) {
		PagedResultSet<Invoice> invoicePagedResultSet = invoiceDao.getInvoicesBy(queryParameters);
		return invoicePagedResultSet.getResults()
				.stream()
				.map(this::convertToInvoiceScheduleDTO)
				.collect(Collectors.toList());
	}

	private InvoiceScheduleDTO convertToInvoiceScheduleDTO(Invoice invoice) {
		String[] splitCustomerNames = invoice.getCustomer().getName().split(" ");
		String customerLastName = splitCustomerNames[splitCustomerNames.length - 1];

		String rider = invoice.getRiderDetails() != null ? invoice.getRiderDetails().getPersonalDetails().getLastName()
				: "N/A";
		InvoiceScheduleDTO invoiceScheduleDTO = new InvoiceScheduleDTO();
		invoiceScheduleDTO.setId(invoice.getId());
		invoiceScheduleDTO.setTitle(customerLastName);
		invoiceScheduleDTO.setCity(invoice.getCustomer().getAddress().getCity());
		invoiceScheduleDTO.setDescription(invoice.getNumber() + "-" + rider);
		invoiceScheduleDTO.setStart(invoice.getDeliveryDate());
		invoiceScheduleDTO.setEnd(invoice.getDeliveryDate().plusMinutes(30));
		invoiceScheduleDTO.setStatus(invoice.getDeliveryStatus().name());
		if(DELIVERY.equals(invoice.getTransactionType())) {
			invoiceScheduleDTO
					.setForSameDayDelivery(invoice.getInvoiceDate()
					.withZone(DateTimeZone.forTimeZone(TimeZone.getTimeZone("GMT+8")))
					.withTimeAtStartOfDay()
			.equals(invoice
					.getDeliveryDate()
					.withZone(DateTimeZone.forTimeZone(TimeZone.getTimeZone("GMT+8")))
					.withTimeAtStartOfDay()));
		}

		return invoiceScheduleDTO;
	}

	public List<InvoiceQueueDTO> getInvoiceQueue(QueryParameters queryParameters) {
		queryParameters.setLimit(20);
		queryParameters.setPage(1);
		queryParameters.setSortField("deliveryDate");
		queryParameters.setAscending(true);
		PagedResultSet<Invoice> invoicePagedResultSet = invoiceDao.getInvoicesBy(queryParameters);
		return invoicePagedResultSet.getResults()
				.stream()
				.map(this::convertToInvoiceQueueDTO)
				.collect(Collectors.toList());
	}

	private InvoiceQueueDTO convertToInvoiceQueueDTO(Invoice invoice) {
		InvoiceQueueDTO invoiceQueueDTO = new InvoiceQueueDTO();
		invoiceQueueDTO.setId(invoice.getId());
		invoiceQueueDTO.setInvoiceNumber(invoice.getNumber());
		if (invoice.getTransactionType() == DELIVERY) {
			invoiceQueueDTO.setCustomerName(invoice.getCustomer().getName());
		} else {
			invoiceQueueDTO.setCustomerName("Table " + invoice.getTableNumber());
		}
		invoiceQueueDTO.setItems(invoice.getItems());
		if(Invoice.DeliveryStatus.PROCESSING == invoice.getDeliveryStatus()) {
			invoiceQueueDTO.setLate(DateTime.now().isAfter(invoice.getDeliveryDate()));
		}
		if(Invoice.DeliveryStatus.FOR_PICK_UP == invoice.getDeliveryStatus()) {
			invoiceQueueDTO.setLate(DateTime.now().isAfter(invoice.getDeliveryDate().plusHours(4)));
		}
		invoiceQueueDTO.setDeliveryDateTimeString(invoice.getDeliveryDate().withZone(DateTimeZone.forTimeZone(TimeZone.getTimeZone("GMT+8"))).toString(DateTimeFormat.forPattern("MMM/dd/yyyy hh:mma")));
		if(DELIVERY.equals(invoice.getTransactionType())) {
			invoiceQueueDTO
					.setForSameDayDelivery(invoice.getInvoiceDate()
							.withZone(DateTimeZone.forTimeZone(TimeZone.getTimeZone("GMT+8")))
							.withTimeAtStartOfDay()
							.equals(invoice
									.getDeliveryDate()
									.withZone(DateTimeZone.forTimeZone(TimeZone.getTimeZone("GMT+8")))
									.withTimeAtStartOfDay()));
		}
		return invoiceQueueDTO;
	}

	public Invoice getInvoice(String id) {
		Invoice invoice = invoiceDao.getInvoice(id);
		if (invoice == null) {
			throw new NotFoundException(String.format("Cannot find an invoice with object id %s", id));
		}
		return invoice;
	}
	public void remove(String id) {
		Invoice invoiceToRemove = invoiceDao.getInvoice(id);
		if (invoiceToRemove == null) {
			throw new NotFoundException(String.format("Cannot find invoice to be removed with object id, %s", id));
		}
		invoiceDao.remove(invoiceToRemove);
	}

	public void voidInvoice(String id) {
		Invoice invoiceToVoid = invoiceDao.getInvoice(id);
		if (invoiceToVoid == null) {
			throw new NotFoundException(String.format("Cannot find invoice to be removed with object id, %s", id));
		}
		invoiceToVoid.setVoided(true);
		Invoice invoiceBeforeVoid = invoiceDao.findAndReplace(invoiceToVoid);

		this.backendKafkaAuditSender.send(INVOICES, invoiceToVoid.getId(), invoiceToVoid.getBranchId(), INVOICE_NUMBER, invoiceToVoid.getNumber(), invoiceBeforeVoid, invoiceToVoid);

	}

	public void updatePaymentDetails(String id, PaymentDetails paymentDetails) {

		if (paymentDetails.getAmount().compareTo(BigDecimal.ZERO) < 0) {
			throw new BadRequestException("Amount cannot be less than 0");
		}

		if (paymentDetails.getPaymentMethod() == null) {
			throw new BadRequestException("Payment method is required");
		}

		if (paymentDetails.getPaymentDate() == null) {
			throw new BadRequestException("Payment Date is required");
		}

		Invoice invoice = invoiceDao.getInvoice(id);
		if(paymentDetails.getAmount().compareTo(invoice.getAmountDue()) >= 0) {
			paymentDetails.setPaymentStatus(PaymentDetails.PaymentStatus.PAID);
		} else {
			paymentDetails.setPaymentStatus(PaymentDetails.PaymentStatus.PARTIAL);
		}
		invoice.setPaymentDetails(paymentDetails);
		invoice.getPaymentDetails().setUpdateDate(DateTime.now());
		Invoice invoiceBeforeUpdate = invoiceDao.findAndReplace(invoice);

		backendKafkaAuditSender.send(INVOICES, invoice.getId(), null, INVOICE_NUMBER, invoice.getNumber(), invoiceBeforeUpdate, invoice);
	}

	public void updateRiderDetails(String id, Invoice.RiderDetails riderDetails) {
		if (riderDetails == null) {
			throw new BadRequestException("Rider Details cannot be null");
		}
		Invoice invoice = invoiceDao.getInvoice(id);
		invoice.setRiderDetails(riderDetails);
		Invoice invoiceBeforeUpdate = invoiceDao.findAndReplace(invoice);

		backendKafkaAuditSender.send(INVOICES, invoice.getId(), invoice.getBranchId(), INVOICE_NUMBER, invoice.getNumber(), invoiceBeforeUpdate, invoice);
	}

	public void checkInvoiceIsPastDeliveryDateBeforeUpdating(String invoiceId) {
		Invoice invoice = invoiceDao.getInvoice(invoiceId);

		if (DateTime.now().isAfter(invoice.getDeliveryDate().plusDays(1).withTimeAtStartOfDay())) {
			if (!SecurityUtils.getSubject().isPermitted(ROOT_ADMIN_PERMISSION)) {
				throw new NotAcceptableException(PAST_DELIVERY_DATE_ERROR_MESSAGE);
			}
		}
	}

	public InvoiceDeliveriesForDashboardDTO getInvoiceDeliveriesForDashboard(String branchId) {
		InvoiceDeliveriesForDashboardDTO invoiceDeliveriesForDashboardDTO = new InvoiceDeliveriesForDashboardDTO();
		determineDeliveriesTodayCount(branchId, invoiceDeliveriesForDashboardDTO);
		determineDeliveredTodayCount(branchId, invoiceDeliveriesForDashboardDTO);
		determineRemainingDeliveriesCount(branchId, invoiceDeliveriesForDashboardDTO);
		determineSameDayDeliveriesCount(branchId, invoiceDeliveriesForDashboardDTO);

		return invoiceDeliveriesForDashboardDTO;
	}

	private void determineDeliveriesTodayCount(String branchId, InvoiceDeliveriesForDashboardDTO invoiceDeliveriesForDashboardDTO) {
		QueryParameters queryParameters = createQueryParametersForRetrievingInvoiceDeliveries(branchId);

		invoiceDeliveriesForDashboardDTO.setDeliveriesToday(invoiceDao.count(queryParameters));
	}

	private QueryParameters createQueryParametersForRetrievingInvoiceDeliveries(String branchId) {
		QueryParameters queryParameters = new QueryParameters();
		if (!"*".equals(branchId)) {
			queryParameters.getCriteria().put(BRANCH_ID, branchId);
		}
		queryParameters.getCriteria().put("transactionType", DELIVERY);
		queryParameters.getCustomCriteria()
				.add(Criteria.where("deliveryDate")
						.lte(DateTime.now().withTimeAtStartOfDay().plusDays(1).minusMillis(1))
						.gte(DateTime.now().withTimeAtStartOfDay()));
		return queryParameters;
	}

	private void determineDeliveredTodayCount(String branchId, InvoiceDeliveriesForDashboardDTO invoiceDeliveriesForDashboardDTO) {
		QueryParameters queryParameters = createQueryParametersForRetrievingInvoiceDeliveries(branchId);
		List<Invoice.DeliveryStatus> statusesAfterDelivery = new ArrayList<>();
		statusesAfterDelivery.add(Invoice.DeliveryStatus.DELIVERED);
		statusesAfterDelivery.add(Invoice.DeliveryStatus.FOR_PICK_UP);
		statusesAfterDelivery.add(Invoice.DeliveryStatus.DONE);
		queryParameters.getCriteria().put("deliveryStatus", statusesAfterDelivery);
		invoiceDeliveriesForDashboardDTO.setDeliveredToday(invoiceDao.count(queryParameters));
	}

	private void determineRemainingDeliveriesCount(String branchId, InvoiceDeliveriesForDashboardDTO invoiceDeliveriesForDashboardDTO) {
		QueryParameters queryParameters = createQueryParametersForRetrievingInvoiceDeliveries(branchId);
		List<Invoice.DeliveryStatus> statusesBeforeDelivery = new ArrayList<>();
		statusesBeforeDelivery.add(Invoice.DeliveryStatus.PROCESSING);
		statusesBeforeDelivery.add(Invoice.DeliveryStatus.FOR_DELIVERY);
		queryParameters.getCriteria().put("deliveryStatus", statusesBeforeDelivery);
		invoiceDeliveriesForDashboardDTO.setRemainingDeliveries(invoiceDao.count(queryParameters));
	}

	private void determineSameDayDeliveriesCount(String branchId, InvoiceDeliveriesForDashboardDTO invoiceDeliveriesForDashboardDTO) {
		QueryParameters queryParameters = createQueryParametersForRetrievingInvoiceDeliveries(branchId);
		queryParameters.getCustomCriteria()
				.add(Criteria.where("createDate")
						.lte(DateTime.now().withTimeAtStartOfDay().plusDays(1).minusMillis(1))
						.gte(DateTime.now().withTimeAtStartOfDay()));
		invoiceDeliveriesForDashboardDTO.setSameDayDeliveries(invoiceDao.count(queryParameters));
	}
}