package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.dao.ExpenseDao;
import com.adapp.adapp_pos_models.expenses.Expense;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.List;

@Service
public class ExpenseService {

    @Autowired
    ExpenseDao expenseDao;

    public List<Expense> getAllExpenses() {
        return expenseDao.getAllExpenses();
    }

    public Expense getExpense(String id) {
        return expenseDao.getExpenses(id);
    }

    public void save(Expense expense) {
        if (expense.getAmount() == null) {
            throw new BadRequestException("Amount is required");
        }
        expenseDao.save(expense);
    }

    public PagedResultSet<Expense> getExpensesBy(QueryParameters queryParameters) {
        return expenseDao.getExpensesBy(queryParameters);
    }

    public void update(Expense expense) {
        if (expense.getId() == null) {
            throw new NullPointerException("Expense Object Id cannot be null for update");
        }

        expense.setUpdateDate(new DateTime());
        expenseDao.save(expense);
    }

    public void remove(String id) {
            Expense expenseToRemove = expenseDao.getExpenses(id);
            if (expenseToRemove == null) {
                throw new NotFoundException(String.format("Cannot find expense to be removed with object id, %s", id));
            }
            expenseDao.remove(expenseToRemove);
    }
}
