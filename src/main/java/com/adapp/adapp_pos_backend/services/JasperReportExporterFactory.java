package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_models.enums.DocType;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.*;

public class JasperReportExporterFactory {

	/**
	 *
	 * @param docType
	 * @return
	 */
	public Exporter<ExporterInput, ? extends ReportExportConfiguration, ? extends ExporterConfiguration, ? extends ExporterOutput>
		getExporter(DocType docType){
		switch (docType){
			case PDF:
				return new JRPdfExporter();
			case XLS:
				return new JRXlsxExporter();
			case DOCX:
				return new JRDocxExporter();
			case CSV:
				return new JRCsvExporter();
			default:
				throw new ServerException(String.format("Unsupported format:%s", docType));
		}
	}
}
