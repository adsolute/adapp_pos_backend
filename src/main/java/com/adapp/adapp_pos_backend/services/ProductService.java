package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.dao.ProductDao;
import com.adapp.adapp_pos_backend.utils.BackendKafkaAuditSender;
import com.adapp.adapp_pos_models.products.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import java.util.List;

import static com.adapp.adapp_pos_models.enums.Resource.PRODUCTS;

@Service
public class ProductService {

	public static final String CODE = "Code";
	ProductDao productDao;

	BackendKafkaAuditSender<Product> backendKafkaAuditSender;

	@Autowired
	public ProductService(ProductDao productDao, BackendKafkaAuditSender<Product> backendKafkaAuditSender) {
		this.productDao = productDao;
		this.backendKafkaAuditSender = backendKafkaAuditSender;
	}

	public List<Product> getAllProducts(){
		return productDao.getAllProducts();
	}

	public void save(Product product) {
		Product savedProduct = productDao.saveAndReturn(product);
		backendKafkaAuditSender.send(PRODUCTS, savedProduct.getId(), null, CODE, savedProduct.getProductCode());
	}

	public void update(Product product) {
		Product productBeforeUpdate = productDao.findAndReplace(product);
		backendKafkaAuditSender.send(PRODUCTS, product.getId(), null, CODE, product.getProductCode(), productBeforeUpdate, product);
	}

	public List<Product> getProducts(String name) {
		return productDao.getProducts(name);
	}

	public PagedResultSet<Product> getProductsBy(QueryParameters queryParameters) {
		return productDao.getProductsBy(queryParameters);
	}

	public Product getProduct(String id) {
		return productDao.getProduct(id);
	};

	public void remove(String id) {
		Product productToRemove = productDao.getProduct(id);
		if (productToRemove == null) {
			throw new NotFoundException(String.format("Cannot find product to be removed with object id, %s", id));
		}
		productDao.remove(productToRemove);
		backendKafkaAuditSender.send(PRODUCTS, null, CODE, productToRemove.getProductCode());
	}
}
