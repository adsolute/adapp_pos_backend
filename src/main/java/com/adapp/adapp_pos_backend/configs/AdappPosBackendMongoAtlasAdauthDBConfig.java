package com.adapp.adapp_pos_backend.configs;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

@Configuration
@ConditionalOnExpression("'${spring.data.mongodb.host}'.contains('mongodb+srv')")
public class AdappPosBackendMongoAtlasAdauthDBConfig {
    @Value("${adauth.host}")
    private String mongoHost;
    @Value("${adauth.database}")
    private String dbName;

    private ConnectionString createConnectionString() {
        return new ConnectionString(mongoHost);
    }

    private MongoClientSettings buildMongoClientSettings() {
        return MongoClientSettings
                .builder()
                .applyConnectionString(createConnectionString())
                .build();
    }

    public SimpleMongoClientDatabaseFactory createMongoDbFactory() {

        return new SimpleMongoClientDatabaseFactory(
                MongoClients.create(buildMongoClientSettings()),
                dbName
        );
    }

    @Bean(name = "adauthMongoTemplate")
    public MongoTemplate createMongoTemplate() {
        return new MongoTemplate(createMongoDbFactory());
    }
}
