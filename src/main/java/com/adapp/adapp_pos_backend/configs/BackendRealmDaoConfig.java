package com.adapp.adapp_pos_backend.configs;

import com.adapp.security.dao.AccountDao;
import com.adapp.security.dao.RoleDao;
import com.adapp.security.dao.UserDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BackendRealmDaoConfig {

	@Bean
	public UserDao createUserDao() {
		return new UserDao();
	}

	@Bean
	public RoleDao createRoleDao() {
		return new RoleDao();
	}

	@Bean
	public AccountDao accountDao() {
		return new AccountDao();
	}
}
