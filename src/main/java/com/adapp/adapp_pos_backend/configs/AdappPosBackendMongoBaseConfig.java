package com.adapp.adapp_pos_backend.configs;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

public abstract class AdappPosBackendMongoBaseConfig {
    protected String mongoHost;
    protected int mongoPort;
    protected String dbName;
    protected String username;
    protected String password;

    private MongoCredential createMongoCredential() {
        return MongoCredential.createCredential(username, dbName, password.toCharArray());
    }

    private ConnectionString createConnectionString() {
        return new ConnectionString("mongodb://"+mongoHost+":"+mongoPort);
    }

    private MongoClientSettings buildMongoClientSettings() {
        return MongoClientSettings
                .builder()
                .applyConnectionString(createConnectionString())
                .credential(createMongoCredential())
                .build();
    }

    public SimpleMongoClientDatabaseFactory createMongoDbFactory() {

        return new SimpleMongoClientDatabaseFactory(
                MongoClients.create(buildMongoClientSettings()),
                dbName
        );
    }

    public abstract MongoTemplate createMongoTemplate();
}
