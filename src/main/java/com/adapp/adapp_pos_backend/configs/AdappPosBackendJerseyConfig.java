package com.adapp.adapp_pos_backend.configs;

import com.adapp.adapp_pos_backend.controllers.*;
import com.adapp.adapp_pos_models.exceptions.ServerExceptionMapper;
import com.adapp.adapp_pos_models.utils.CORSFilter;
import com.adapp.adapp_pos_models.utils.JacksonMapperContextResolver;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AdappPosBackendJerseyConfig extends ResourceConfig {

	public AdappPosBackendJerseyConfig(){
		property( ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true );
		register(CORSFilter.class);
		register(JacksonMapperContextResolver.class);
		register(ServerExceptionMapper.class);
//		packages( "com.adapp.adapp_pos_backend.controllers");
		register(BranchController.class);
		register(CustomerController.class);
		register(InvoiceController.class);
		register(ProductController.class);
		register(ReportController.class);
	}
}
