package com.adapp.adapp_pos_backend.configs;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

@Configuration

@ConditionalOnExpression("!'${spring.data.mongodb.host}'.contains('mongodb+srv')")
public class AdappPosBackendMongoAdauthDBConfig extends AdappPosBackendMongoBaseConfig {

    public AdappPosBackendMongoAdauthDBConfig(
            @Value("${spring.data.mongodb.host}") String mongoHost,
            @Value("${spring.data.mongodb.port}") int mongoPort,
            @Value("${adauth.database}") String dbName,
            @Value("${adauth.username}") String username,
            @Value("${adauth.password}") String password) {
        this.mongoHost = mongoHost;
        this.mongoPort = mongoPort;
        this.dbName = dbName;
        this.username = username;
        this.password = password;
    }

    @Override
    @Bean(name = "adauthMongoTemplate")
    public MongoTemplate createMongoTemplate() {
        return new MongoTemplate(createMongoDbFactory());
    }
}
