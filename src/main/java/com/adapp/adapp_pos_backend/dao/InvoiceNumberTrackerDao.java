package com.adapp.adapp_pos_backend.dao;

import com.adapp.adapp_pos_models.invoices.InvoiceNumberTracker;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;


@Repository
public class InvoiceNumberTrackerDao extends BaseDao<InvoiceNumberTracker>{

	public InvoiceNumberTracker findAndModifyCurrentNumber(String branchId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("branchId").is(branchId));

		Update update = new Update();
		update.inc("currentNumber");

		return mongoTemplate.findAndModify(query, update, FindAndModifyOptions.options().returnNew(true).upsert(true), InvoiceNumberTracker.class);
	}

}
