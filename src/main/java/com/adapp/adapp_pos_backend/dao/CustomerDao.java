package com.adapp.adapp_pos_backend.dao;

import com.adapp.adapp_pos_models.customers.Customer;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerDao extends BaseDao<Customer> {

	public List<Customer> getAllCustomers() {
		return getAll(Customer.class);
	}

	public Customer saveAndReturn(Customer customer) {
		return super.mongoTemplate.save(customer);
	}

	public Customer findAndReplace(Customer customer) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(customer.getId()));
		return super.mongoTemplate.findAndReplace(query, customer, FindAndReplaceOptions.options().upsert());
	}

	public List<Customer> getCustomers(String name){
		Criteria regex = Criteria.where("name").regex(name, "i");
		return mongoTemplate.find(new Query().addCriteria(regex), Customer.class);
	}

	public PagedResultSet<Customer> getCustomersBy(QueryParameters queryParameters) {
		return super.retrieve(queryParameters, Customer.class);
	}

	public Customer getCustomer(String id) {
		return mongoTemplate.findById(new ObjectId(id), Customer.class);
	}

	public void remove(Customer customer){
		super.remove(customer);

	}
}
