package com.adapp.adapp_pos_backend.dao;

import com.adapp.adapp_pos_models.reports.DocumentData;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class DocumentDataDao extends BaseDao<DocumentData> {

	public static final int ONE_MONTH_IN_SECONDS = 2629746;

	public void save (DocumentData documentData) {
		super.mongoTemplate
				.indexOps(DocumentData.class)
				.ensureIndex(new Index().named("expiry_index").on("requestDate", Sort.Direction.ASC).expire(ONE_MONTH_IN_SECONDS));
		super.save(documentData);
	}

	public DocumentData getByDocumentDataId (String documentDataId) {
		return mongoTemplate.findOne(Query.query(Criteria.where("documentDataId").is(documentDataId)), DocumentData.class);
	}
}
