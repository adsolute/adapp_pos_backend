package com.adapp.adapp_pos_backend.dao;

import com.adapp.adapp_pos_models.invoices.Invoice;
import com.adapp.adapp_pos_models.reports.DailySalesReportBean;
import com.adapp.adapp_pos_models.reports.DailySalesReportBean.OrderedProduct;
import com.adapp.adapp_pos_models.reports.InvoiceSalesReport;
import com.adapp.adapp_pos_models.reports.InvoiceSalesReport.TotalInvoiceByHour;
import com.adapp.security.models.UserPrincipal;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.springframework.data.mongodb.core.aggregation.DateOperators.Hour.hourOf;
import static org.springframework.data.mongodb.core.aggregation.DateOperators.Timezone.valueOf;

@Repository
public class InvoiceDao extends BaseDao<Invoice> {

	public List<Invoice> getAllInvoices(){
		return getAll(Invoice.class);
	}

	public PagedResultSet<Invoice> getInvoicesBy(QueryParameters queryParameters) {
		return super.retrieve(queryParameters, Invoice.class);
	}

	public Invoice saveAndReturn(Invoice invoice) {
		return super.mongoTemplate.save(invoice);
	}

	public Invoice findAndReplace(Invoice invoice) {
		Subject subject = SecurityUtils.getSubject();
		UserPrincipal userPrincipal = (UserPrincipal) subject.getPrincipal();
		invoice.setUpdatedBy(userPrincipal.getUsername());
		invoice.setUpdateDate(new DateTime());

		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(invoice.getId()));
		return super.mongoTemplate.findAndReplace(query, invoice, FindAndReplaceOptions.options().upsert());
	}

	public boolean exists(String invoiceId) {
		return super.exists("invoiceId", invoiceId, Invoice.class);
	}

	public Invoice getInvoice(String id) {
		return mongoTemplate.findById(new ObjectId(id), Invoice.class);
	}

	public void remove(Invoice invoice) {
		super.remove(invoice);
	}

	public List<InvoiceSalesReport> aggregateInvoiceSalesReport(QueryParameters queryParameters) {
		MatchOperation matchOperation = buildMatchOperation(queryParameters);

		GroupOperation groupOperation = Aggregation.group()
				.count().as("totalOrders")
				.sum(ConvertOperators.valueOf("amountDue").convertToDouble()).as("totalSales");

		Aggregation aggregation = Aggregation.newAggregation(
				matchOperation,
				groupOperation
		);
		return mongoTemplate.aggregate(aggregation, Invoice.class, InvoiceSalesReport.class).getMappedResults();
	}

	private MatchOperation buildMatchOperation(QueryParameters queryParameters) {
		List<Criteria> criteriaList = new ArrayList<>();
		for(DateRange r : queryParameters.getDateRanges()){
			criteriaList.add(Criteria.where(r.getField()).lte(r.getEnd()).gte(r.getStart()));
		}

		for (Map.Entry<String, Object> entry : queryParameters.getCriteria().entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();

			if (value instanceof Collection<?>) {
				criteriaList.add(Criteria.where(key).in((ArrayList<String>) value));
			} else {
				if(queryParameters.isRegex()){
					criteriaList.add(Criteria.where(key).regex(String.valueOf(value), "i"));
				}
				else{
					criteriaList.add(Criteria.where(key).is(value));
				}
			}
		}

		for (Criteria criteria : queryParameters.getCustomCriteria()) {
			criteriaList.add(criteria);
		}

		MatchOperation matchOperation;
		if (criteriaList.isEmpty()) {
			matchOperation = Aggregation.match(new Criteria());
		} else {
			matchOperation = Aggregation
					.match(new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()])));
		}
		return matchOperation;
	}

	public List<TotalInvoiceByHour> aggregateInvoiceByHourOrdered(QueryParameters queryParameters) {
		MatchOperation matchOperation = buildMatchOperation(queryParameters);
		ProjectionOperation projectionOperation = Aggregation.project()
				.and(hourOf("invoiceDate").withTimezone(valueOf("+08:00"))).as("hour");

		GroupOperation groupOperation = Aggregation.group("hour")
				.last("hour").as("hour")
				.count().as("totalInvoice");
		Aggregation aggregation = Aggregation.newAggregation(
				matchOperation,
				projectionOperation,
				groupOperation
		);
		return mongoTemplate.aggregate(aggregation, Invoice.class, TotalInvoiceByHour.class).getMappedResults();
	}

	public List<OrderedProduct> aggregateInvoiceByOrderedProducts(QueryParameters queryParameters) {
		MatchOperation matchOperation = buildMatchOperation(queryParameters);
		ProjectionOperation projectionOperation = Aggregation.project()
				.and("items").as("orders");
		UnwindOperation unwindOperation = Aggregation.unwind("orders");
		GroupOperation groupOperation = Aggregation.group("orders.product._id")
				.last("orders.product.productCode").as("code")
				.sum("orders.quantity").as("totalOrders");
		Aggregation aggregation = Aggregation.newAggregation(
				matchOperation,
				projectionOperation,
				unwindOperation,
				groupOperation
		);
		return mongoTemplate.aggregate(aggregation, Invoice.class, OrderedProduct.class).getMappedResults();
	}

	public long count (QueryParameters queryParameters) {
		return super.count(queryParameters, Invoice.class);
	}
}
