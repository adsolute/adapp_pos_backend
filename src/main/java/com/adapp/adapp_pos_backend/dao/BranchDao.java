package com.adapp.adapp_pos_backend.dao;

import com.adapp.adapp_pos_models.profiles.Branch;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BranchDao extends BaseDao<Branch>{

	public List<Branch> getAllBranches() {
		return getAll(Branch.class);
	}

	public Branch saveThenReturn(Branch branch) {
		return mongoTemplate.save(branch);
	}

	public Branch findAndReplace(Branch branch) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(branch.getId()));
		return super.mongoTemplate.findAndReplace(query, branch, FindAndReplaceOptions.options().upsert());
	}

	public boolean exists(String branchId) {
		return super.exists("branchId", branchId, Branch.class);
	}

	public PagedResultSet<Branch> getPagedResultSet(QueryParameters queryParameters) {
		return super.retrieve(queryParameters, Branch.class);
	}

	public Branch getBranch(String id) {
		return mongoTemplate.findById(new ObjectId(id), Branch.class);
	}

	public Branch getBranchByBranchId(String branchId) {
		return mongoTemplate.findOne(Query.query(Criteria.where("branchId").is(branchId)), Branch.class);
	}

	public void remove(Branch branch) {
		super.remove(branch);
	}
}
