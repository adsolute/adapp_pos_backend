package com.adapp.adapp_pos_backend.dao;

import com.adapp.adapp_pos_models.products.Product;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductDao extends BaseDao<Product>{

	public List<Product> getAllProducts() {
		return getAll(Product.class);
	}

	public Product saveAndReturn(Product product){
		return super.mongoTemplate.save(product);
	}

	public Product findAndReplace(Product product) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(product.getId()));
		return super.mongoTemplate.findAndReplace(query, product, FindAndReplaceOptions.options().upsert());
	}

	public List<Product> getProducts(String name){
		Criteria regexName = Criteria.where("name").regex(name, "i");
		Criteria regexCode = Criteria.where("productCode").regex(name, "i");
		return mongoTemplate.find(new Query().addCriteria(new Criteria().orOperator(regexName, regexCode)), Product.class);
	}

	public PagedResultSet<Product> getProductsBy(QueryParameters queryParameters) {
		return super.retrieve(queryParameters, Product.class);
	}

	public Product getProduct(String id) {
		return mongoTemplate.findById(new ObjectId(id), Product.class);
	}

	public void remove(Product product) {
		super.remove(product);
	}
}
