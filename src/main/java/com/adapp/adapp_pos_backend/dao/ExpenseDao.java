package com.adapp.adapp_pos_backend.dao;

import com.adapp.adapp_pos_models.expenses.Expense;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ExpenseDao extends BaseDao<Expense> {

    public List<Expense> getAllExpenses() {
        return getAll(Expense.class);
    }

    public void save(Expense expense) {
        super.save(expense);
    }

    public boolean exists(String expenseId) {
        return super.exists("expenseId", expenseId, Expense.class);
    }

    public PagedResultSet<Expense> getPageResultSet(QueryParameters queryParameters) {
        return  super.retrieve(queryParameters, Expense.class);
    }

    public PagedResultSet<Expense> getExpensesBy(QueryParameters queryParameters) {
        return  super.retrieve(queryParameters, Expense.class);
    }

    public Expense getExpenses(String id) {
        return mongoTemplate.findById(new ObjectId(id), Expense.class);
    }

    public void remove(Expense expense) {
        super.remove(expense);
    }
}