package com.adapp.adapp_pos_backend.builders;

import com.adapp.adapp_pos_backend.dao.BranchDao;
import com.adapp.adapp_pos_models.enums.ReportType;
import com.adapp.adapp_pos_models.invoices.Invoice;
import com.adapp.adapp_pos_models.invoices.Invoice.Item;
import com.adapp.adapp_pos_models.invoices.Invoice.Note;
import com.adapp.adapp_pos_models.profiles.Branch;
import com.adapp.adapp_pos_models.reports.DailySalesReportBean;
import com.adapp.adapp_pos_models.reports.DailySalesReportBean.InvoiceDailySalesBean;
import com.adapp.adapp_pos_models.reports.DailySalesReportBean.OrderedProduct;
import com.adapp.adapp_pos_models.reports.InvoicePdfBean;
import com.adapp.adapp_pos_models.reports.InvoicePdfBean.Order;
import com.adapp.adapp_pos_models.reports.DocumentData;
import com.adapp.adapp_pos_models.reports.ReportRequest;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.BadRequestException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.adapp.adapp_pos_models.invoices.Invoice.TransactionType.DELIVERY;
import static com.adapp.adapp_pos_models.invoices.Invoice.TransactionType.DINE_IN;

@Component
public class ReportDataMapper {

	private static final String DINE_IN = "DINE IN";
	@Value("${INVOICE_TEMPLATE_PATH}")
	String invoiceTemplatePath;
	@Value("${INVOICE_RECEIPT_TEMPLATE_PATH}")
	String invoiceReceiptTemplatePath;
	@Value("${DAILY_SALES_REPORT_TEMPLATE_PATH}")
	String dailySalesReportTemplatePath;

	BranchDao branchDao;

	@Autowired
	private ReportDataMapper(BranchDao branchDao) {
		super();
		this.branchDao = branchDao;
	}

	public DocumentData map(ReportRequest reportRequest, ReportType reportType) {
		switch (reportType) {
			case INVOICE:
				return buildInvoicePdfBean(reportRequest.getInvoice(),invoiceTemplatePath);
			case INVOICE_RECEIPT:
				return buildInvoicePdfBean(reportRequest.getInvoice(),invoiceReceiptTemplatePath);
			case DAILY_SALES_REPORT:
				return buildDailySalesReportBean(reportRequest.getInvoicesForDailyReport(), reportRequest.getOrderedProducts());
			default:
				throw new BadRequestException(String.format("Invalid Report Type %s", reportType));
		}
	}

	private InvoicePdfBean buildInvoicePdfBean(Invoice invoice, String template) {
		InvoicePdfBean invoicePdfBean = new InvoicePdfBean(template);
		StringBuilder addressStringBuilder = new StringBuilder();
		if (invoice.getTransactionType() == DELIVERY) {
			invoicePdfBean.setBillTo(invoice.getCustomer().getName());
			addressStringBuilder.append(invoice.getCustomer().getAddress().getAddressLine1()).append(" ");
			addressStringBuilder.append(invoice.getCustomer().getAddress().getCity()).append(" ");
			addressStringBuilder.append(invoice.getCustomer().getAddress().getProvince()).append(" ");
			addressStringBuilder.append(invoice.getCustomer().getAddress().getZipCode()).append(" ");
			invoicePdfBean.setBillingAddress(addressStringBuilder.toString());
			invoicePdfBean.setEmail(invoice.getCustomer().getContactInformation().getEmailAddress());
			invoicePdfBean.setContactNumber(invoice.getCustomer().getContactInformation().getPhoneNumber());
			if(invoice.getRiderDetails() != null) {
				invoicePdfBean.setRider(String.format("%s %s",
						invoice.getRiderDetails().getPersonalDetails().getFirstName(),
						invoice.getRiderDetails().getPersonalDetails().getLastName()));
			} else {
				invoicePdfBean.setRider("No Rider assigned");
			}
			if(invoice.getDeliveryCharge() != null) {
				invoicePdfBean.setDeliveryCharge(invoice.getDeliveryCharge().getCharge().toPlainString());
			} else {
				invoicePdfBean.setDeliveryCharge("0");
			}
			invoicePdfBean.setDeliveryTime(invoice.getDeliveryDate().toString(DateTimeFormat.forPattern("MMM/dd/yyyy hh:mm:ssa")));
		} else {
			invoicePdfBean.setBillTo(String.format("Table number %s", invoice.getTableNumber()));
			invoicePdfBean.setBillingAddress("");
			invoicePdfBean.setEmail("");
			invoicePdfBean.setContactNumber("");
			invoicePdfBean.setRider(Invoice.TransactionType.DINE_IN.toString());
			invoicePdfBean.setDeliveryTime(Invoice.TransactionType.DINE_IN.toString());
		}
		Branch branch = branchDao.getBranchByBranchId(invoice.getBranchId());
		invoicePdfBean.setBranchAddress(branch.getAddress().getCity());
		invoicePdfBean.setInvoiceNumber(invoice.getNumber());
		invoicePdfBean.setInvoiceDate(invoice.getInvoiceDate().toString(DateTimeFormat.forPattern("MMM/dd/yyyy hh:mm:ssa")));
		invoicePdfBean.setAmountDue(invoice.getAmountDue().toPlainString());
		for (Item item : invoice.getItems()) {
			Order order = new Order();
			order.setQuantity(String.valueOf(item.getQuantity()));
			order.setPrice(item.getProduct().getPrice().toPlainString());
			order.setDescription(item.getProduct().getDescription());
			order.setName(item.getProduct().getName());
			order.setAmount(item.getProduct().getPrice().multiply(new BigDecimal(item.getQuantity())).toPlainString());
			invoicePdfBean.getOrders().add(order);
		}
		invoicePdfBean.setRequestDate(new DateTime());
		invoicePdfBean.setNotes(invoice.getNotes());
		invoicePdfBean.setDiscount(invoice.getDiscount().toPlainString());
		invoicePdfBean.setRemarks(invoice.getRemarks());
		invoicePdfBean.setServiceCharge(invoice.getServiceCharge() == null ? "0" : invoice.getServiceCharge().toPlainString());
		return invoicePdfBean;
	}

	private DailySalesReportBean buildDailySalesReportBean(List<Invoice> invoices, List<OrderedProduct> orderedProducts) {
		DailySalesReportBean dailySalesReportBean = new DailySalesReportBean(dailySalesReportTemplatePath);
		dailySalesReportBean.setOrderedProducts(orderedProducts);
		List<InvoiceDailySalesBean> invoiceDailySalesBeans = new ArrayList<>();
		for (Invoice invoice : invoices) {
			InvoiceDailySalesBean invoiceDailySalesBean = new InvoiceDailySalesBean();
			invoiceDailySalesBean.setInvoiceDate(invoice.getInvoiceDate().toString(DateTimeFormat.forPattern("MMM/dd/yyyy hh:mm:ssa")));
			invoiceDailySalesBean.setInvoiceNumber(invoice.getNumber());
			if (invoice.getPaymentDetails() != null) {
				invoiceDailySalesBean.setPaymentMethod(invoice.getPaymentDetails().getPaymentMethod().toString());
				invoiceDailySalesBean.setBalance(invoice.getAmountDue().subtract(invoice.getPaymentDetails().getAmount()).toPlainString());
			} else {
				invoiceDailySalesBean.setPaymentMethod("No payment details");
				invoiceDailySalesBean.setBalance(invoice.getAmountDue().toPlainString());
			}
			invoiceDailySalesBean.setOriginalDue(invoice.getAmountDue().add(invoice.getDiscount()).toPlainString());
			invoiceDailySalesBean.setDiscount(invoice.getDiscount().toPlainString());
			invoiceDailySalesBean.setAmountDue(invoice.getAmountDue().toPlainString());
			invoiceDailySalesBean.setServiceCharge(invoice.getServiceCharge().toPlainString());
			Optional<Note> optionalNote = invoice.getNotes().stream().filter(note -> note.getType().equals("Others")).findFirst();
			if(optionalNote.isPresent()) {
				invoiceDailySalesBean.setRemarks(optionalNote.get().getMessage());
			} else {
				invoiceDailySalesBean.setRemarks("");
			}
			if(DELIVERY == invoice.getTransactionType()) {
				if(invoice.getRiderDetails() != null) {
					invoiceDailySalesBean.setRider(String.format("%s %s",
							invoice.getRiderDetails().getPersonalDetails().getFirstName(),
							invoice.getRiderDetails().getPersonalDetails().getLastName()));
				} else {
					invoiceDailySalesBean.setRider("No Rider assigned");
				}
				invoiceDailySalesBean.setCustomer(invoice.getCustomer().getName());
				invoiceDailySalesBean.setLocation(invoice.getCustomer().getAddress().getCity());
				if (invoice.getDeliveryCharge() != null) {
					invoiceDailySalesBean.setDeliveryCode(invoice.getDeliveryCharge().getItemCode());
					invoiceDailySalesBean.setIncentive(invoice.getDeliveryCharge().getIncentive().toPlainString());
				} else {
					invoiceDailySalesBean.setDeliveryCode("None Selected");
					invoiceDailySalesBean.setIncentive("None Selected");
				}
				int deliveryNumberOfPax = 0;
				for (Item item : invoice.getItems()) {
					deliveryNumberOfPax = deliveryNumberOfPax + item.getProduct().getNumberOfPax();
				}
				invoiceDailySalesBean.setNumberOfPax(String.valueOf(deliveryNumberOfPax));
			} else {
				invoiceDailySalesBean.setRider(DINE_IN);
				invoiceDailySalesBean.setCustomer(String.format("Table number %s", invoice.getTableNumber()));
				invoiceDailySalesBean.setLocation(DINE_IN);
				invoiceDailySalesBean.setDeliveryCode(DINE_IN);
				invoiceDailySalesBean.setIncentive(DINE_IN);
				invoiceDailySalesBean.setNumberOfPax(String.valueOf(invoice.getNumberOfPax()));
			}
			invoiceDailySalesBeans.add(invoiceDailySalesBean);
			computeTotalSales(invoice, dailySalesReportBean);
		}
		dailySalesReportBean.setInvoiceDailySalesBeans(invoiceDailySalesBeans);
		return dailySalesReportBean;
	}

	private void computeTotalSales(Invoice invoice, DailySalesReportBean dailySalesReportBean) {
		if (invoice.getPaymentDetails() != null) {
			switch (invoice.getPaymentDetails().getPaymentMethod()) {
				case CASH:
					dailySalesReportBean
							.setTotalCash(
									dailySalesReportBean.getTotalCash()
											.add(invoice.getPaymentDetails().getAmount()));
					break;
				case GCASH:
					dailySalesReportBean
							.setTotalGCash(
									dailySalesReportBean.getTotalGCash()
											.add(invoice.getPaymentDetails().getAmount()));
					break;
				case BANK_TRANSFER:
					dailySalesReportBean
							.setTotalBankTransfer(
									dailySalesReportBean.getTotalBankTransfer()
											.add(invoice.getPaymentDetails().getAmount()));
					break;
				case FOOD_PANDA:
					dailySalesReportBean
							.setTotalFoodPanda(
									dailySalesReportBean.getTotalFoodPanda()
											.add(invoice.getPaymentDetails().getAmount()));
					break;
				case PETTY_CASH:
					dailySalesReportBean
							.setTotalPettyCash(
									dailySalesReportBean.getTotalPettyCash()
											.add(invoice.getPaymentDetails().getAmount()));
					break;
				case LALAMOVE:
					dailySalesReportBean
							.setTotalLalamove(
									dailySalesReportBean.getTotalLalamove()
											.add(invoice.getPaymentDetails().getAmount()));
					break;
				case GRAB:
					dailySalesReportBean
							.setTotalGrab(
									dailySalesReportBean.getTotalGrab()
											.add(invoice.getPaymentDetails().getAmount()));
					break;
				case METRO_DEAL:
					dailySalesReportBean
							.setTotalMetroDeal(
									dailySalesReportBean.getTotalMetroDeal()
											.add(invoice.getPaymentDetails().getAmount()));
					break;
				default:
					dailySalesReportBean
							.setTotalOther(
									dailySalesReportBean.getTotalOther()
											.add(invoice.getPaymentDetails().getAmount()));
					break;
			}

			dailySalesReportBean.setTotalBalance(
					dailySalesReportBean.getTotalBalance()
							.add(invoice.getAmountDue().subtract(invoice.getPaymentDetails().getAmount()))
			);
		}else {
			dailySalesReportBean
					.setTotalBalance(
							dailySalesReportBean.getTotalBalance()
									.add(invoice.getAmountDue()));
		}

		dailySalesReportBean.setTotalSales(
				dailySalesReportBean.getTotalSales()
						.add(invoice.getAmountDue())
		);

		dailySalesReportBean.setTotalDiscounts(
				dailySalesReportBean.getTotalDiscounts()
						.add(invoice.getDiscount())
		);

		dailySalesReportBean.setTotalServiceCharge(
				dailySalesReportBean.getTotalServiceCharge()
						.add(invoice.getServiceCharge())
		);

		dailySalesReportBean.setOriginalTotal(
				dailySalesReportBean.getOriginalTotal()
						.add(invoice.getAmountDue().add(invoice.getDiscount()))
		);
	}
}
