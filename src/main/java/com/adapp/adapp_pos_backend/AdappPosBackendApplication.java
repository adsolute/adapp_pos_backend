package com.adapp.adapp_pos_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
public class AdappPosBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdappPosBackendApplication.class, args);
	}

}
