package com.adapp.adapp_pos_backend.utils;

import com.adapp.adapp_pos_models.audit.AuditMessage;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.invoices.Invoice;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;

import static com.adapp.adapp_pos_models.audit.AuditMessage.AuditAction.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BackendKafkaAuditSenderTest {

    @InjectMocks
    BackendKafkaAuditSender<Object> classUnderTest;

    @Mock
    KafkaTemplate<String, Object> kafkaTemplate;

    @Mock
    Subject subject;

    @BeforeEach
    public void setup (){
        SubjectThreadState threadState = new SubjectThreadState(subject);
        threadState.bind();
    }

    @Test
    void sendCreateMessage() {
        classUnderTest.send(Resource.INVOICES, "objectId", "accountId", "name", "identifier");
        ArgumentCaptor<AuditMessage<Object>> auditMessageArgumentCaptor = ArgumentCaptor.forClass(AuditMessage.class);
        verify(kafkaTemplate, times(1)).send(eq("audit"), auditMessageArgumentCaptor.capture());
        AuditMessage<Object> sentMessage =  auditMessageArgumentCaptor.getValue();
        assertEquals(CREATE, sentMessage.getAuditAction());
    }

    @Test
    void sendUpdateMessage() {
        classUnderTest.send(Resource.INVOICES, "objectId", "accountId", "name", "identifier", new Invoice(), new Invoice());
        ArgumentCaptor<AuditMessage<Object>> auditMessageArgumentCaptor = ArgumentCaptor.forClass(AuditMessage.class);
        verify(kafkaTemplate, times(1)).send(eq("audit"), auditMessageArgumentCaptor.capture());
        AuditMessage<Object> sentMessage =  auditMessageArgumentCaptor.getValue();
        assertEquals(UPDATE, sentMessage.getAuditAction());
    }

    @Test
    void sendDeleteMessage() {
        classUnderTest.send(Resource.INVOICES, "accountId", "name", "identifier");
        ArgumentCaptor<AuditMessage<Object>> auditMessageArgumentCaptor = ArgumentCaptor.forClass(AuditMessage.class);
        verify(kafkaTemplate, times(1)).send(eq("audit"), auditMessageArgumentCaptor.capture());
        AuditMessage<Object> sentMessage =  auditMessageArgumentCaptor.getValue();
        assertEquals(DELETE, sentMessage.getAuditAction());
    }

}