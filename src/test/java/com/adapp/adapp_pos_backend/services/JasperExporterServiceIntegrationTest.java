package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_models.enums.DocType;
import com.adapp.adapp_pos_models.reports.DocumentData;
import com.adapp.adapp_pos_models.reports.InvoicePdfBean;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.print.Doc;
import javax.ws.rs.InternalServerErrorException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class JasperExporterServiceIntegrationTest {

    @Autowired
    JasperExporterService jasperExporterService;

    @Value("${INVOICE_TEMPLATE_PATH}")
    String invoiceTemplatePath;

    @Test
    void export() throws Exception{
        InputStream inputStream = new FileInputStream(new File(invoiceTemplatePath));
        ByteArrayOutputStream byteArrayOutputStream =  new ByteArrayOutputStream();
        jasperExporterService.export(inputStream, byteArrayOutputStream, new InvoicePdfBean(invoiceTemplatePath), DocType.PDF);
        assertTrue(byteArrayOutputStream.toByteArray().length > 0);
    }

    @Test
    void exportFailedWrongDocumentDataBean() throws Exception{
        InputStream inputStream = new FileInputStream(new File(invoiceTemplatePath));
        ByteArrayOutputStream byteArrayOutputStream =  new ByteArrayOutputStream();
        DocumentData wrongDocumentDataBean = new DocumentData(invoiceTemplatePath);
        assertThrows(InternalServerErrorException.class, ()
                -> jasperExporterService.export(inputStream, byteArrayOutputStream, wrongDocumentDataBean, DocType.PDF));
    }

}