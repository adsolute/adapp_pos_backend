package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_models.enums.DocType;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.Exporter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class JasperReportExporterFactoryTest {

    @Test
    void getPDFExporter() {
        Exporter exporter = new JasperReportExporterFactory().getExporter(DocType.PDF);
        assertTrue(exporter instanceof JRPdfExporter);
    }

    @Test
    void getXLSExporter() {
        Exporter exporter = new JasperReportExporterFactory().getExporter(DocType.XLS);
        assertTrue(exporter instanceof JRXlsxExporter);
    }

    @Test
    void getDOCXExporter() {
        Exporter exporter = new JasperReportExporterFactory().getExporter(DocType.DOCX);
        assertTrue(exporter instanceof JRDocxExporter);
    }

    @Test
    void getCSVXExporter() {
        Exporter exporter = new JasperReportExporterFactory().getExporter(DocType.CSV);
        assertTrue(exporter instanceof JRCsvExporter);
    }

    @Test
    void getExporterFailed() {
        JasperReportExporterFactory jasperReportExporterFactory = new JasperReportExporterFactory();
        assertThrows(ServerException.class, () -> jasperReportExporterFactory.getExporter(DocType.INVALID_FORMAT));
    }

}