package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.dao.BranchDao;
import com.adapp.adapp_pos_backend.utils.BackendKafkaAuditSender;
import com.adapp.adapp_pos_models.Address;
import com.adapp.adapp_pos_models.ContactInformation;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.profiles.Branch;
import com.adapp.adapp_pos_models.profiles.BranchDTO;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BranchServiceTest {

	private static final String MONGO_CONNECTION_ERROR = "Connection Error";
	private static final String BRANCH_NAME = "Branch Name";
	private static final String CITY = "City";
	private static final String EMAIL = "Email";
	private static final String PHONE_NUMBER = "Phone Number";
	public static final String BRANCH_ID = "branch id";
	public static final String ID = "ID";

	@InjectMocks
	BranchService classUnderTest;

	@Mock
	BranchDao branchDao;

	@Mock
	BackendKafkaAuditSender<Branch> backendKafkaAuditSender;

	@Test
	public void getAllBranchesSucceed() {
		List<Branch> branchList = new ArrayList<>();

		givenThatThereAreSavedBranches(branchList);
		when(branchDao.getAllBranches()).thenReturn(branchList);
		List<BranchDTO> branchDTOList = classUnderTest.getAllBranches();
		assertTrue(!branchDTOList.isEmpty());
		assertEquals(BRANCH_NAME, branchDTOList.get(0).getName());
		assertEquals(CITY, branchDTOList.get(0).getAddress());
		assertEquals(EMAIL, branchDTOList.get(0).getEmail());
		assertEquals(PHONE_NUMBER, branchDTOList.get(0).getPhone());
		verify(branchDao, times(1)).getAllBranches();
	}

	private void givenThatThereAreSavedBranches(List<Branch> retrievedBranches) {
		Branch branch = new Branch();
		branch.setName(BRANCH_NAME);
		branch.setAddress(new Address());
		branch.getAddress().setCity(CITY);
		branch.setContactInformation(new ContactInformation());
		branch.getContactInformation().setEmailAddress(EMAIL);
		branch.getContactInformation().setPhoneNumber(PHONE_NUMBER);
		branch.setCreateDate(new DateTime());
		retrievedBranches.add(branch);
	}

	@Test
	public void getAllBranchesSucceedWithEmptyList() {
		List<BranchDTO> branches = classUnderTest.getAllBranches();
		assertTrue(branches.isEmpty());
		verify(branchDao, times(1)).getAllBranches();
	}

	@Test
	public void saveBranchSucceed() {
		Branch savedBranch = new Branch();
		savedBranch.setId(ID);
		given(branchDao.saveThenReturn(any(Branch.class))).willReturn(savedBranch);
		Branch branchToSave = new Branch();
		branchToSave.setName(BRANCH_NAME);
		classUnderTest.save(branchToSave);
		verify(branchDao).saveThenReturn(any(Branch.class));
		verify(backendKafkaAuditSender, times(1)).send(any(Resource.class), anyString(), anyString(), anyString(), anyString());
	}

	@Test
	public void getBranchDTOPagedResultSetSucceed() {
		PagedResultSet<Branch> branchPagedResultSet = new PagedResultSet<>();
		List<Branch> branchList = new ArrayList<>();
		givenThatThereAreSavedBranches(branchList);
		branchPagedResultSet.setResults(branchList);
		given(branchDao.getPagedResultSet(any(QueryParameters.class))).willReturn(branchPagedResultSet);

		PagedResultSet<BranchDTO> retrievedPagedResultSet = classUnderTest.getBranchDTOPagedResultSetBy(new QueryParameters());
		List<BranchDTO> branchDTOList = retrievedPagedResultSet.getResults();

		verify(branchDao, times(1)).getPagedResultSet(any(QueryParameters.class));
		assertNotNull(retrievedPagedResultSet);
		assertFalse(branchDTOList.isEmpty());
		assertEquals(branchList.get(0).getName(), branchDTOList.get(0).getName());
		assertEquals(branchList.get(0).getId(), branchDTOList.get(0).getId());
		assertEquals(branchList.get(0).getBranchId(), branchDTOList.get(0).getBranchId());
		assertEquals(branchList.get(0).getContactInformation().getPhoneNumber(), branchDTOList.get(0).getPhone());
		assertEquals(branchList.get(0).getContactInformation().getEmailAddress(), branchDTOList.get(0).getEmail());
		assertEquals(branchList.get(0).getCreateDate(), branchDTOList.get(0).getCreateDate());
	}

	@Test
	public void getBranchDTOPagedResultSetSucceedWithEmptyResults() {
		given(branchDao.getPagedResultSet(any(QueryParameters.class))).willReturn(new PagedResultSet<>());

		PagedResultSet<BranchDTO> retrievedPagedResultSet = classUnderTest.getBranchDTOPagedResultSetBy(new QueryParameters());
		List<BranchDTO> branchDTOList = retrievedPagedResultSet.getResults();

		verify(branchDao, times(1)).getPagedResultSet(any(QueryParameters.class));
		assertNotNull(retrievedPagedResultSet);
		assertTrue(branchDTOList.isEmpty());
	}

	@Test
	public void getBranchDTOListSucceed() {
		PagedResultSet pagedResultSet = new PagedResultSet<>();
		List<Branch> branches = new ArrayList<>();
		givenThatThereAreSavedBranches(branches);
		pagedResultSet.setResults(branches);
		given(branchDao.getPagedResultSet(any(QueryParameters.class))).willReturn(pagedResultSet);
		List<BranchDTO> branchDTOList = classUnderTest.getBranchDTOList(new QueryParameters());

		assertNotNull(branchDTOList);
		assertFalse(branchDTOList.isEmpty());
	}

	@Test
	public void getBranchDTOListSucceedWithEmptyList() {
		given(branchDao.getPagedResultSet(any(QueryParameters.class))).willReturn(new PagedResultSet<>());
		List<BranchDTO> branchDTOList = classUnderTest.getBranchDTOList(new QueryParameters());

		assertNotNull(branchDTOList);
		assertTrue(branchDTOList.isEmpty());
	}

	@Test
	public void getBranchSucceed() {
		given(branchDao.getBranch(anyString())).willReturn(new Branch());
		Branch branch = classUnderTest.getBranch(BRANCH_ID);
		assertNotNull(branch);
	}

	@Test
	public void getBranchSucceedWithNullResult() {
		Branch branch = classUnderTest.getBranch(BRANCH_ID);
		assertNull(branch);
	}

	@Test
	public void getBranchByBranchIdSucceed() {
		given(branchDao.getBranchByBranchId(anyString())).willReturn(new Branch());
		Branch branch = classUnderTest.getBranchByBranchId(BRANCH_ID);
		assertNotNull(branch);
	}

	@Test
	public void getBranchByBranchIdSucceedWithNullResult() {
		Branch branch = classUnderTest.getBranchByBranchId(BRANCH_ID);
		assertNull(branch);
	}

	@Test
	public void updateSucceed() {
		Branch branchToUpdate = new Branch();
		branchToUpdate.setId(ID);
		branchToUpdate.setName(BRANCH_NAME);
		branchToUpdate.setBranchId(BRANCH_ID);
		given(branchDao.findAndReplace(any(Branch.class))).willReturn(new Branch());

		classUnderTest.update(branchToUpdate);

		ArgumentCaptor<Branch> branchArgumentCaptor = ArgumentCaptor.forClass(Branch.class);
		verify(branchDao, times(1)).findAndReplace(branchArgumentCaptor.capture());
		Branch updatedBranch = branchArgumentCaptor.getValue();
		assertNotNull(updatedBranch.getUpdateDate());
		verify(backendKafkaAuditSender, times(1)).send(any(Resource.class), anyString(), anyString(), anyString(), anyString(), any(Branch.class), any(Branch.class));
	}

	@Test
	public void removeSucceed() {
		Branch branchToRemove = new Branch();
		branchToRemove.setName(BRANCH_NAME);
		branchToRemove.setBranchId(BRANCH_ID);
		given(branchDao.getBranch(anyString())).willReturn(branchToRemove);
		classUnderTest.remove(BRANCH_ID);
		verify(branchDao, times(1)).remove(any(Branch.class));
		verify(backendKafkaAuditSender, times(1)).send(any(), anyString(), anyString(), anyString());
	}

	@Test
	public void removeFailedNotFound() {
		given(branchDao.getBranch(anyString())).willReturn(null);
		assertThrows(NotFoundException.class, () -> classUnderTest.remove(BRANCH_ID));
	}
}