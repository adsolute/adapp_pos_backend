package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_backend.builders.ReportDataMapper;
import com.adapp.adapp_pos_backend.dao.BaseDao;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.dao.DocumentDataDao;
import com.adapp.adapp_pos_backend.dao.InvoiceDao;
import com.adapp.adapp_pos_models.enums.DocType;
import com.adapp.adapp_pos_models.enums.ReportType;
import com.adapp.adapp_pos_models.reports.DocumentData;
import com.adapp.adapp_pos_models.reports.InvoicePdfBean;
import com.adapp.adapp_pos_models.reports.InvoiceSalesReport;
import com.adapp.adapp_pos_models.reports.ReportRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
public class ReportServiceTest {

	public static final int UUID_LENGTH = 36;
	@InjectMocks
	ReportService classUnderTest;

	@Mock
	ReportDataMapper reportDataMapper;

	@Mock
	DocumentDataDao documentDataDao;

	@Mock
	JasperExporterService jasperExporterService;

	@Mock
	InvoiceDao invoiceDao;

	@Test
	public void saveDocumentDataSucceed() {
		given(reportDataMapper.map(any(ReportRequest.class), any(ReportType.class))).willReturn(new DocumentData(""));
		String documentDataId = classUnderTest.saveDocumentData(new ReportRequest(), ReportType.INVOICE);
		verify(documentDataDao, times(1)).save(any(DocumentData.class));
		assertNotNull(documentDataId);
		assertTrue(documentDataId.length() == UUID_LENGTH);
	}


	@Test
	public void saveDocumentDataInvalidData() {
		given(reportDataMapper.map(any(ReportRequest.class), any(ReportType.class))).willThrow(NullPointerException.class);
		assertThrows(BadRequestException.class, () -> classUnderTest.saveDocumentData(new ReportRequest(), ReportType.INVOICE));
	}

	@Test
	public void generateReportByteArraySucceed() {
		given(documentDataDao.getByDocumentDataId(anyString())).willReturn(new InvoicePdfBean("src/main/resources/jasper-templates/InvoiceTemplate.jrxml"));
		byte[] reportByteArray = classUnderTest.generateReportByteArray("documantDataId", DocType.PDF);
		verify(jasperExporterService, times(1)).export(any(InputStream.class), any(ByteArrayOutputStream.class), any(DocumentData.class), any(DocType.class));
	}

	@Test
	public void generateReportByteArrayTemplateNotFound() {
		given(documentDataDao.getByDocumentDataId(anyString())).willReturn(new InvoicePdfBean("not existing file"));
		assertThrows(NotFoundException.class, () -> classUnderTest.generateReportByteArray("documantDataId", DocType.PDF));
	}

	@Test
	public void generateReportByteArrayJasperFailed() {
		given(documentDataDao.getByDocumentDataId(anyString())).willReturn(new InvoicePdfBean("src/main/resources/jasper-templates/InvoiceTemplate.jrxml"));
		willThrow(InternalServerErrorException.class).given(jasperExporterService).export(any(InputStream.class), any(ByteArrayOutputStream.class), any(DocumentData.class), any(DocType.class));
		assertThrows(InternalServerErrorException.class, () -> classUnderTest.generateReportByteArray("documantDataId", DocType.PDF));
	}

	@Test
	public void getInvoiceSalesReportSucceed() {
		InvoiceSalesReport invoiceSalesReport = new InvoiceSalesReport();
		invoiceSalesReport.setTotalOrders(110.0);
		invoiceSalesReport.setTotalSales(new BigDecimal("123653"));
		given(invoiceDao.aggregateInvoiceSalesReport(any(QueryParameters.class)))
				.willReturn(Arrays.asList(invoiceSalesReport));
		InvoiceSalesReport generatedInvoiceReport = classUnderTest.getInvoiceSalesReport(new QueryParameters());
		assertEquals(invoiceSalesReport.getTotalOrders(), generatedInvoiceReport.getTotalOrders());
		assertEquals(invoiceSalesReport.getTotalSales(), generatedInvoiceReport.getTotalSales());
	}

	@Test
	public void getInvoiceSalesReportSucceedWithEmptyResult() {
		given(invoiceDao.aggregateInvoiceSalesReport(any(QueryParameters.class)))
				.willReturn(Collections.emptyList());
		InvoiceSalesReport generatedInvoiceReport = classUnderTest.getInvoiceSalesReport(new QueryParameters());
		assertEquals(null, generatedInvoiceReport.getTotalOrders());
		assertEquals(null, generatedInvoiceReport.getTotalSales());
	}

	@Test
	public void saveDailySalesReportDocumentDataSucceed() {
		given(invoiceDao.getInvoicesBy(any(QueryParameters.class))).willReturn(new BaseDao.PagedResultSet<>());
		given(reportDataMapper.map(any(ReportRequest.class), any(ReportType.class))).willReturn(new DocumentData(""));

		classUnderTest.saveDailySalesReportDocumentData(new QueryParameters());
		verify(documentDataDao, times(1)).save(any(DocumentData.class));
	}

	@Test
	public void saveDailySalesReportDocumentDataFailed() {
		given(invoiceDao.getInvoicesBy(any(QueryParameters.class))).willReturn(new BaseDao.PagedResultSet<>());

		assertThrows(BadRequestException.class, () -> classUnderTest.saveDailySalesReportDocumentData(new QueryParameters()));
	}
}