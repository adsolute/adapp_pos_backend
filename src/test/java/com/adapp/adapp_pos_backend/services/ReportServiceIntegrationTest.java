package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_models.Address;
import com.adapp.adapp_pos_models.ContactInformation;
import com.adapp.adapp_pos_models.customers.Customer;
import com.adapp.adapp_pos_models.enums.DocType;
import com.adapp.adapp_pos_models.enums.ReportType;
import com.adapp.adapp_pos_models.invoices.Invoice;
import com.adapp.adapp_pos_models.products.Product;
import com.adapp.adapp_pos_models.reports.ReportRequest;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;


@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ReportServiceIntegrationTest {

	@Autowired
	ReportService reportService;

	@Test
	public void saveDocumentData() {
		Invoice invoice = new Invoice();
		Product product = new Product();
		product.setName("SSB1");
		product.setDescription("Good for 2 Pax\n" +
				"Meat: 250g   Pork, 250g   Beef\n" +
				"Side Dishes:  \n" +
				"130g   Kimchi\n" +
				"160g   Sweet Baby Potatoes\n" +
				"100g   Cucumber\n" +
				"2sets   Korean Egg\n" +
				"2sets   Korean Braised Tofu\n" +
				"2cups  Rice\n" +
				"120g  Lettuce\n" +
				"20g Garlic\n" +
				"Sauces:\n" +
				"30g Bean Paste\n" +
				"10g Sesame Oil    \n");
		product.setPrice(new BigDecimal("100"));
		Invoice.Item item = new Invoice.Item();
		item.setProduct(product);
		item.setQuantity(2);
		invoice.getItems().add(item);
		invoice.setCustomer(new Customer());
		invoice.getCustomer().setName("Customer");
		invoice.getCustomer().setAddress(new Address());
		invoice.setNumber("2020-0000001");
		invoice.setInvoiceDate(new DateTime());
		invoice.setAmountDue(new BigDecimal("1000"));
		invoice.getCustomer().setContactInformation(new ContactInformation());
		invoice.setDeliveryDate(new DateTime());
		invoice.setTransactionType(Invoice.TransactionType.DINE_IN);
		invoice.setTableNumber("4");
		ReportRequest reportRequest = new ReportRequest();
		reportRequest.setInvoicesForDailyReport(Arrays.asList(invoice));
		try {
			String documentDataId =  reportService.saveDocumentData(reportRequest, ReportType.DAILY_SALES_REPORT);
			System.out.println(documentDataId);
			assertNotNull(documentDataId);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void generateReportByteArray () {
		try {
			File file = new File("dailysalesreport.xlsx");
			byte[] documentByteArray = reportService.generateReportByteArray("29ea6ab4-6ba3-40ad-8fb9-cb67564ae277", DocType.XLS);
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			fileOutputStream.write(documentByteArray);
			fileOutputStream.flush();
			fileOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

}