package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.dao.ProductDao;
import com.adapp.adapp_pos_backend.utils.BackendKafkaAuditSender;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.products.Product;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

	public static final String PRODUCT_NAME = "Product Name";
	public static final String PRODUCT_ID = "product id";
	public static final String PRODUCT_OBJECT_ID = "product object id";
	public static final String PRODUCT_CODE = "productCode";
	public static final String CODE = "Code";
	@InjectMocks
	ProductService classUnderTest;

	@Mock
	ProductDao productDao;

	@Mock
	BackendKafkaAuditSender<Product> backendKafkaAuditSender;

	@Test
	public void getAllProductsSucceed() {
		given(productDao.getAllProducts()).willReturn(Arrays.asList(new Product()));
		List<Product> products = classUnderTest.getAllProducts();
		verify(productDao, times(1)).getAllProducts();
		assertTrue(products.size() > 0);
	}

	@Test
	public void getAllProductsSucceedWithEmptyList() {
		List<Product> products = classUnderTest.getAllProducts();
		verify(productDao, times(1)).getAllProducts();
		assertTrue(products.isEmpty());
	}

	@Test
	public void saveSucceed() {
		Product savedProduct = new Product();
		savedProduct.setId(PRODUCT_OBJECT_ID);
		savedProduct.setProductCode(PRODUCT_CODE);
		given(productDao.saveAndReturn(any(Product.class))).willReturn(savedProduct);

		classUnderTest.save(new Product());

		ArgumentCaptor<String> idArgumentCaptor = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> identifierNameArgumentCaptor = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> identifierArgumentCaptor = ArgumentCaptor.forClass(String.class);
		verify(productDao, times(1)).saveAndReturn(any(Product.class));
		verify(backendKafkaAuditSender, times(1))
				.send(any(Resource.class), idArgumentCaptor.capture(), eq(null), identifierNameArgumentCaptor.capture(), identifierArgumentCaptor.capture());
		assertEquals(PRODUCT_OBJECT_ID, idArgumentCaptor.getValue());
		assertEquals(CODE, identifierNameArgumentCaptor.getValue());
		assertEquals(PRODUCT_CODE, identifierArgumentCaptor.getValue());
	}

	@Test
	public void updateSucceed() {
		Product productForUpdate = new Product();
		productForUpdate.setProductCode(CODE);
		productForUpdate.setId(PRODUCT_OBJECT_ID);

		given(productDao.findAndReplace(any(Product.class))).willReturn(new Product());
		classUnderTest.update(productForUpdate);
		verify(productDao, times(1)).findAndReplace(any(Product.class));
		verify(backendKafkaAuditSender, times(1))
				.send(any(Resource.class), anyString(), eq(null), anyString(), anyString(), any(Product.class), any(Product.class));
	}


	@Test
	public void getProductsByNameSucceed() {
		List<Product> retrievedProducts = new ArrayList<>();

		givenThatProductsHaveSameName(retrievedProducts);
		given(productDao.getProducts(anyString())).willReturn(retrievedProducts);
		List<Product> products = classUnderTest.getProducts(PRODUCT_NAME);
		assertTrue(!products.isEmpty());
		assertTrue(products.get(0).getName().contains(PRODUCT_NAME));
	}

	private void givenThatProductsHaveSameName(List<Product> retrievedProducts) {
		Product product = new Product();
		product.setName("Product Name of this Product");
		retrievedProducts.add(product);
	}

	@Test
	public void getProductsByNameSucceedWithEmptyList() {
		given(productDao.getProducts(anyString())).willReturn(Collections.emptyList());
		List<Product> products = classUnderTest.getProducts(PRODUCT_NAME);
		assertTrue(products.isEmpty());
	}


	@Test
	public void getProductsByQuerySucceed() {
		PagedResultSet<Product> retrievedPagedResult = new PagedResultSet<>();

		givenThatProductsAreRetrieved(retrievedPagedResult);
		given(productDao.getProductsBy(any(QueryParameters.class))).willReturn(retrievedPagedResult);
		PagedResultSet<Product> productPagedResultSet = classUnderTest.getProductsBy(new QueryParameters());
		assertNotNull(productPagedResultSet);
		assertFalse(productPagedResultSet.getResults().isEmpty());
		verify(productDao, times(1)).getProductsBy(any(QueryParameters.class));
	}

	private void givenThatProductsAreRetrieved(PagedResultSet<Product> retrievedPagedResult) {
		retrievedPagedResult.setResults(new ArrayList<>());
		retrievedPagedResult.getResults().add(new Product());
	}

	@Test
	public void getProductsByQuerySucceedWithEmptyResults() {
		PagedResultSet retrievedPageResult = new PagedResultSet();

		givenThatPagedResultIsEmpty(retrievedPageResult);
		given(productDao.getProductsBy(any(QueryParameters.class))).willReturn(retrievedPageResult);
		PagedResultSet<Product> productPagedResultSet = classUnderTest.getProductsBy(new QueryParameters());
		assertNotNull(productPagedResultSet);
		assertTrue(productPagedResultSet.getResults().isEmpty());
		verify(productDao, times(1)).getProductsBy(any(QueryParameters.class));
	}

	private void givenThatPagedResultIsEmpty(PagedResultSet retrievedPageResult) {
		retrievedPageResult.setResults(new ArrayList());
	}

	@Test
	public void getProductByIdSucceed() {
		given(productDao.getProduct(anyString())).willReturn(new Product());
		Product product = classUnderTest.getProduct(PRODUCT_ID);

		assertNotNull(product);
		verify(productDao, times(1)).getProduct(anyString());
	}

	@Test
	public void getProductSucceedWithNullResult() {
		Product product = classUnderTest.getProduct(PRODUCT_ID);

		assertNull(product);
		verify(productDao, times(1)).getProduct(anyString());
	}

	@Test
	public void removeSucceed() {
		Product productToBeRemoved = new Product();
		productToBeRemoved.setProductCode(CODE);
		given(productDao.getProduct(anyString())).willReturn(productToBeRemoved);
		classUnderTest.remove(PRODUCT_ID);
		verify(productDao, times(1)).remove(any(Product.class));
		verify(backendKafkaAuditSender, times(1)).send(any(Resource.class), eq(null), anyString(), anyString());
	}

	@Test
	public void removeFailedNotFound() {
		given(productDao.getProduct(anyString())).willReturn(null);
		assertThrows(NotFoundException.class, () -> classUnderTest.remove(PRODUCT_ID));
	}
}