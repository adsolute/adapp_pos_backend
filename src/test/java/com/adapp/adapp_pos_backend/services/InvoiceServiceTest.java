package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.dao.BranchDao;
import com.adapp.adapp_pos_backend.dao.InvoiceDao;
import com.adapp.adapp_pos_backend.dao.InvoiceNumberTrackerDao;
import com.adapp.adapp_pos_backend.utils.BackendKafkaAuditSender;
import com.adapp.adapp_pos_models.Address;
import com.adapp.adapp_pos_models.customers.Customer;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.invoices.*;
import com.adapp.adapp_pos_models.invoices.Invoice.RiderDetails;
import com.adapp.adapp_pos_models.payments.PaymentDetails;
import com.adapp.adapp_pos_models.profiles.Branch;
import com.mongodb.MongoException;
import com.mongodb.MongoTimeoutException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.joda.time.DateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.NotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
public class InvoiceServiceTest {

	private static final String INVOICE_ID = "invoiceId";
	private static final String CUSTOMER_NAME = "customer name";
	private static final String BRANCH_ID = "branchId";
	private static final int YEAR_ID = 2020;
	public static final String INVOICE_NUMBER = "INVOICE NUMBER";
	InvoiceService classUnderTest;

	@Mock
	InvoiceDao invoiceDao;

	@Mock
	InvoiceNumberTrackerDao invoiceNumberTrackerDao;

	@Mock
	BranchDao branchDao;

	@Mock
	BackendKafkaAuditSender<Invoice> backendKafkaAuditSender;

	@Mock
	Subject subject;

	@BeforeEach
	public void setup() {
		classUnderTest = new InvoiceService(
				9,
				22,
				invoiceDao,
				invoiceNumberTrackerDao,
				branchDao,
                backendKafkaAuditSender);
		SubjectThreadState threadState = new SubjectThreadState(subject);
		threadState.bind();
	}

	@Test
	public void getAllInvoicesSucceed() {
		given(invoiceDao.getAllInvoices()).willReturn(Arrays.asList(new Invoice(), new Invoice()));
		List<Invoice> invoices = classUnderTest.getAllInvoices();
		assertEquals(2, invoices.size());
	}

	@Test
	public void getAllInvoicesSucceedWithEmptyList() {
		given(invoiceDao.getAllInvoices()).willReturn(Arrays.asList());
		List<Invoice> invoices = classUnderTest.getAllInvoices();
		assertTrue(invoices.isEmpty());
	}

	@Test
	public void getAllInvoicesFailed() {
		given(invoiceDao.getAllInvoices()).willThrow(new MongoException("Connection Error"));
		assertThrows(MongoException.class, () -> classUnderTest.getAllInvoices());
	}

	@Test
	public void saveSucceededForDineInTransaction() {
		Invoice invoice = new Invoice();
		invoice.setTransactionType(Invoice.TransactionType.DINE_IN);
		givenThatNewInvoiceBelongsToABranch(invoice);
		InvoiceNumberTracker invoiceNumberTracker = new InvoiceNumberTracker();
		givenThatInvoiceNumberTrackerWasFoundAndModified(invoiceNumberTracker);
		given(branchDao.getBranchByBranchId(anyString())).willReturn(getTestBranch());
		given(invoiceNumberTrackerDao.findAndModifyCurrentNumber(anyString())).willReturn(invoiceNumberTracker);
		given(invoiceDao.saveAndReturn(any(Invoice.class))).willReturn(invoice);

		classUnderTest.save(invoice);

		ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
		verify(invoiceDao, times(1)).saveAndReturn(invoiceArgumentCaptor.capture());
		Invoice savedInvoice = invoiceArgumentCaptor.getValue();
		assertEquals(savedInvoice.getCreateDate(), savedInvoice.getInvoiceDate());
		assertEquals(savedInvoice.getDueDate(), savedInvoice.getInvoiceDate().plusDays(1));
		assertNotNull(savedInvoice.getDeliveryStatus());
		assertEquals(
				String.format("%s%05d",
						"TB",
						invoiceNumberTracker.getCurrentNumber()),
				savedInvoice.getNumber()
		);
		assertNull(invoiceArgumentCaptor.getValue().getId());
		assertNull(savedInvoice.getUpdateDate());
		assertTrue(savedInvoice.getDeliveryDate().isAfter(DateTime.now()));
	}

	private Branch getTestBranch() {
		Branch branch = new Branch();
		branch.setName("Test Branch");
		return branch;
	}

	@Test
	public void saveSucceededForDeliveryTransaction() {
		Invoice invoice = new Invoice();
		givenThatNewInvoiceBelongsToABranch(invoice);
		givenThatTransactionIsDelivery(invoice);
		givenThatDeliveryDateIsCorrect(invoice);
		InvoiceNumberTracker invoiceNumberTracker = new InvoiceNumberTracker();
		givenThatInvoiceNumberTrackerWasFoundAndModified(invoiceNumberTracker);
		given(branchDao.getBranchByBranchId(anyString())).willReturn(getTestBranch());
		given(invoiceNumberTrackerDao.findAndModifyCurrentNumber(anyString())).willReturn(invoiceNumberTracker);
		given(invoiceDao.saveAndReturn(any(Invoice.class))).willReturn(invoice);

		classUnderTest.save(invoice);

		ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
		verify(invoiceDao, times(1)).saveAndReturn(invoiceArgumentCaptor.capture());
		Invoice savedInvoice = invoiceArgumentCaptor.getValue();
		assertEquals(savedInvoice.getCreateDate(), savedInvoice.getInvoiceDate());
		assertEquals(savedInvoice.getDueDate(), savedInvoice.getInvoiceDate().plusDays(1));
		assertEquals(Invoice.DeliveryStatus.PROCESSING, savedInvoice.getDeliveryStatus());
		assertEquals(
				String.format("%s%05d",
						"TB",
						invoiceNumberTracker.getCurrentNumber()),
				savedInvoice.getNumber()
		);
		assertNull(invoiceArgumentCaptor.getValue().getId());
		assertNull(savedInvoice.getUpdateDate());
	}

	private void givenThatNewInvoiceBelongsToABranch(Invoice invoice) {
		invoice.setBranchId(BRANCH_ID);
	}

	private void givenThatTransactionIsDelivery(Invoice invoice) {
		invoice.setTransactionType(Invoice.TransactionType.DELIVERY);
	}

	private void givenThatDeliveryDateIsCorrect(Invoice invoice) {
		invoice.setDeliveryDate(DateTime.now().withHourOfDay(10));
	}

	private void givenThatInvoiceNumberTrackerWasFoundAndModified(InvoiceNumberTracker invoiceNumberTracker) {
		invoiceNumberTracker.setBranchId(BRANCH_ID);
		invoiceNumberTracker.setCurrentNumber(1);
	}

	@Test
	public void saveFailedDeliveryIsBeforeStartTime() {
		Invoice invoice = new Invoice();
		givenThatNewInvoiceBelongsToABranch(invoice);
		givenThatTransactionIsDelivery(invoice);
		givenThatDeliveryDateIsBeforeStartTime(invoice);
		InvoiceNumberTracker invoiceNumberTracker = new InvoiceNumberTracker();
		givenThatInvoiceNumberTrackerWasFoundAndModified(invoiceNumberTracker);

		assertThrows(BadRequestException.class, () -> classUnderTest.save(invoice));
	}

	private void givenThatDeliveryDateIsBeforeStartTime(Invoice invoice) {
		invoice.setDeliveryDate(DateTime.now().withHourOfDay(8).withMinuteOfHour(59));
	}

	@Test
	public void saveFailedDeliveryIsAfterEndTime() {
		Invoice invoice = new Invoice();
		givenThatNewInvoiceBelongsToABranch(invoice);
		givenThatTransactionIsDelivery(invoice);
		givenThatDeliveryDateIsAfterEndTime(invoice);
		InvoiceNumberTracker invoiceNumberTracker = new InvoiceNumberTracker();
		givenThatInvoiceNumberTrackerWasFoundAndModified(invoiceNumberTracker);

		assertThrows(BadRequestException.class, () -> classUnderTest.save(invoice));
	}

	private void givenThatDeliveryDateIsAfterEndTime(Invoice invoice) {
		invoice.setDeliveryDate(DateTime.now().withHourOfDay(22).withMinuteOfHour(01));
	}

	@Test
	public void updateSucceed() {
		Invoice invoice = givenThatInvoiceIsForUpdate();
		classUnderTest.update(invoice);
		verify(invoiceDao, times(1)).findAndReplace(any(Invoice.class));
	}

	@Test
	public void updateFailedDeliveryTimeIsBeforeStartTime() {
		Invoice invoice = givenThatInvoiceIsForUpdate();
		givenThatDeliveryDateIsBeforeStartTime(invoice);
		invoice.setTransactionType(Invoice.TransactionType.DELIVERY);
		assertThrows(BadRequestException.class, () -> classUnderTest.update(invoice));
	}

	@Test
	public void updateFailedDeliveryTimeIsAfterEndTime() {
		Invoice invoice = givenThatInvoiceIsForUpdate();
		givenThatDeliveryDateIsAfterEndTime(invoice);
		invoice.setTransactionType(Invoice.TransactionType.DELIVERY);
		assertThrows(BadRequestException.class, () -> classUnderTest.update(invoice));
	}

	private Invoice givenThatInvoiceIsForUpdate() {
		Invoice invoice = new Invoice();
		invoice.setId(INVOICE_ID);
		return invoice;
	}

	@Test
	public void updateFailed() {
		assertThrows(NullPointerException.class, () -> classUnderTest.update(new Invoice()));
	}

	@Test
	public void saveFailedInvoiceHasNoRequiredValues() {
		assertThrows(NullPointerException.class, () -> classUnderTest.save(new Invoice()));
	}

	@Test
	public void getInvoiceDTOListSucceed() {
		Invoice expectedInvoice = new Invoice();

		givenThatInvoiceHaveValues(expectedInvoice);
		given(invoiceDao.getAllInvoices()).willReturn(Collections.singletonList(expectedInvoice));
		List<InvoiceDTO> invoiceDTOList = classUnderTest.getInvoiceDTOList();
		InvoiceDTO invoiceDTO = invoiceDTOList.get(0);

		assertTrue(invoiceDTOList.size() > 0);
		assertEquals(expectedInvoice.getCustomer().getName(), invoiceDTO.getCustomer());
		assertEquals(expectedInvoice.getDueDate(), invoiceDTO.getDueDate());
		assertEquals(expectedInvoice.getInvoiceDate(), invoiceDTO.getInvoiceDate());
		assertEquals(expectedInvoice.getNumber(), invoiceDTO.getNumber());
		assertEquals(expectedInvoice.getId(), invoiceDTO.getId());
		assertEquals(expectedInvoice.getAmountDue(), invoiceDTO.getAmountDue());
	}

	private void givenThatInvoiceHaveValues(Invoice invoice) {
		invoice.setCustomer(new Customer());
		invoice.getCustomer().setName("Customer Name");
		invoice.setDueDate(DateTime.now());
		invoice.setInvoiceDate(DateTime.now());
		invoice.setNumber("Invoice Number");
		invoice.setBranchId(BRANCH_ID);
		invoice.setDeliveryStatus(Invoice.DeliveryStatus.FOR_DELIVERY);
		invoice.setAmountDue(BigDecimal.valueOf(1000));
		invoice.setTransactionType(Invoice.TransactionType.DELIVERY);
	}

	@Test
	public void getInvoiceDTOListSucceedWithEmptyList() {
		given(invoiceDao.getAllInvoices()).willReturn(Collections.emptyList());
		List<InvoiceDTO> invoiceDTOList = classUnderTest.getInvoiceDTOList();

		assertTrue(invoiceDTOList.isEmpty());
		verify(invoiceDao, times(1)).getAllInvoices();
	}

	@Test
	public void getInvoiceDTOListFailed() {
		given(invoiceDao.getAllInvoices()).willThrow(MongoTimeoutException.class);
		assertThrows(MongoException.class, () -> classUnderTest.getInvoiceDTOList());
	}

	@Test
	public void getInvoiceDTOListBySucceed() {
		PagedResultSet<Invoice> invoicePagedResultSet = givenThatInvoicesWereRetrieved();
		List<Invoice> retrievedInvoices = invoicePagedResultSet.getResults();

		given(invoiceDao.getInvoicesBy(any(QueryParameters.class))).willReturn(invoicePagedResultSet);
		PagedResultSet<InvoiceDTO> invoiceDTOPagedResultSet
				= classUnderTest.getInvoiceDTOListBy(new QueryParameters());
		List<InvoiceDTO> convertedInvoiceDTO = invoiceDTOPagedResultSet.getResults();

		verify(invoiceDao, times(1)).getInvoicesBy(any(QueryParameters.class));
		assertNotNull(invoiceDTOPagedResultSet);
		assertNotNull(convertedInvoiceDTO);
		assertFalse(convertedInvoiceDTO.isEmpty());
		assertEquals(retrievedInvoices.get(0).getInvoiceId(), convertedInvoiceDTO.get(0).getInvoiceId());
		assertEquals(retrievedInvoices.get(0).getCustomer().getName(), convertedInvoiceDTO.get(0).getCustomer());
		assertEquals(retrievedInvoices.get(0).getDeliveryStatus().getStatus(), convertedInvoiceDTO.get(0).getStatus());
	}

	private PagedResultSet<Invoice> givenThatInvoicesWereRetrieved() {
		Invoice invoice = new Invoice();
		invoice.setId("id");
		invoice.setInvoiceId(INVOICE_ID);
		invoice.setCustomer(new Customer());
		invoice.getCustomer().setName(CUSTOMER_NAME);
		invoice.getCustomer().setAddress(new Address());
		invoice.setDeliveryStatus(Invoice.DeliveryStatus.FOR_DELIVERY);
		invoice.setDeliveryDate(new DateTime());
		invoice.setTransactionType(Invoice.TransactionType.DELIVERY);
		invoice.setDeliveryDate(DateTime.now());
		invoice.setInvoiceDate(DateTime.now());

		PagedResultSet<Invoice> invoicePagedResultSet = new PagedResultSet<>();
		invoicePagedResultSet.setResults(Arrays.asList(
				invoice
		));
		return invoicePagedResultSet;
	}

	@Test
	public void getInvoiceDTOListBySucceedWithEmptyResults() {
		PagedResultSet<Invoice> invoicePagedResultSet = givenThatInvoiceResultIsEmpty();

		given(invoiceDao.getInvoicesBy(any(QueryParameters.class))).willReturn(invoicePagedResultSet);
		PagedResultSet<InvoiceDTO> invoiceDTOPagedResultSet = classUnderTest.getInvoiceDTOListBy(new QueryParameters());
		verify(invoiceDao, times(1)).getInvoicesBy(any(QueryParameters.class));
		assertNotNull(invoiceDTOPagedResultSet);
		assertTrue(invoiceDTOPagedResultSet.getResults().isEmpty());
	}

	private PagedResultSet<Invoice> givenThatInvoiceResultIsEmpty() {
		PagedResultSet pagedResultSet = new PagedResultSet();
		pagedResultSet.setPage(1);
		pagedResultSet.setSize(10);
		pagedResultSet.setTotalCount(0);
		pagedResultSet.setResults(new ArrayList());
		return pagedResultSet;
	}

	@Test
	public void getInvoiceSchedulesSucceed() {
		PagedResultSet<Invoice> invoicePagedResultSet = givenThatInvoicesWereRetrieved();

		given(invoiceDao.getInvoicesBy(any(QueryParameters.class))).willReturn(invoicePagedResultSet);
		List<InvoiceScheduleDTO> invoiceScheduleDTOList = classUnderTest.getInvoiceSchedules(new QueryParameters());

		verify(invoiceDao, times(1)).getInvoicesBy(any(QueryParameters.class));
		assertNotNull(invoiceScheduleDTOList);
		assertFalse(invoiceScheduleDTOList.isEmpty());
		for (InvoiceScheduleDTO invoiceScheduleDTO : invoiceScheduleDTOList) {
			assertNotNull(invoiceScheduleDTO.getId());
			assertNotNull(invoiceScheduleDTO.getTitle());
			assertNotNull(invoiceScheduleDTO.getStart());
			assertEquals(invoiceScheduleDTO.getEnd(), invoiceScheduleDTO.getStart().plusMinutes(30));
		}
	}

	@Test
	public void getInvoiceSchedulesSucceedWithEmptyList() {
		PagedResultSet<Invoice> invoicePagedResultSet = givenThatInvoiceResultIsEmpty();
		given(invoiceDao.getInvoicesBy(any(QueryParameters.class))).willReturn(invoicePagedResultSet);

		List<InvoiceScheduleDTO> invoiceScheduleDTOList = classUnderTest.getInvoiceSchedules(new QueryParameters());

		verify(invoiceDao, times(1)).getInvoicesBy(any(QueryParameters.class));
		assertNotNull(invoiceScheduleDTOList);
		assertTrue(invoiceScheduleDTOList.isEmpty());
	}

	@Test
	public void getInvoiceSucceed() {
		given(invoiceDao.getInvoice(anyString())).willReturn(new Invoice());
		Invoice invoice = classUnderTest.getInvoice(INVOICE_ID);

		assertNotNull(invoice);
		verify(invoiceDao, times(1)).getInvoice(anyString());
	}

	@Test
	public void getInvoiceSucceedWithNullRetrieved() {
		given(invoiceDao.getInvoice(anyString())).willReturn(null);

		assertNull(invoiceDao.getInvoice(INVOICE_ID));
		verify(invoiceDao, times(1)).getInvoice(anyString());
	}

	@Test
	public void givenSavedInvoiceReachedNumberPaddingLimit() {
		Invoice invoice = new Invoice();
		givenThatNewInvoiceBelongsToABranch(invoice);
		InvoiceNumberTracker invoiceNumberTracker = new InvoiceNumberTracker();
		invoiceNumberTracker.setCurrentNumber(9999);
		given(branchDao.getBranchByBranchId(anyString())).willReturn(getTestBranch());
		given(invoiceNumberTrackerDao.findAndModifyCurrentNumber(anyString())).willReturn(invoiceNumberTracker);
		given(invoiceDao.saveAndReturn(any(Invoice.class))).willReturn(invoice);

		classUnderTest.save(invoice);

		ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
		verify(invoiceDao, times(1)).saveAndReturn(invoiceArgumentCaptor.capture());
		Invoice savedInvoice = invoiceArgumentCaptor.getValue();
		assertEquals(savedInvoice.getNumber(), "TB09999");
	}


	@Test
	public void givenSavedInvoiceExceededNumberPaddingLimit() {
		Invoice invoice = new Invoice();
		givenThatNewInvoiceBelongsToABranch(invoice);
		InvoiceNumberTracker invoiceNumberTracker = new InvoiceNumberTracker();
		invoiceNumberTracker.setCurrentNumber(10000);
		given(branchDao.getBranchByBranchId(anyString())).willReturn(getTestBranch());
		given(invoiceNumberTrackerDao.findAndModifyCurrentNumber(anyString())).willReturn(invoiceNumberTracker);
		given(invoiceDao.saveAndReturn(any(Invoice.class))).willReturn(invoice);

		classUnderTest.save(invoice);

		ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
		verify(invoiceDao, times(1)).saveAndReturn(invoiceArgumentCaptor.capture());
		Invoice savedInvoice = invoiceArgumentCaptor.getValue();
		assertEquals(savedInvoice.getNumber(), "TB10000");
	}
	@Test
	public void removeSucceed() {
		given(invoiceDao.getInvoice(anyString())).willReturn(new Invoice());
		classUnderTest.remove(INVOICE_ID);
		verify(invoiceDao, times(1)).remove(any(Invoice.class));
	}

	@Test
	public void removeFailedNotFound() {
		given(invoiceDao.getInvoice(anyString())).willReturn(null);
		assertThrows(NotFoundException.class, () -> classUnderTest.remove(INVOICE_ID));
	}

	@Test
	public void voidSucceed() {
		Invoice invoiceToVoid = new Invoice();
		invoiceToVoid.setId(INVOICE_ID);
		invoiceToVoid.setBranchId(BRANCH_ID);
		invoiceToVoid.setNumber(INVOICE_NUMBER);
		given(invoiceDao.getInvoice(anyString())).willReturn(invoiceToVoid);
		given(invoiceDao.findAndReplace(any(Invoice.class))).willReturn(invoiceToVoid);
		classUnderTest.voidInvoice(INVOICE_ID);
		verify(invoiceDao, times(1)).findAndReplace(any(Invoice.class));
		verify(backendKafkaAuditSender, times(1))
				.send(any(Resource.class), anyString(), anyString(), anyString(), anyString(), any(Invoice.class), any(Invoice.class));
	}

	@Test
	public void voidFailedNotFound() {
		given(invoiceDao.getInvoice(anyString())).willReturn(null);
		assertThrows(NotFoundException.class, () -> classUnderTest.voidInvoice(INVOICE_ID));
	}

	@Test
	public void updatePaymentDetailsSucceedFullPayment() {
		PaymentDetails paymentDetails = new PaymentDetails();
		paymentDetails.setAmount(new BigDecimal(99));
		paymentDetails.setPaymentMethod(PaymentDetails.PaymentMethod.CASH);
		paymentDetails.setPaymentDate(DateTime.now());
		Invoice paidInvoice = new Invoice();
		paidInvoice.setAmountDue(new BigDecimal(99));
		given(invoiceDao.getInvoice(anyString())).willReturn(paidInvoice);

		classUnderTest.updatePaymentDetails(INVOICE_ID, paymentDetails);

		ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
		verify(invoiceDao).findAndReplace(invoiceArgumentCaptor.capture());
		Invoice savedInvoice = invoiceArgumentCaptor.getValue();
		assertEquals(paymentDetails.getAmount(), savedInvoice.getPaymentDetails().getAmount());
		assertNotNull(savedInvoice.getPaymentDetails().getUpdateDate());
		assertTrue(savedInvoice.getPaymentDetails().getPaymentStatus().equals(PaymentDetails.PaymentStatus.PAID));
	}

	@Test
	public void updatePaymentDetailsSucceedPartialPayment() {
		PaymentDetails paymentDetails = new PaymentDetails();
		paymentDetails.setAmount(new BigDecimal(9));
		paymentDetails.setPaymentMethod(PaymentDetails.PaymentMethod.CASH);
		paymentDetails.setPaymentDate(DateTime.now());
		Invoice paidInvoice = new Invoice();
		paidInvoice.setAmountDue(new BigDecimal(99));
		given(invoiceDao.getInvoice(anyString())).willReturn(paidInvoice);

		classUnderTest.updatePaymentDetails(INVOICE_ID, paymentDetails);

		ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
		verify(invoiceDao).findAndReplace(invoiceArgumentCaptor.capture());
		Invoice savedInvoice = invoiceArgumentCaptor.getValue();
		assertEquals(paymentDetails.getAmount(), savedInvoice.getPaymentDetails().getAmount());
		assertNotNull(savedInvoice.getPaymentDetails().getUpdateDate());
		assertTrue(savedInvoice.getPaymentDetails().getPaymentStatus().equals(PaymentDetails.PaymentStatus.PARTIAL));
	}

	@Test
	public void updatePaymentDetailsFailedNegativeAmount() {
		PaymentDetails paymentDetails = new PaymentDetails();
		paymentDetails.setAmount(new BigDecimal(-1));
		assertThrows(BadRequestException.class, () -> classUnderTest.updatePaymentDetails(INVOICE_ID, paymentDetails));
	}

	@Test
	public void updatePaymentDetailsFailedNoPaymentMethod() {
		PaymentDetails paymentDetails = new PaymentDetails();
		paymentDetails.setAmount(new BigDecimal(99));

		assertThrows(BadRequestException.class, () -> classUnderTest.updatePaymentDetails(INVOICE_ID, paymentDetails));
	}

	@Test
	public void updatePaymentDetailsFailedNoPaymentDate() {
		PaymentDetails paymentDetails = new PaymentDetails();
		paymentDetails.setAmount(new BigDecimal(99));
		paymentDetails.setPaymentMethod(PaymentDetails.PaymentMethod.CASH);

		assertThrows(BadRequestException.class, () -> classUnderTest.updatePaymentDetails(INVOICE_ID, paymentDetails));
	}

	@Test
	public void updateRiderDetailsSucceed() {
		given(invoiceDao.getInvoice(anyString())).willReturn(new Invoice());
		classUnderTest.updateRiderDetails(INVOICE_ID, new RiderDetails());
		verify(invoiceDao, times(1)).findAndReplace(any(Invoice.class));
	}

	@Test
	public void updateRiderDetailsWithNullValue() {
		assertThrows(BadRequestException.class, () -> classUnderTest.updateRiderDetails(INVOICE_ID, null));
	}

	@Test
	public void checkInvoiceWithPastDeliveryDateUpdatedByNonRootAdmin() {
		Invoice invoice = new Invoice();
		invoice.setDeliveryDate(DateTime.now().withTimeAtStartOfDay().minusMillis(1));
		given(invoiceDao.getInvoice(anyString())).willReturn(invoice);

		assertThrows(NotAcceptableException.class, () -> classUnderTest.checkInvoiceIsPastDeliveryDateBeforeUpdating(INVOICE_ID));
	}

	@Test
	public void checkInvoiceWithPastDeliveryDateUpdatedByRootAdmin() {
		Invoice invoice = new Invoice();
		invoice.setDeliveryDate(DateTime.now().withTimeAtStartOfDay().minusMillis(1));
		given(subject.isPermitted("*:*:*")).willReturn(true);
		given(invoiceDao.getInvoice(anyString())).willReturn(invoice);

		classUnderTest.checkInvoiceIsPastDeliveryDateBeforeUpdating(INVOICE_ID);
	}

	@Test
	public void checkInvoiceThatIsStillWithinDeliveryDate() {
		Invoice invoice = new Invoice();
		invoice.setDeliveryDate(DateTime.now());
		given(invoiceDao.getInvoice(anyString())).willReturn(invoice);

		classUnderTest.checkInvoiceIsPastDeliveryDateBeforeUpdating(INVOICE_ID);
	}

	@Test
	public void getInvoiceDeliveriesForDashboardSucceed() {
		given(invoiceDao.count(any(QueryParameters.class))).willReturn(1L);
		InvoiceDeliveriesForDashboardDTO invoiceDeliveriesForDashboardDTO = classUnderTest.getInvoiceDeliveriesForDashboard(BRANCH_ID);
		assertEquals(invoiceDeliveriesForDashboardDTO.getDeliveriesToday(), 1L);
		assertEquals(invoiceDeliveriesForDashboardDTO.getSameDayDeliveries(), 1L);
		assertEquals(invoiceDeliveriesForDashboardDTO.getDeliveredToday(), 1L);
		assertEquals(invoiceDeliveriesForDashboardDTO.getRemainingDeliveries(), 1L);
		verify(invoiceDao, times(4)).count(any(QueryParameters.class));
	}
}
