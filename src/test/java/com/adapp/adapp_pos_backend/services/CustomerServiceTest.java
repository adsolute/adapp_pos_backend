package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.dao.CustomerDao;
import com.adapp.adapp_pos_backend.utils.BackendKafkaAuditSender;
import com.adapp.adapp_pos_models.Address;
import com.adapp.adapp_pos_models.ContactInformation;
import com.adapp.adapp_pos_models.customers.Customer;
import com.adapp.adapp_pos_models.customers.CustomerDTO;
import com.adapp.adapp_pos_models.enums.Resource;
import com.mongodb.MongoException;
import com.mongodb.MongoTimeoutException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {

	private static final String CUSTOMER_NAME = "Customer";
	private static final String MONGO_CONNECTION_ERROR = "Connection Error";
	public static final String CUSTOMER_ID = "customer id";
	public static final String OBJECT_ID = "OBJECT ID";
	@InjectMocks
	CustomerService classUnderTest;

	@Mock
	CustomerDao customerDao;

	@Mock
	BackendKafkaAuditSender<Customer> backendKafkaAuditSender;

	@Test
	public void getAllCustomersSucceed() {
		Customer customer = new Customer();

		givenThatCustomerWillHaveValues(customer);
		given(customerDao.getAllCustomers()).willReturn(Collections.singletonList(customer));
		List<CustomerDTO> customerDTOS = classUnderTest.getAllCustomers();
		CustomerDTO expectedCustomerDTO = customerDTOS.get(0);
		assertEquals(1, customerDTOS.size());
		assertEquals(customer.getName(), expectedCustomerDTO.getName());
		assertEquals(customer.getContactInformation().getEmailAddress(), expectedCustomerDTO.getEmailAddress());
		assertEquals(customer.getContactInformation().getPhoneNumber(), expectedCustomerDTO.getPhoneNumber());
		assertEquals(customer.getId(), expectedCustomerDTO.getId());
	}

	private void givenThatCustomerWillHaveValues(Customer customer) {
		customer.setName(CUSTOMER_NAME);
		customer.setId("customerId");
		customer.setContactInformation(new ContactInformation());
		customer.getContactInformation().setEmailAddress("emailAddress");
		customer.getContactInformation().setPhoneNumber("1234");
		customer.setAddress(new Address());
		customer.getAddress().setAddressLine1("line 1");
		customer.getAddress().setAddressLine2("line 3");
		customer.getAddress().setCity("city");
		customer.getAddress().setProvince("province");
		customer.getAddress().setZipCode("4106");
		customer.getAddress().setCountry("PH");
	}

	@Test
	public void getAllCustomersSucceedWithEmptyList() {
		given(customerDao.getAllCustomers()).willReturn(Collections.emptyList());
		List<CustomerDTO> customers = classUnderTest.getAllCustomers();
		assertTrue(customers.isEmpty());
	}

	@Test
	public void saveSucceeded() {
		Customer savedCustomer = new Customer();
		savedCustomer.setId(OBJECT_ID);
		savedCustomer.setName(CUSTOMER_NAME);
		given(customerDao.saveAndReturn(any(Customer.class))).willReturn(savedCustomer);
		classUnderTest.save(new Customer());
		verify(customerDao, times(1)).saveAndReturn(any(Customer.class));
		verify(backendKafkaAuditSender, times(1)).send(any(Resource.class), anyString(), eq(null), anyString(), anyString());
	}

	@Test
	public void saveSucceededForCustomerUpdate() {
		Customer customerForUpdate = new Customer();
		customerForUpdate.setId(OBJECT_ID);
		customerForUpdate.setName(CUSTOMER_NAME);

		Customer customerBeforeUpdate = new Customer();
		customerBeforeUpdate.setId(OBJECT_ID);
		given(customerDao.findAndReplace(any(Customer.class))).willReturn(customerBeforeUpdate);
		classUnderTest.save(customerForUpdate);
		verify(customerDao, times(1)).findAndReplace(any(Customer.class));
		verify(backendKafkaAuditSender, times(1))
				.send(any(Resource.class), anyString(), eq(null), anyString(), anyString(), any(Customer.class), any(Customer.class));
	}

	@Test
	public void getCustomersByNameSucceed() {
		List<Customer> retrievedCustomers = new ArrayList<>();

		givenThatCustomersHaveSameName(retrievedCustomers);
		given(customerDao.getCustomers(anyString())).willReturn(retrievedCustomers);
		List<Customer> customers = classUnderTest.getCustomers(CUSTOMER_NAME);
		assertTrue(customers.size() > 1);
		assertTrue(customers.get(0).getName().contains(CUSTOMER_NAME));
		assertTrue(customers.get(1).getName().contains(CUSTOMER_NAME));
	}

	private void givenThatCustomersHaveSameName(List<Customer> retrievedCustomers) {
		Customer customer = new Customer();
		customer.setName("Customer 1");
		retrievedCustomers.add(customer);
		customer = new Customer();
		customer.setName("Customer A");
		retrievedCustomers.add(customer);
	}

	@Test
	public void getCustomersByNameSucceedWithEmptyList() {
		given(customerDao.getCustomers(anyString())).willReturn(Collections.emptyList());
		List<Customer> customers = classUnderTest.getCustomers(CUSTOMER_NAME);
		assertTrue(customers.isEmpty());
	}

	@Test
	public void getCustomerDTOResultSetByQuerySucceed() {
		PagedResultSet<Customer> retrievedPagedResultSet = new PagedResultSet<>();

		givenThatCustomersWereRetrieved(retrievedPagedResultSet);
		given(customerDao.getCustomersBy(any(QueryParameters.class))).willReturn(retrievedPagedResultSet);

		PagedResultSet<CustomerDTO> customerDTOPagedResultSet = classUnderTest.getCustomerDTOResultSetBy(new QueryParameters());
		Customer customer = retrievedPagedResultSet.getResults().get(0);
		CustomerDTO customerDTO = customerDTOPagedResultSet.getResults().get(0);

		assertNotNull(customerDTOPagedResultSet);
		assertFalse(customerDTOPagedResultSet.getResults().isEmpty());
		assertEquals(retrievedPagedResultSet.getSize(), customerDTOPagedResultSet.getSize());
		assertEquals(retrievedPagedResultSet.getPage(), customerDTOPagedResultSet.getPage());
		assertEquals(retrievedPagedResultSet.getTotalCount(), customerDTOPagedResultSet.getTotalCount());
		assertEquals(retrievedPagedResultSet.getResults().size(), customerDTOPagedResultSet.getResults().size());
		assertEquals(customer.getId(), customerDTO.getId());
		assertEquals(customer.getName(), customerDTO.getName());
		assertEquals(customer.getContactInformation().getEmailAddress(), customerDTO.getEmailAddress());
		assertEquals(customer.getContactInformation().getPhoneNumber(), customerDTO.getPhoneNumber());
		verify(customerDao, times(1)).getCustomersBy(any(QueryParameters.class));
	}

	private void givenThatCustomersWereRetrieved(PagedResultSet<Customer> retrievedPagedResultSet) {
		Customer customer = new Customer();
		givenThatCustomerWillHaveValues(customer);
		retrievedPagedResultSet.setResults(Arrays.asList(customer));
	}

	@Test
	public void getCustomerDTOResultSetByQuerySucceedWithEmptyList() {
		PagedResultSet emptyResultSet = new PagedResultSet();
		emptyResultSet.setResults(new ArrayList());
		given(customerDao.getCustomersBy(any(QueryParameters.class))).willReturn(emptyResultSet);

		PagedResultSet<CustomerDTO> customerDTOPagedResultSet = classUnderTest.getCustomerDTOResultSetBy(new QueryParameters());
		assertNotNull(customerDTOPagedResultSet);
		assertTrue(customerDTOPagedResultSet.getResults().isEmpty());
		assertEquals(emptyResultSet.getPage(), customerDTOPagedResultSet.getPage());
		assertEquals(emptyResultSet.getTotalCount(), customerDTOPagedResultSet.getTotalCount());
		assertEquals(emptyResultSet.getSize(), customerDTOPagedResultSet.getSize());
	}

	@Test
	public void getCustomerSucceed() {
		Customer existingCustomer = new Customer();
		existingCustomer.setId(CUSTOMER_ID);
		given(customerDao.getCustomer(anyString())).willReturn(existingCustomer);

		Customer retrievedCustomer = classUnderTest.getCustomer(CUSTOMER_ID);
		assertNotNull(retrievedCustomer);
		assertEquals(CUSTOMER_ID, retrievedCustomer.getId());
		verify(customerDao, times(1)).getCustomer(anyString());
	}

	@Test
	public void getCustomerSucceedWithNullResult() {
		Customer retrievedCustomer = classUnderTest.getCustomer(CUSTOMER_ID);

		assertNull(retrievedCustomer);
		verify(customerDao, times(1)).getCustomer(anyString());
	}

	@Test
	public void getCustomerMongoConnectionFailed() {
		given(customerDao.getCustomer(CUSTOMER_ID)).willThrow(MongoTimeoutException.class);
		assertThrows(MongoException.class, () -> classUnderTest.getCustomer(CUSTOMER_ID));
	}
	@Test
	public void removeSucceed() {
		Customer removedCustomer = new Customer();
		removedCustomer.setName(CUSTOMER_NAME);
		given(customerDao.getCustomer(anyString())).willReturn(removedCustomer);
		classUnderTest.remove(CUSTOMER_ID);
		verify(customerDao, times(1)).remove(any(Customer.class));
		verify(backendKafkaAuditSender, times(1)).send(any(Resource.class), eq(null), anyString(), anyString());
	}

	@Test
	public void removeFailedNotFound() {
		given(customerDao.getCustomer(anyString())).willReturn(null);
		assertThrows(NotFoundException.class, () -> classUnderTest.remove(CUSTOMER_ID));
	}
}