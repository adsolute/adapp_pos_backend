package com.adapp.adapp_pos_backend.services;

import com.adapp.adapp_pos_backend.dao.ExpenseDao;
import com.adapp.adapp_pos_models.expenses.Expense;
import com.mongodb.MongoException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ExpenseServiceTest {


    public static final String EXPENSE_ID = "expenseId";

    @InjectMocks
    ExpenseService classUnderTest;

    @Mock
    ExpenseDao expenseDao;

    @Test
    public void getAllExpensesSucceed() {
        given(expenseDao.getAllExpenses()).willReturn(Arrays.asList(new Expense()));
        List<Expense> expenses = classUnderTest.getAllExpenses();
        verify(expenseDao, times(1)).getAllExpenses();
        assertTrue(expenses.size() > 0);
    }

    @Test
    public void getAllExpensesSucceedWithEmptyList() {
        List<Expense> expenses = classUnderTest.getAllExpenses();
        verify(expenseDao, times(1)).getAllExpenses();
        assertTrue(expenses.isEmpty());
    }

    @Test
    public void getAllExpensesFailed() {
        given(expenseDao.getAllExpenses()).willThrow(new MongoException("Connection Error"));
        assertThrows(MongoException.class, () -> classUnderTest.getAllExpenses());
    }

    @Test
    public void getExpenseByIdSucceed() {
        given(expenseDao.getExpenses(anyString())).willReturn(new Expense());
        Expense expense = classUnderTest.getExpense(EXPENSE_ID);

        assertNotNull(expense);
        verify(expenseDao, times(1)).getExpenses(anyString());
    }

    @Test
    public void saveExpenseSucceed() {
        Expense expense = new Expense();
        expense.setAmount(new BigDecimal(500));
        classUnderTest.save(expense);
        verify(expenseDao, times(1)).save(any(Expense.class));
    }

    @Test
    public void saveExpenseFailed() {
        assertThrows(BadRequestException.class, () -> classUnderTest.save(new Expense()));
    }

    @Test
    public void updateExpenseSucceed() {
        Expense expense = givenThatExpenseIsForUpdate();
        classUnderTest.update(expense);
        verify(expenseDao,times(1)).save(any(Expense.class));
    }

    @Test
    public void updateExpensesFailed() {
        assertThrows(NullPointerException.class, () -> classUnderTest.update(new Expense()));
    }

    private Expense givenThatExpenseIsForUpdate() {
        Expense expense = new Expense();
        expense.setId(EXPENSE_ID);
        return expense;
    }

    @Test
    public void removeSucceed() {
        given(expenseDao.getExpenses(anyString())).willReturn(new Expense());
        classUnderTest.remove(EXPENSE_ID);
        verify(expenseDao, times(1)).remove(any(Expense.class));
    }

    @Test
    public void removeFailedNotFound() {
        given(expenseDao.getExpenses(anyString())).willReturn(null);
        assertThrows(NotFoundException.class, () -> classUnderTest.remove(EXPENSE_ID));
    }
}
