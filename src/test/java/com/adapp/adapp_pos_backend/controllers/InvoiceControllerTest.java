package com.adapp.adapp_pos_backend.controllers;

import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.services.InvoiceService;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.invoices.Invoice;
import com.adapp.adapp_pos_models.invoices.InvoiceDTO;
import com.adapp.adapp_pos_models.invoices.InvoiceScheduleDTO;
import com.adapp.adapp_pos_models.payments.PaymentDetails;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.adapp.adapp_pos_models.invoices.Invoice.DeliveryStatus.*;
import static com.adapp.security.models.Permission.Action.UPDATE;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class InvoiceControllerTest {

	public static final String BRANCH_ID_KEY = "branchId";
	public static final String BRANCH_ID_VALUE = "branch id";
	public static final String BRANCH_ID = "branch id";
	public static final String INVOICE_ID = "invoice id";
	public static final String DELIVERY_STATUS = "deliveryStatus";
	@InjectMocks
	InvoiceController classUnderTest;

	@Mock
	InvoiceService invoiceService;

	@Mock
	Subject subject;

	@BeforeEach
	public void setup() {
		SubjectThreadState threadState = new SubjectThreadState(subject);
		threadState.bind();
	}

	@Test
	public void getAllInvoicesThatSucceed(){
		when(invoiceService.getInvoiceDTOList()).thenReturn(Collections.singletonList(new InvoiceDTO()));
		given(subject.isPermitted(anyString())).willReturn(true);
		Response apiResponse = classUnderTest.getAllInvoices();
		List<InvoiceDTO> invoiceDTOList = (List<InvoiceDTO>) apiResponse.getEntity();
		assertFalse(invoiceDTOList.isEmpty());
		assertEquals(200, apiResponse.getStatus());
	}

	@Test
	public void getAllInvoicesThatSucceedWithEmptyInvoice(){
		given(subject.isPermitted(anyString())).willReturn(true);
		Response apiResponse = classUnderTest.getAllInvoices();
		List<Invoice> invoices = (List<Invoice>) apiResponse.getEntity();
		assertTrue(invoices.isEmpty());
		assertEquals(200, apiResponse.getStatus());
	}

	@Test
	public void getAllInvoicesFailedNotPermitted() {
		Response response = classUnderTest.getAllInvoices();
		assertEquals(401, response.getStatus());
	}

	@Test
	public void getAllInvoicesThatFailed(){
		given(subject.isPermitted(anyString())).willReturn(true);
		given(invoiceService.getInvoiceDTOList()).willThrow(new NullPointerException());
		assertThrows(ServerException.class, ()-> classUnderTest.getAllInvoices());
	}

	@Test
	public void saveThatSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.save(givenThatInvoiceCreatedWithBranchId());
		verify(invoiceService, times(1)).save(any(Invoice.class));
		assertEquals(200, response.getStatus());
	}

	private Invoice givenThatInvoiceCreatedWithBranchId() {
		Invoice invoice = new Invoice();
		invoice.setBranchId(BRANCH_ID);
		return invoice;
	}

	@Test
	public void saveFailedBadRequest() {
		Invoice invoice = givenThatInvoiceCreatedWithBranchId();
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(BadRequestException.class).given(invoiceService).save(any(Invoice.class));
		Response response = classUnderTest.save(invoice);
		assertEquals(400, response.getStatus());
	}

	@Test
	public void saveFailedNoBranchId() {
		Response response = classUnderTest.save(new Invoice());
		assertEquals(400, response.getStatus());
	}

	@Test
	public void saveFailedNotPermitted() {
		Response response = classUnderTest.save(givenThatInvoiceCreatedWithBranchId());
		assertEquals(401, response.getStatus());
	}

	@Test
	public void saveFailed() {
		willThrow(new RuntimeException()).given(invoiceService).save(any(Invoice.class));
		given(subject.isPermitted(anyString())).willReturn(true);
		assertThrows(ServerException.class, () -> classUnderTest.save(givenThatInvoiceCreatedWithBranchId()));
	}

	@Test
	public void updateInvoice() {
		ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
		Invoice invoice = givenThatInvoiceIsForUpdate();

		given(subject.isPermitted(anyString())).willReturn(true);

		Response response = classUnderTest.update(invoice);

		verify(invoiceService, times(1)).update(invoiceArgumentCaptor.capture());
		assertNotNull(invoiceArgumentCaptor.getValue().getId());
		assertNotNull(invoiceArgumentCaptor.getValue().getInvoiceId());
		assertEquals(200, response.getStatus());
	}

	@Test
	public void updateInvoiceFailedNoId() {
		Response response = classUnderTest.update(givenThatInvoiceCreatedWithBranchId());

		assertEquals(400, response.getStatus());
	}

	private Invoice givenThatInvoiceIsForUpdate() {
		Invoice invoice = new Invoice();
		invoice.setId("1");
		invoice.setInvoiceId("1");
		invoice.setBranchId("1");

		return invoice;
	}

	@Test
	public void updateInvoiceFailedBadRequest() {
		ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
		Invoice invoice = givenThatInvoiceIsForUpdate();
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(BadRequestException.class).given(invoiceService).update(any(Invoice.class));

		Response response = classUnderTest.update(invoice);

		verify(invoiceService, times(1)).update(invoiceArgumentCaptor.capture());
		assertEquals(400, response.getStatus());
	}

	@Test
	public void updateInvoiceByNonRootAdminAfterEndOfDeliveryDay() {
		ArgumentCaptor<Invoice> invoiceArgumentCaptor = ArgumentCaptor.forClass(Invoice.class);
		Invoice invoice = givenThatInvoiceIsForUpdate();
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(NotAcceptableException.class)
				.given(invoiceService).checkInvoiceIsPastDeliveryDateBeforeUpdating(anyString());

		Response response = classUnderTest.update(invoice);

		assertEquals(406, response.getStatus());
		verify(invoiceService, never()).update(invoiceArgumentCaptor.capture());
	}

	@Test
	public void saveFailedForUpdateNotPermitted() {
		Invoice invoice = givenThatInvoiceIsForUpdate();

		Response response = classUnderTest.update(invoice);

		assertEquals(401, response.getStatus());
		assertTrue(response.getEntity().toString().contains(UPDATE.toString()));
	}

	@Test
	public void getInvoiceDTOListBySucceed() {
		PagedResultSet<InvoiceDTO> invoiceDTOPagedResultSet = givenThatInvoicesDTOListWasRetrieved();
		given(invoiceService.getInvoiceDTOListBy(any(QueryParameters.class))).willReturn(invoiceDTOPagedResultSet);
		given(subject.isPermitted(anyString())).willReturn(true);

		Response response = classUnderTest.getInvoiceDTOListBy(BRANCH_ID_VALUE, new QueryParameters());
		PagedResultSet<InvoiceDTO> retrievedPagedResultSet = (PagedResultSet<InvoiceDTO>) response.getEntity();

		verify(invoiceService, times(1)).getInvoiceDTOListBy(any(QueryParameters.class));
		assertEquals(200, response.getStatus());
		assertNotNull(retrievedPagedResultSet);
		assertFalse(retrievedPagedResultSet.getResults().isEmpty());
		assertEquals(invoiceDTOPagedResultSet.getSize(), retrievedPagedResultSet.getSize());
		assertEquals(invoiceDTOPagedResultSet.getResults().size(), retrievedPagedResultSet.getResults().size());
	}

	private PagedResultSet<InvoiceDTO> givenThatInvoicesDTOListWasRetrieved() {
		PagedResultSet<InvoiceDTO> invoiceDTOPagedResultSet = new PagedResultSet<>();
		invoiceDTOPagedResultSet.setResults(Arrays.asList(new InvoiceDTO()));
		return invoiceDTOPagedResultSet;
	}

	@Test
	public void getInvoiceDTOListBySucceedWithEmptyResults() {
		PagedResultSet<InvoiceDTO> invoiceDTOPagedResultSet = givenThatInvoiceResultsIsEmpty();
		given(invoiceService.getInvoiceDTOListBy(any(QueryParameters.class))).willReturn(invoiceDTOPagedResultSet);
		given(subject.isPermitted(anyString())).willReturn(true);

		Response response = classUnderTest.getInvoiceDTOListBy(BRANCH_ID_VALUE, new QueryParameters());
		PagedResultSet<InvoiceDTO> retrievedPagedResults = (PagedResultSet<InvoiceDTO>) response.getEntity();

		verify(invoiceService, times(1)).getInvoiceDTOListBy(any(QueryParameters.class));
		assertEquals(200, response.getStatus());
		assertNotNull(retrievedPagedResults);
		assertTrue(retrievedPagedResults.getResults().isEmpty());
	}

	private PagedResultSet<InvoiceDTO> givenThatInvoiceResultsIsEmpty() {
		PagedResultSet pagedResultSet = new PagedResultSet();
		pagedResultSet.setResults(Collections.EMPTY_LIST);
		return pagedResultSet;
	}

	@Test
	public void getInvoiceDTOListByFailedNotPermitted() {
		Response response = classUnderTest.getInvoiceDTOListBy(BRANCH_ID_VALUE, new QueryParameters());

		verify(subject, times(1)).isPermitted(anyString());
		assertEquals(401, response.getStatus());
	}

	@Test
	public void getInvoiceDTOListByFailed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(invoiceService.getInvoiceDTOListBy(any(QueryParameters.class))).willThrow(NullPointerException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getInvoiceDTOListBy(BRANCH_ID_VALUE, new QueryParameters()));
	}

	@Test
	public void getInvoiceSchedulesSucceed() {
		List<InvoiceScheduleDTO> invoiceScheduleDTOListForDelivery = givenThatInvoiceScheduledForDeliveryWasRetrieved();
		given(subject.isPermitted(anyString())).willReturn(true);
		given(invoiceService.getInvoiceSchedules(any(QueryParameters.class))).willReturn(invoiceScheduleDTOListForDelivery);

		Response response = classUnderTest.getInvoiceSchedules(BRANCH_ID_VALUE, new QueryParameters());
		List<InvoiceScheduleDTO> invoiceScheduleDTOList = (List<InvoiceScheduleDTO>) response.getEntity();

		verify(invoiceService, times(1)).getInvoiceSchedules(any(QueryParameters.class));
		assertEquals(200, response.getStatus());
		assertFalse(invoiceScheduleDTOList.isEmpty());
	}

	private List<InvoiceScheduleDTO> givenThatInvoiceScheduledForDeliveryWasRetrieved() {
		return Arrays.asList(new InvoiceScheduleDTO());
	}

	@Test
	public void getInvoiceSchedulesSucceedWithEmptyList() {
		given(subject.isPermitted(anyString())).willReturn(true);

		Response response = classUnderTest.getInvoiceSchedules(BRANCH_ID_VALUE, new QueryParameters());
		List<InvoiceScheduleDTO> invoiceScheduleDTOList = (List<InvoiceScheduleDTO>) response.getEntity();

		verify(invoiceService, times(1)).getInvoiceSchedules(any(QueryParameters.class));
		assertEquals(200, response.getStatus());
		assertTrue(invoiceScheduleDTOList.isEmpty());
	}

	@Test
	public void getInvoiceSchedulesFailedNotPermitted() {
		Response response = classUnderTest.getInvoiceSchedules(BRANCH_ID_VALUE, new QueryParameters());
		assertEquals(401, response.getStatus());
	}

	@Test
	public void getInvoiceSchedulesFailed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(invoiceService.getInvoiceSchedules(any(QueryParameters.class))).willThrow(NullPointerException.class);

		assertThrows(ServerException.class, () -> classUnderTest.getInvoiceSchedules(BRANCH_ID_VALUE, new QueryParameters()));
	}

	@Test
	public void getInvoiceSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(invoiceService.getInvoice(anyString())).willReturn(new Invoice());
		Response response = classUnderTest.getInvoice(INVOICE_ID, BRANCH_ID);

		assertEquals(200, response.getStatus());
		assertNotNull(response.getEntity());
	}

	@Test
	public void getInvoiceFailedNotFound() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.getInvoice(INVOICE_ID, BRANCH_ID);

		assertEquals(404, response.getStatus());
		assertNotNull(response.getEntity());
		assertTrue(response.getEntity().toString().contains(INVOICE_ID));
	}

	@Test
	public void getInvoiceFailedNotPermitted() {
		Response response = classUnderTest.getInvoice(INVOICE_ID, BRANCH_ID);

		assertEquals(401, response.getStatus());
		assertNotNull(response.getEntity());
		assertTrue(response.getEntity().toString().contains(BRANCH_ID));
	}

	@Test
	public void getInvoiceFailedSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(invoiceService.getInvoice(anyString())).willThrow(RuntimeException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getInvoice(INVOICE_ID, BRANCH_ID));
	}

	@Test
	public void updateDeliveryStatusSucceed() {
		Invoice invoice = givenThatInvoiceCreatedWithBranchId();
		given(invoiceService.getInvoice(anyString())).willReturn(invoice);
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.updateDeliveryStatus(INVOICE_ID, DELIVERED);

		assertEquals(200, response.getStatus());
	}

	@Test
	public void updateDeliveryStatusFailedNotFound() {
		given(invoiceService.getInvoice(anyString())).willThrow(NotFoundException.class);
		Response response = classUnderTest.updateDeliveryStatus(INVOICE_ID, DELIVERED);

		assertEquals(404, response.getStatus());
	}

	@Test
	public void updateDeliveryStatusFailedInvoiceHasNoBranch() {
		given(invoiceService.getInvoice(anyString())).willReturn(new Invoice());
		Response response = classUnderTest.updateDeliveryStatus(INVOICE_ID, DELIVERED);

		assertEquals(400, response.getStatus());
	}

	@Test
	public void updateDeliveryStatusFailedNotPermitted() {
		Invoice invoice = givenThatInvoiceCreatedWithBranchId();
		given(invoiceService.getInvoice(anyString())).willReturn(invoice);
		given(subject.isPermitted(anyString())).willReturn(false);
		Response response = classUnderTest.updateDeliveryStatus(INVOICE_ID, DELIVERED);

		assertEquals(401, response.getStatus());
	}

	@Test
	public void updateDeliveryStatusSomethingWentWrong() {
		Invoice invoice = givenThatInvoiceCreatedWithBranchId();
		given(invoiceService.getInvoice(anyString())).willReturn(invoice);
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(invoiceService).update(any(Invoice.class));

		assertThrows(ServerException.class, () -> classUnderTest.updateDeliveryStatus(INVOICE_ID, DELIVERED));
	}

	@Test
	public void getInvoiceQueueSucceed() {
		QueryParameters queryParameters = new QueryParameters();
		queryParameters.getCriteria().put(DELIVERY_STATUS, PROCESSING.toString());
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.getInvoiceQueue(BRANCH_ID, queryParameters);
		assertEquals(200, response.getStatus());
	}

	@Test
	public void getInvoiceQueueNotPermitted() {
		QueryParameters queryParameters = new QueryParameters();
		queryParameters.getCriteria().put(DELIVERY_STATUS, PROCESSING.toString());
		given(subject.isPermitted(anyString())).willReturn(false);
		Response response = classUnderTest.getInvoiceQueue(BRANCH_ID, queryParameters);
		assertEquals(401, response.getStatus());
	}

	@Test
	public void getInvoiceQueueFailedHasNoDeliveryStatusCriteria() {
		Response response = classUnderTest.getInvoiceQueue(BRANCH_ID, new QueryParameters());
		assertEquals(400, response.getStatus());
	}

	@Test
	public void getInvoiceQueueSomethingFailed() {
		QueryParameters queryParameters = new QueryParameters();
		queryParameters.getCriteria().put(DELIVERY_STATUS, PROCESSING.toString());
		given(subject.isPermitted(anyString())).willReturn(true);
		given(invoiceService.getInvoiceQueue(any(QueryParameters.class))).willThrow(RuntimeException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getInvoiceQueue(BRANCH_ID, queryParameters));
	}

	@Test
	public void updateInvoiceQueueSucceed() {
		Invoice invoice = givenThatInvoiceCreatedWithBranchId();
		given(invoiceService.getInvoice(anyString())).willReturn(invoice);
		given(subject.isPermitted(anyString())).willReturn(true);

		Response response = classUnderTest.updateInvoiceQueue(INVOICE_ID, FOR_DELIVERY);
		assertEquals(200, response.getStatus());
	}

	@Test
	public void updateInvoiceQueueNotFound() {
		given(invoiceService.getInvoice(anyString())).willThrow(NotFoundException.class);

		Response response = classUnderTest.updateInvoiceQueue(INVOICE_ID, FOR_DELIVERY);

		assertEquals(404, response.getStatus());
	}

	@Test
	public void updateInvoiceQueueNotPermitted() {
		Invoice invoice = givenThatInvoiceCreatedWithBranchId();
		given(invoiceService.getInvoice(anyString())).willReturn(invoice);
		given(subject.isPermitted(anyString())).willReturn(false);

		Response response = classUnderTest.updateInvoiceQueue(INVOICE_ID, FOR_DELIVERY);
		assertEquals(401, response.getStatus());
	}

	@Test
	public void updateInvoiceQueuehasNoBranch() {
		given(invoiceService.getInvoice(anyString())).willReturn(new Invoice());
		Response response = classUnderTest.updateInvoiceQueue(INVOICE_ID, FOR_DELIVERY);

		assertEquals(400, response.getStatus());
	}

	@Test
	public void updateInvoiceQueueSomethingWentWrong() {
		given(invoiceService.getInvoice(anyString())).willThrow(RuntimeException.class);
		assertThrows(ServerException.class, () -> classUnderTest.updateInvoiceQueue(INVOICE_ID, FOR_DELIVERY));
	}

	@Test
	public void deleteSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.delete(INVOICE_ID, BRANCH_ID);

		assertEquals(200, response.getStatus());
	}

	@Test
	public void deleteFailedNotPermitted() {
		Response response = classUnderTest.delete(INVOICE_ID, BRANCH_ID);

		assertEquals(401, response.getStatus());
	}

	@Test
	public void deleteFailedNotFound() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(NotFoundException.class).given(invoiceService).remove(anyString());
		Response response = classUnderTest.delete(INVOICE_ID, BRANCH_ID);

		assertEquals(404, response.getStatus());
	}

	@Test
	public void deleteSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(invoiceService).remove(anyString());
		assertThrows(ServerException.class, () -> classUnderTest.delete(INVOICE_ID, BRANCH_ID));
	}

	@Test
	public void voidSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.voidInvoice(INVOICE_ID, BRANCH_ID);

		assertEquals(200, response.getStatus());
	}

	@Test
	public void voidFailedNotPermitted() {
		Response response = classUnderTest.delete(INVOICE_ID, BRANCH_ID);

		assertEquals(401, response.getStatus());
	}

	@Test
	public void voidFailedNotFound() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(NotFoundException.class).given(invoiceService).remove(anyString());
		Response response = classUnderTest.delete(INVOICE_ID, BRANCH_ID);

		assertEquals(404, response.getStatus());
	}

	@Test
	public void voidSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(invoiceService).remove(anyString());
		assertThrows(ServerException.class, () -> classUnderTest.delete(INVOICE_ID, BRANCH_ID));
	}

	@Test
	public void updatePaymentDetailsSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.updatePaymentDetails(BRANCH_ID, INVOICE_ID, new PaymentDetails());

		assertEquals(200, response.getStatus());
	}

	@Test
	public void updatePaymentDetailsFailedNotPermitted() {
		Response response = classUnderTest.updatePaymentDetails(BRANCH_ID, INVOICE_ID, new PaymentDetails());

		assertEquals(401, response.getStatus());
	}

	@Test
	public void updatePaymentDetailsFailedBadRequest() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(BadRequestException.class).given(invoiceService).updatePaymentDetails(anyString(), any(PaymentDetails.class));

		Response response = classUnderTest.updatePaymentDetails(BRANCH_ID, INVOICE_ID, new PaymentDetails());

		assertEquals(400, response.getStatus());
	}

	@Test
	public void updatePaymentDetailsSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(invoiceService).updatePaymentDetails(anyString(), any(PaymentDetails.class));
		assertThrows(ServerException.class, () -> classUnderTest.updatePaymentDetails(BRANCH_ID, INVOICE_ID, new PaymentDetails()));
	}

	@Test
	public void updateRiderDetailsSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.updateRiderDetails(BRANCH_ID, INVOICE_ID, new Invoice.RiderDetails());
		assertEquals(200, response.getStatus());
	}

	@Test
	public void updateRiderDetailsNotPermitted() {
		Response response = classUnderTest.updateRiderDetails(BRANCH_ID, INVOICE_ID, new Invoice.RiderDetails());
		assertEquals(401, response.getStatus());
	}

	@Test
	public void updateRiderDetailsBadRequest() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(BadRequestException.class).given(invoiceService).updateRiderDetails(anyString(), any(Invoice.RiderDetails.class));
		Response response = classUnderTest.updateRiderDetails(BRANCH_ID, INVOICE_ID, new Invoice.RiderDetails());
		assertEquals(400, response.getStatus());
	}

	@Test
	public void updateRiderDetailsSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(invoiceService).updateRiderDetails(anyString(), any(Invoice.RiderDetails.class));
		assertThrows(ServerException.class, () -> classUnderTest.updateRiderDetails(BRANCH_ID, INVOICE_ID, new Invoice.RiderDetails()));
	}

	@Test
	public void getDeliveriesDashboardSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.getDeliveriesDashboard(BRANCH_ID);
		assertEquals(200, response.getStatus());
	}

	@Test
	public void getDeliveriesDashboardNotPermitted() {
		given(subject.isPermitted(anyString())).willReturn(false);
		Response response = classUnderTest.getDeliveriesDashboard(BRANCH_ID);
		assertEquals(401, response.getStatus());
	}

	@Test
	public void getDeliveriesDashboardInternalError() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(invoiceService.getInvoiceDeliveriesForDashboard(anyString())).willThrow(RuntimeException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getDeliveriesDashboard(BRANCH_ID));
	}
}
