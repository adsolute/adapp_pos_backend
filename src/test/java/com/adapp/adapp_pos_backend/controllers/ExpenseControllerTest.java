package com.adapp.adapp_pos_backend.controllers;


import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.services.ExpenseService;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.expenses.Expense;
import com.mongodb.MongoException;
import com.mongodb.MongoTimeoutException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ExpenseControllerTest {
    public static final String BRANCH_ID = "branchId";
    public static final String EXPENSE_ID = "expense Id";

    @InjectMocks
    ExpenseController classUnderTest;

    @Mock
    ExpenseService expenseService;

    @Mock
    static Subject subject;

    @BeforeEach
    public void setup() {
        SubjectThreadState threadState = new SubjectThreadState(subject);
        threadState.bind();
    }

    @Test
    public void getAllExpensesSucceed() {
        given(subject.isPermitted(anyString())).willReturn(true);
        given(expenseService.getAllExpenses()).willReturn(Arrays.asList(new Expense()));
        Response response = classUnderTest.getAllExpenses();
        List<Expense> expenses = (List<Expense>) response.getEntity();
        verify(expenseService, times(1)).getAllExpenses();
        assertTrue(response.getStatus() == 200);
        assertTrue(expenses.size() > 0);
    }

    @Test
    public void getAllExpensesSucceedWithEmptyList() {
        given(subject.isPermitted(anyString())).willReturn(true);
        given(expenseService.getAllExpenses()).willReturn(Arrays.asList());
        Response response = classUnderTest.getAllExpenses();
        List<Expense> expenses = (List<Expense>) response.getEntity();
        verify(expenseService, times(1)).getAllExpenses();
        assertTrue(response.getStatus() == 200);
        assertTrue(expenses.isEmpty());
    }

    @Test
    public void getAllExpensesFailedNotPermitted() {
        Response response = classUnderTest.getAllExpenses();
        assertEquals(401, response.getStatus());
    }

    @Test
    public void getAllExpensesFailed() {
        given(subject.isPermitted(anyString())).willReturn(true);
        given(expenseService.getAllExpenses()).willThrow(new MongoException("Connection Error."));
        assertThrows(ServerException.class, () -> classUnderTest.getAllExpenses());
    }

    @Test
    public void getExpensesIdSucceed() {
        given(subject.isPermitted(anyString())).willReturn(true);
        classUnderTest.getExpense(EXPENSE_ID, BRANCH_ID);
        verify(expenseService, times(1)).getExpense(anyString());
    }

    @Test
    public void saveThatSucceed() {
        given(subject.isPermitted(anyString())).willReturn(true);
        Response response = classUnderTest.save(new Expense(), BRANCH_ID);
        assertEquals(200, response.getStatus());
        verify(expenseService, times(1)).save(any(Expense.class));
    }

    @Test
    public void saveFailedNotPermitted() {
        Response response = classUnderTest.save(new Expense(), BRANCH_ID);
        assertEquals(401, response.getStatus());
    }

    @Test
    public void saveFailed() {
        given(subject.isPermitted(anyString())).willReturn(true);
        willThrow(RuntimeException.class).given(expenseService).save(any(Expense.class));
        assertThrows(ServerException.class, () -> classUnderTest.save(new Expense(), BRANCH_ID));
    }

    @Test
    public void saveFailedBadRequest() {
        given(subject.isPermitted(anyString())).willReturn(true);
        willThrow(BadRequestException.class).given(expenseService).save(any(Expense.class));
        classUnderTest.save(new Expense(), BRANCH_ID);
        Response response = classUnderTest.save(new Expense(), BRANCH_ID);
        assertEquals(400, response.getStatus());
    }

    @Test
    public void getExpensesByQuerySucceed() {
        PagedResultSet<Expense> retrievedPagedResult = new PagedResultSet<>();

        given(subject.isPermitted(anyString())).willReturn(true);
        givenThatExpenseAreRetrieved(retrievedPagedResult);
        given(expenseService.getExpensesBy(any(QueryParameters.class))).willReturn(retrievedPagedResult);
        Response response = classUnderTest.getExpensesBy(BRANCH_ID, new QueryParameters());
        PagedResultSet<Expense> expensePagedResultSet = (PagedResultSet<Expense>) response.getEntity();

        assertEquals(200, response.getStatus());
        assertNotNull(expensePagedResultSet);
        assertFalse(expensePagedResultSet.getResults().isEmpty());
        verify(expenseService, times(1)).getExpensesBy(any(QueryParameters.class));
    }

    private  void givenThatExpenseAreRetrieved(PagedResultSet<Expense> retrievedPageResult) {
        retrievedPageResult.setResults(new ArrayList<>());
        retrievedPageResult.getResults().add(new Expense());
    }

    @Test
    public void getExpensesByQueryFailedNotPermitted() {
        Response response = classUnderTest.getExpensesBy(BRANCH_ID, new QueryParameters());
        assertEquals(401, response.getStatus());
    }

    @Test
    public void getExpensesbyQueryFailed() {
        given(subject.isPermitted(anyString())).willReturn(true);
        given(expenseService.getExpensesBy(any(QueryParameters.class))).willThrow(MongoTimeoutException.class);
        assertThrows(ServerException.class, () -> classUnderTest.getExpensesBy(BRANCH_ID, new QueryParameters()));
    }

    @Test
    public void updateExpense() {
        ArgumentCaptor<Expense> expenseArgumentCaptor = ArgumentCaptor.forClass(Expense.class);
        Expense expense = givenThatExpenseIsForUpdate();
        given(subject.isPermitted(anyString())).willReturn(true);
        Response response = classUnderTest.update(expense);

        verify(expenseService, times(1)).update(expenseArgumentCaptor.capture());
        assertNotNull(expenseArgumentCaptor.getValue().getId());
        assertNotNull(expenseArgumentCaptor.getValue().getAmount());
        assertEquals(200,response.getStatus());
    }

    @Test
    public void updateExpenseIsNotPermitted() {
        Expense expense = givenThatExpenseIsForUpdate();
        Response response = classUnderTest.update(expense);
        assertEquals(401, response.getStatus());
    }

    private Expense givenThatExpenseIsForUpdate() {
        Expense expense = new Expense();
        expense.setId("1");
        expense.setExpenseId("1");
        expense.setAmount(new BigDecimal(1));
        return expense;
    }

    @Test
    public void updateExpenseBadRequest() {
        Expense expense = givenThatExpenseIsForUpdate();
        given(subject.isPermitted(anyString())).willReturn(true);
        willThrow(BadRequestException.class).given(expenseService).update(any(Expense.class));
        Response response = classUnderTest.update(expense);
        assertEquals(400,response.getStatus());
    }

    @Test
    public void deleteSucceed() {
        given(subject.isPermitted(anyString())).willReturn(true);
        Response response = classUnderTest.delete(EXPENSE_ID, BRANCH_ID);
        assertEquals(200, response.getStatus());
    }

    @Test
    public void deleteFailedNotPermitted() {
        Response response = classUnderTest.delete(EXPENSE_ID, BRANCH_ID);
        assertEquals(401, response.getStatus());
    }

    @Test
    public void deleteFailedNotFound() {
        given(subject.isPermitted(anyString())).willReturn(true);
        willThrow(NotFoundException.class).given(expenseService).remove(anyString());
        Response response = classUnderTest.delete(EXPENSE_ID, BRANCH_ID);
        assertEquals(404, response.getStatus());
    }

    @Test
    public void deleteSomethingWentWrong() {
        given(subject.isPermitted(anyString())).willReturn(true);
        willThrow(RuntimeException.class).given(expenseService).remove(anyString());
        assertThrows(ServerException.class, () -> classUnderTest.delete(EXPENSE_ID, BRANCH_ID));
    }
}
