package com.adapp.adapp_pos_backend.controllers;

import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.services.BranchService;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.profiles.Branch;
import com.adapp.adapp_pos_models.profiles.BranchDTO;
import com.mongodb.MongoTimeoutException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BranchControllerTest {

	public static final String CONNECTION_ERROR = "Connection Error";
	public static final String BRANCH_ID = "branchId";
	public static final String FIELD = "field";
	@InjectMocks
	BranchController classUnderTest;

	@Mock
	BranchService branchService;

	@Mock
	Subject subject;

	@BeforeEach
	public void setup() {
		SubjectThreadState subjectThreadState = new SubjectThreadState(subject);
		subjectThreadState.bind();
	}

	@Test
	public void getAllBranchesSucceed() {
		List<BranchDTO> convertedBranchDTO = new ArrayList<>();

		givenThatThereAreSavedBranches(convertedBranchDTO);
		when(branchService.getAllBranches()).thenReturn(convertedBranchDTO);
		Response response = classUnderTest.getAllBranches();
		List<Branch> retrievedBranches = (List<Branch>) response.getEntity();
		assertEquals(200, response.getStatus());
		assertTrue(!retrievedBranches.isEmpty());
		verify(branchService, times(1)).getAllBranches();
	}

	private void givenThatThereAreSavedBranches(List<BranchDTO> convertedBranchDTO) {
		convertedBranchDTO.add(new BranchDTO());
	}

	@Test
	public void getAllBranchSucceedWithEmptyList() {
		Response response = classUnderTest.getAllBranches();
		List<BranchDTO> retrievedBranches = (List<BranchDTO>) response.getEntity();
		assertTrue(retrievedBranches.isEmpty());
		verify(branchService, times(1)).getAllBranches();
	}

	@Test
	public void getAllBranchFailed() {
		willThrow(new MongoTimeoutException(CONNECTION_ERROR)).given(branchService).getAllBranches();
		assertThrows(ServerException.class, () -> classUnderTest.getAllBranches());
	}

	@Test
	public void saveSucceed() {
		Response response = classUnderTest.save(new Branch());
		assertEquals(200, response.getStatus());
		verify(branchService, times(1)).save(any(Branch.class));
	}

	@Test
	public void saveFailed() {
		willThrow(new MongoTimeoutException(CONNECTION_ERROR)).given(branchService).save(any(Branch.class));
		assertThrows(ServerException.class, () -> classUnderTest.save(new Branch()));
	}

	@Test
	public void getBranchDTOPagedResultSetSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		PagedResultSet<BranchDTO> branchDTOPagedResultSet = new PagedResultSet<>();
		givenThatThereAreSavedBranches(branchDTOPagedResultSet.getResults());
		given(branchService.getBranchDTOPagedResultSetBy(any(QueryParameters.class))).willReturn(branchDTOPagedResultSet);

		Response response = classUnderTest.getBranchDTOPagedResultSetBy(BRANCH_ID, new QueryParameters());
		PagedResultSet<BranchDTO> retrievedBranchDTOPagedResultSet = (PagedResultSet<BranchDTO>) response.getEntity();

		verify(branchService, times(1)).getBranchDTOPagedResultSetBy(any(QueryParameters.class));
		assertEquals(200, response.getStatus());
		assertNotNull(retrievedBranchDTOPagedResultSet);
		assertFalse(retrievedBranchDTOPagedResultSet.getResults().isEmpty());
	}

	@Test
	public void getBranchDTOPagedResultSetSucceedWithEmptyResults() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(branchService.getBranchDTOPagedResultSetBy(any(QueryParameters.class))).willReturn(new PagedResultSet<>());

		Response response = classUnderTest.getBranchDTOPagedResultSetBy(BRANCH_ID, new QueryParameters());
		PagedResultSet<BranchDTO> retrievedBranchDTOPagedResultSet = (PagedResultSet<BranchDTO>) response.getEntity();

		verify(branchService, times(1)).getBranchDTOPagedResultSetBy(any(QueryParameters.class));
		assertEquals(200, response.getStatus());
		assertNotNull(retrievedBranchDTOPagedResultSet);
		assertTrue(retrievedBranchDTOPagedResultSet.getResults().isEmpty());
	}

	@Test
	public void getBranchDTOPagedResultSetFailedNotPermitted() {
		given(subject.isPermitted(anyString())).willReturn(false);

		Response response = classUnderTest.getBranchDTOPagedResultSetBy(BRANCH_ID, new QueryParameters());

		verify(branchService, never()).getBranchDTOPagedResultSetBy(any(QueryParameters.class));
		assertEquals(401, response.getStatus());
	}

	@Test
	public void getBranchDTOPagedResultSetFailed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(branchService.getBranchDTOPagedResultSetBy(any(QueryParameters.class))).willThrow(NullPointerException.class);

		assertThrows(ServerException.class, () -> classUnderTest.getBranchDTOPagedResultSetBy(BRANCH_ID, new QueryParameters()));
	}

	@Test
	public void getBranchDTOListSucceed() {
		List<BranchDTO> branchDTOList = new ArrayList<>();
		givenThatThereAreSavedBranches(branchDTOList);
		given(branchService.getBranchDTOList(any(QueryParameters.class))).willReturn(branchDTOList);
		Response response = classUnderTest.getBranchDTOList(FIELD, new ArrayList<>());
		List<BranchDTO> retrievedBranchDTOList = (List<BranchDTO>) response.getEntity();

		assertEquals(200, response.getStatus());
		assertNotNull(retrievedBranchDTOList);
		assertFalse(retrievedBranchDTOList.isEmpty());
		verify(branchService, times(1)).getBranchDTOList(any(QueryParameters.class));
	}

	@Test
	public void getBranchDTOListSucceedWithEmptyList() {
		Response response = classUnderTest.getBranchDTOList(FIELD, new ArrayList<>());
		List<BranchDTO> retrievedBranchDTOList = (List<BranchDTO>) response.getEntity();

		assertEquals(200, response.getStatus());
		assertNotNull(retrievedBranchDTOList);
		assertTrue(retrievedBranchDTOList.isEmpty());
		verify(branchService, times(1)).getBranchDTOList(any(QueryParameters.class));
	}

	@Test
	public void getBranchDTOListFailed() {
		given(branchService.getBranchDTOList(any(QueryParameters.class))).willThrow(NullPointerException.class);
		assertThrows(ServerException.class, ()-> classUnderTest.getBranchDTOList(FIELD, new ArrayList<>()));
	}

	@Test
	public void getBranchSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(branchService.getBranch(anyString())).willReturn(new Branch());
		Response response = classUnderTest.getBranch(BRANCH_ID, BRANCH_ID);
		assertEquals(200, response.getStatus());
	}

	@Test
	public void getBranchFailedNotPermitted() {
		Response response = classUnderTest.getBranch(BRANCH_ID, BRANCH_ID);
		assertEquals(401, response.getStatus());
	}

	@Test
	public void getBranchFailedNotFound() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.getBranch(BRANCH_ID, BRANCH_ID);
		assertEquals(404, response.getStatus());
	}

	@Test
	public void getBranchFailedInvalidObjectId() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(branchService.getBranch(anyString())).willThrow(IllegalArgumentException.class);

		Response response = classUnderTest.getBranch(BRANCH_ID, BRANCH_ID);

		assertEquals(400, response.getStatus());
	}

	@Test
	public void getBranchSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(branchService.getBranch(anyString())).willThrow(RuntimeException.class);

		assertThrows(ServerException.class, () -> classUnderTest.getBranch(BRANCH_ID, BRANCH_ID));
	}

	@Test
	public void getBranchByBranchIdSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(branchService.getBranchByBranchId(anyString())).willReturn(new Branch());
		Response response = classUnderTest.getBranch(BRANCH_ID);
		assertEquals(200, response.getStatus());
	}

	@Test
	public void getBranchByBranchIdNotFound() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.getBranch(BRANCH_ID);
		assertEquals(404, response.getStatus());
	}

	@Test
	public void getBranchByBranchIdNotPermitted() {
		given(subject.isPermitted(anyString())).willReturn(false);
		Response response = classUnderTest.getBranch(BRANCH_ID);
		assertEquals(401, response.getStatus());
	}

	@Test
	public void getBranchByBranchIdSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(branchService.getBranchByBranchId(anyString())).willThrow(RuntimeException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getBranch(BRANCH_ID));
	}

	@Test
	public void updateSucceed() {
		Branch branchToUpdate = new Branch();
		givenThatBranchToUpdateHasObjectId(branchToUpdate);
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.update(BRANCH_ID, branchToUpdate);
		assertEquals(200, response.getStatus());
	}

	private void givenThatBranchToUpdateHasObjectId(Branch branch) {
		branch.setId("id");
	}

	@Test
	public void updateFailedInvalidBranch() {
		Response response = classUnderTest.update(BRANCH_ID, new Branch());

		assertEquals(400, response.getStatus());
	}

	@Test
	public void updateFailedNotPermitted() {
		Branch branchToUpdate = new Branch();
		givenThatBranchToUpdateHasObjectId(branchToUpdate);

		Response response = classUnderTest.update(BRANCH_ID, branchToUpdate);

		assertEquals(401, response.getStatus());
	}

	@Test
	public void updateSomethingWentWrong() {
		Branch branchToUpdate = new Branch();
		givenThatBranchToUpdateHasObjectId(branchToUpdate);
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(branchService).update(any(Branch.class));
		assertThrows(ServerException.class, () -> classUnderTest.update(BRANCH_ID, branchToUpdate));
	}
	@Test
	public void deleteSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.delete("branchIdToBeDeleted", BRANCH_ID);
		assertEquals(200, response.getStatus());
	}

	@Test
	public void deleteFailedNotPermitted() {
		Response response = classUnderTest.delete("branchIdToBeDeleted", BRANCH_ID);

		assertEquals(401, response.getStatus());
	}

	@Test
	public void deleteFailedNotFound() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(NotFoundException.class).given(branchService).remove(anyString());
		Response response = classUnderTest.delete("branchIdToBeDeleted", BRANCH_ID);

		assertEquals(404, response.getStatus());
	}

	@Test
	public void deleteSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(branchService).remove(anyString());
		assertThrows(ServerException.class, () -> classUnderTest.delete("branchIdToBeDeleted", BRANCH_ID));
	}

}