package com.adapp.adapp_pos_backend.controllers;

import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.services.ReportService;
import com.adapp.adapp_pos_models.enums.DocType;
import com.adapp.adapp_pos_models.enums.ReportType;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.reports.ReportRequest;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import static com.adapp.adapp_pos_models.enums.DocType.PDF;
import static com.adapp.adapp_pos_models.enums.ReportType.INVOICE;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ReportControllerTest {

	public static final String DOCUMENT_DATA_ID = "documentDataId";
	public static final String BRANCH_ID = "branchId";
	@InjectMocks
	ReportController classUnderTest;

	@Mock
	ReportService reportService;

	static Subject subject;

	@Mock
	UriInfo uriInfo;

	@Mock
	UriBuilder uriBuilder;

	@BeforeAll
	public static void setup() {
		subject = mock(Subject.class);
		SubjectThreadState threadState = new SubjectThreadState(subject);
		threadState.bind();
	}

	@Test
	public void saveDocumentDataSucceed() {
		given(reportService.saveDocumentData(any(ReportRequest.class), any(ReportType.class))).willReturn("documentDataId");
		given(uriInfo.getBaseUriBuilder()).willReturn(uriBuilder);
		given(uriBuilder.path(anyString())).willReturn(uriBuilder);
		given(uriInfo.getPath()).willReturn("path");

		Response response = classUnderTest.saveDocumentData(uriInfo, INVOICE, new ReportRequest());

		assertEquals(200, response.getStatus());
	}

	@Test
	public void saveDocumentDataFailed() {
		UriInfo uriInfo = Mockito.mock(UriInfo.class);

		assertThrows(ServerException.class, () -> classUnderTest.saveDocumentData(uriInfo, INVOICE, new ReportRequest()));
	}

	@Test
	public void generateReportDocumentSucceed() {
		  Response response = classUnderTest.generateReportDocument(DOCUMENT_DATA_ID, PDF);

		  assertEquals(200, response.getStatus());
		  assertNotNull(response.getEntity());
	}

	@Test
	public void generateReportDocumentTemplateNotFound() {
		given(reportService.generateReportByteArray(anyString(), any(DocType.class))).willThrow(NotFoundException.class);

		Response response = classUnderTest.generateReportDocument(DOCUMENT_DATA_ID, PDF);

		assertEquals(404, response.getStatus());
	}

	@Test
	public void generateReportDocumentFailed() {
		given(reportService.generateReportByteArray(anyString(), any(DocType.class))).willThrow(InternalServerErrorException.class);

		assertThrows(ServerException.class, () -> classUnderTest.generateReportDocument(DOCUMENT_DATA_ID, PDF));
	}

	@Test
	public void getInvoiceSalesReportSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.getInvoiceSalesReport(new QueryParameters(), BRANCH_ID);
		assertEquals(200, response.getStatus());
		verify(reportService, times(1)).getInvoiceSalesReport(any(QueryParameters.class));
	}

	@Test
	public void getInvoiceSalesReportNotPermitted() {
		given(subject.isPermitted(anyString())).willReturn(false);
		Response response = classUnderTest.getInvoiceSalesReport(new QueryParameters(), BRANCH_ID);
		assertEquals(401, response.getStatus());
	}

	@Test
	public void getInvoiceSalesReportFailed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(reportService.getInvoiceSalesReport(any(QueryParameters.class))).willThrow(RuntimeException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getInvoiceSalesReport(new QueryParameters(), BRANCH_ID));
	}

	@Test
	public void saveDailySalesReportDocumentDataSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(reportService.saveDailySalesReportDocumentData(any(QueryParameters.class))).willReturn("documentDataId");
		given(uriInfo.getBaseUriBuilder()).willReturn(uriBuilder);
		given(uriBuilder.path(anyString())).willReturn(uriBuilder);

		Response response = classUnderTest.saveDailySalesReportDocumentData(uriInfo, new QueryParameters(), BRANCH_ID);
		assertEquals(200, response.getStatus());
	}

	@Test
	public void saveDailySalesReportDocumentDataNotPermitted() {
		given(subject.isPermitted(anyString())).willReturn(false);

		Response response = classUnderTest.saveDailySalesReportDocumentData(uriInfo, new QueryParameters(), BRANCH_ID);
		assertEquals(401, response.getStatus());
	}

	@Test
	public void saveDailySalesReportDocumentDataInternalError() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(reportService.saveDailySalesReportDocumentData(any(QueryParameters.class))).willThrow(RuntimeException.class);

		assertThrows(ServerException.class, () -> classUnderTest.saveDailySalesReportDocumentData(uriInfo, new QueryParameters(), BRANCH_ID));
	}
}