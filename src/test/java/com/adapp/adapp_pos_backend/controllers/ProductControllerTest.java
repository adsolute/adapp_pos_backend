package com.adapp.adapp_pos_backend.controllers;

import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.services.ProductService;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.adapp_pos_models.products.Product;
import com.mongodb.MongoException;
import com.mongodb.MongoTimeoutException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductControllerTest {

	public static final String PRODUCT_NAME = "Product Name";
	public static final String BRANCH_ID = "branchId";
	public static final String PRODUCT_ID = "product id";
	@InjectMocks
	ProductController classUnderTest;

	@Mock
	ProductService productService;

	static Subject subject;

	@BeforeAll
	public static void setup() {
		subject = mock(Subject.class);
		SubjectThreadState threadState = new SubjectThreadState(subject);
		threadState.bind();
	}

	@Test
	public void getAllProductsSucceed() {
		given(productService.getAllProducts()).willReturn(Arrays.asList(new Product()));
		Response response = classUnderTest.getAllProducts();
		List<Product> products = (List<Product>) response.getEntity();
		verify(productService, times(1)).getAllProducts();
		assertTrue(response.getStatus() == 200);
		assertTrue(products.size() > 0);
	}

	@Test
	public void getAllProductsSucceedWithEmptyList() {
		given(productService.getAllProducts()).willReturn(Arrays.asList());
		Response response = classUnderTest.getAllProducts();
		List<Product> products = (List<Product>) response.getEntity();
		verify(productService, times(1)).getAllProducts();
		assertTrue(response.getStatus() == 200);
		assertTrue(products.isEmpty());
	}

	@Test
	public void getAllProductsFailed() {
		given(productService.getAllProducts()).willThrow(new MongoException("Connection Error"));
		assertThrows(ServerException.class, () -> classUnderTest.getAllProducts());
	}

	@Test
	public void saveSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.save(BRANCH_ID,new Product());
		verify(productService, times(1)).save(any(Product.class));
		assertTrue(response.getStatus() == 200);
	}

	@Test
	public void saveFailedNotPermitted() {
		given(subject.isPermitted(anyString())).willReturn(false);

		Response response = classUnderTest.save(BRANCH_ID,new Product());

		assertEquals(401, response.getStatus());
	}

	@Test
	public void saveFailed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(productService).save(any(Product.class));
		assertThrows(ServerException.class, () -> classUnderTest.save(BRANCH_ID, new Product()));
	}

	@Test
	public void getProductsByNameSucceed() {
		List<Product> retrievedProducts = new ArrayList<>();
		givenProductsHaveSameName(retrievedProducts);
		given(productService.getProducts(anyString())).willReturn(retrievedProducts);
		Response response = classUnderTest.getProducts(PRODUCT_NAME);
		List<Product> products = (List<Product>) response.getEntity();
		assertEquals(200, response.getStatus());
		assertTrue(!products.isEmpty());
		assertTrue(products.get(0).getName().contains(PRODUCT_NAME));
		verify(productService, times(1)).getProducts(anyString());
	}

	private void givenProductsHaveSameName(List<Product> retrievedProducts) {
		Product product = new Product();
		product.setName("Product Name of this Product");
		retrievedProducts.add(product);
	}

	@Test
	public void getProductsByNameSucceedWithEmptyList() {
		Response response = classUnderTest.getProducts(PRODUCT_NAME);
		List<Product> products = (List<Product>) response.getEntity();
		assertEquals(200, response.getStatus());
		assertTrue(products.isEmpty());
		verify(productService, times(1)).getProducts(anyString());
	}

	@Test
	public void getProductsByNameFailed() {
		given(productService.getProducts(anyString())).willThrow(MongoTimeoutException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getProducts(PRODUCT_NAME));
	}

	@Test
	public void getProductsByQuerySucceed() {
		PagedResultSet<Product> retrievedPagedResult = new PagedResultSet<>();

		given(subject.isPermitted(anyString())).willReturn(true);
		givenThatProductsAreRetrieved(retrievedPagedResult);
		given(productService.getProductsBy(any(QueryParameters.class))).willReturn(retrievedPagedResult);
		Response response = classUnderTest.getProductsBy(BRANCH_ID, new QueryParameters());
		PagedResultSet<Product> productPagedResultSet = (PagedResultSet<Product>) response.getEntity();

		assertEquals(200, response.getStatus());
		assertNotNull(productPagedResultSet);
		assertFalse(productPagedResultSet.getResults().isEmpty());
		verify(productService, times(1)).getProductsBy(any(QueryParameters.class));
	}

	private void givenThatProductsAreRetrieved(PagedResultSet<Product> retrievedPagedResult) {
		retrievedPagedResult.setResults(new ArrayList<>());
		retrievedPagedResult.getResults().add(new Product());
	}

	@Test
	public void getProductsByQuerySucceedWithEmptyResults() {
		PagedResultSet retrievedPagedResult = new PagedResultSet();

		given(subject.isPermitted(anyString())).willReturn(true);
		givenThatPagedResultsIsEmpty(retrievedPagedResult);
		given(productService.getProductsBy(any(QueryParameters.class))).willReturn(retrievedPagedResult);
		Response response = classUnderTest.getProductsBy(BRANCH_ID, new QueryParameters());
		PagedResultSet<Product> productPagedResultSet = (PagedResultSet<Product>) response.getEntity();

		assertEquals(200, response.getStatus());
		assertNotNull(productPagedResultSet);
		assertTrue(productPagedResultSet.getResults().isEmpty());
		verify(productService, times(1)).getProductsBy(any(QueryParameters.class));
	}

	private void givenThatPagedResultsIsEmpty(PagedResultSet retrievedPagedResults) {
		retrievedPagedResults.setResults(new ArrayList());
	}

	@Test
	public void getProductsByQueryFailedNotPermitted() {
		given(subject.isPermitted(anyString())).willReturn(false);
		Response response = classUnderTest.getProductsBy(BRANCH_ID, new QueryParameters());

		assertEquals(401, response.getStatus());
	}

	@Test
	public void getProductsByQueryFailed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(productService.getProductsBy(any(QueryParameters.class))).willThrow(MongoTimeoutException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getProductsBy(BRANCH_ID, new QueryParameters()));
	}

	@Test
	public void updateSucceed() {
		Product product = givenThatProductIsForUpdate();
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.update(BRANCH_ID, product);

		assertEquals(200, response.getStatus());
	}

	@Test
	public void updateFailedIdIsNull() {
		Response response = classUnderTest.update(BRANCH_ID, new Product());

		assertEquals(400, response.getStatus());
	}

	@Test
	public void updateFailedNotPermitted() {
		given(subject.isPermitted(anyString())).willReturn(false);
		Response response = classUnderTest.update(BRANCH_ID, givenThatProductIsForUpdate());

		assertEquals(401, response.getStatus());
	}

	@Test
	public void updateFailedSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(productService).update(any(Product.class));
		assertThrows(ServerException.class, () -> classUnderTest.update(BRANCH_ID, givenThatProductIsForUpdate()));
	}

	private Product givenThatProductIsForUpdate() {
		Product product = new Product();
		product.setId(PRODUCT_ID);
		return product;
	}

	@Test
	public void getProductSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(productService.getProduct(anyString())).willReturn(new Product());

		Response response = classUnderTest.getProduct(PRODUCT_ID, BRANCH_ID);

		assertEquals(200, response.getStatus());
		assertNotNull(response.getEntity());
		verify(productService, times(1)).getProduct(anyString());
	}

	@Test
	public void getProductSucceedWithNullResult() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(productService.getProduct(anyString())).willReturn(null);

		Response response = classUnderTest.getProduct(PRODUCT_ID, BRANCH_ID);

		assertEquals(404, response.getStatus());
		assertTrue(response.getEntity().toString().contains(PRODUCT_ID));
		verify(productService, times(1)).getProduct(anyString());
	}

	@Test
	public void getProductFailedNotPermitted() {
		Response response = classUnderTest.getProduct(PRODUCT_ID, BRANCH_ID);

		assertEquals(401, response.getStatus());
		assertTrue(response.getEntity().toString().contains(BRANCH_ID));
	}

	@Test
	public void getProductFailedSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(productService.getProduct(anyString())).willThrow(RuntimeException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getProduct(PRODUCT_ID, BRANCH_ID));
	}

	@Test
	public void deleteSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.delete(PRODUCT_ID, BRANCH_ID);
		assertEquals(200, response.getStatus());
	}

	@Test
	public void deleteFailedNotPermitted() {
		given(subject.isPermitted(anyString())).willReturn(false);
		Response response = classUnderTest.delete(PRODUCT_ID, BRANCH_ID);

		assertEquals(401, response.getStatus());
	}

	@Test
	public void deleteFailedNotFound() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(NotFoundException.class).given(productService).remove(anyString());
		Response response = classUnderTest.delete(PRODUCT_ID, BRANCH_ID);

		assertEquals(404, response.getStatus());
	}

	@Test
	public void deleteSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(productService).remove(anyString());
		assertThrows(ServerException.class, () -> classUnderTest.delete(PRODUCT_ID, BRANCH_ID));
	}
}