package com.adapp.adapp_pos_backend.controllers;

import com.adapp.adapp_pos_backend.dao.BaseDao.PagedResultSet;
import com.adapp.adapp_pos_backend.dao.BaseDao.QueryParameters;
import com.adapp.adapp_pos_backend.services.CustomerService;
import com.adapp.adapp_pos_models.customers.Customer;
import com.adapp.adapp_pos_models.customers.CustomerDTO;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.exceptions.ServerException;
import com.adapp.security.models.Permission;
import com.mongodb.MongoException;
import com.mongodb.MongoTimeoutException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CustomerControllerTest {

	private static final String CUSTOMER_NAME = "Customer";
	private static final String MONGO_CONNECTION_ERROR = "Connection Error";
	public static final String BRANCH_ID = "branchId";
	public static final String CUSTOMER_ID = "customer id";
	@InjectMocks
	CustomerController classUnderTest;

	@Mock
	CustomerService customerService;

	Subject subject;

	@BeforeEach
	public void setup() {
		subject = mock(Subject.class);
		SubjectThreadState subjectThreadState = new SubjectThreadState(subject);
		subjectThreadState.bind();
	}

	@Test
	public void getAllCustomersSucceedWithEmptyList() {
		given(customerService.getAllCustomers()).willReturn(Collections.emptyList());
		Response response = classUnderTest.getAllCustomers();
		assertEquals(200,response.getStatus());
		List<CustomerDTO> customerDTOList = (List<CustomerDTO>) response.getEntity();
		verify(customerService, times(1)).getAllCustomers();
		assertTrue(customerDTOList.isEmpty());
	}

	@Test
	public void getAllCustomersSucceed() {
		given(customerService.getAllCustomers()).willReturn(Arrays.asList(new CustomerDTO()));
		Response response = classUnderTest.getAllCustomers();
		assertEquals(200,response.getStatus());
		List<CustomerDTO> customerDTOList = (List<CustomerDTO>) response.getEntity();
		verify(customerService, times(1)).getAllCustomers();
		assertTrue(customerDTOList.size() > 0);
	}

	@Test
	public void getAllCustomersFailed() {
		given(customerService.getAllCustomers()).willThrow(new MongoException(MONGO_CONNECTION_ERROR));
		assertThrows(ServerException.class, () -> classUnderTest.getAllCustomers());
	}

	@Test
	public void saveSucceed(){
		Response response = classUnderTest.save(new Customer());
		verify(customerService, times(1)).save(any(Customer.class));
		assertEquals(200,response.getStatus());
	}

	@Test
	public void saveFailed() {
		willThrow(new MongoException(MONGO_CONNECTION_ERROR)).given(customerService).save(any(Customer.class));
		assertThrows(ServerException.class, () -> classUnderTest.save(new Customer()));
	}

	@Test
	public void getCustomersByNameSucceed() {
		List<Customer> retrievedCustomers = new ArrayList<>();

		givenThatCustomersHaveSameName(retrievedCustomers);
		given(customerService.getCustomers(anyString())).willReturn(retrievedCustomers);
		Response response = classUnderTest.getCustomers(CUSTOMER_NAME);
		List<Customer> customers = (List<Customer>) response.getEntity();
		verify(customerService, times(1)).getCustomers(anyString());
		assertEquals(200,response.getStatus());
		assertTrue(customers.size() > 0);
		assertTrue(customers.get(0).getName().contains(CUSTOMER_NAME));
		assertTrue(customers.get(1).getName().contains(CUSTOMER_NAME));
	}

	private void givenThatCustomersHaveSameName(List<Customer> retrievedCustomers) {
		Customer customer = new Customer();
		customer.setName("Customer 1");
		retrievedCustomers.add(customer);
		customer = new Customer();
		customer.setName("Customer A");
		retrievedCustomers.add(customer);
	}

	@Test
	public void getCustomersByNameSucceedWithEmptyList() {
		given(customerService.getCustomers(anyString())).willReturn(Collections.emptyList());
		Response response = classUnderTest.getCustomers(CUSTOMER_NAME);
		List<String> customers = (List<String>) response.getEntity();
		verify(customerService, times(1)).getCustomers(anyString());
		assertEquals(200,response.getStatus());
		assertTrue(customers.isEmpty());
	}

	@Test
	public void getCustomersByNameFailed() {
		given(customerService.getCustomers(anyString())).willThrow(new MongoTimeoutException(MONGO_CONNECTION_ERROR));
		assertThrows(ServerException.class, () -> classUnderTest.getCustomers(CUSTOMER_NAME));
	}

	@Test
	public void getCustomerDTOResultSetByQuerySucceed() {
		PagedResultSet retrievedPagedResultSet = new PagedResultSet();
		given(subject.isPermitted(anyString())).willReturn(true);
		givenThatCustomersWereRetrieved(retrievedPagedResultSet);
		given(customerService.getCustomerDTOResultSetBy(any(QueryParameters.class))).willReturn(retrievedPagedResultSet);

		Response response = classUnderTest.getCustomerDTOResultSetBy(BRANCH_ID, new QueryParameters());
		PagedResultSet<CustomerDTO> customerDTOPagedResultSet = (PagedResultSet<CustomerDTO>) response.getEntity();

		assertEquals(200, response.getStatus());
		assertNotNull(customerDTOPagedResultSet);
		assertFalse(customerDTOPagedResultSet.getResults().isEmpty());
		verify(customerService, times(1)).getCustomerDTOResultSetBy(any(QueryParameters.class));
	}

	private void givenThatCustomersWereRetrieved(PagedResultSet retrievedPagedResultSet) {
		retrievedPagedResultSet.setResults(new ArrayList());
		retrievedPagedResultSet.getResults().add(new CustomerDTO());
	}

	@Test
	public void getCustomerDTOResultSetByQuerySucceedWithEmptyResults() {
		given(subject.isPermitted(anyString())).willReturn(true);
		PagedResultSet emptyResultSet = new PagedResultSet();
		given(customerService.getCustomerDTOResultSetBy(any(QueryParameters.class))).willReturn(emptyResultSet);

		Response response = classUnderTest.getCustomerDTOResultSetBy(BRANCH_ID, new QueryParameters());
		PagedResultSet pagedResultSet = (PagedResultSet) response.getEntity();

		assertEquals(200, response.getStatus());
		assertTrue(pagedResultSet.getResults().isEmpty());
		verify(customerService, times(1)).getCustomerDTOResultSetBy(any(QueryParameters.class));
	}

	@Test
	public void getCustomerDTOResultSetByQueryFailedNotPermitted() {
		Response response = classUnderTest.getCustomerDTOResultSetBy(BRANCH_ID, new QueryParameters());

		assertEquals(401, response.getStatus());
	}

	@Test
	public void getCustomerDTOResultSetByQueryFailed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(customerService.getCustomerDTOResultSetBy(any(QueryParameters.class))).willThrow(NullPointerException.class);
		assertThrows(ServerException.class, () -> classUnderTest.getCustomerDTOResultSetBy(BRANCH_ID, new QueryParameters()));
	}

	@Test
	public void getCustomerSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(customerService.getCustomer(anyString())).willReturn(new Customer());
		Response response = classUnderTest.getCustomer(CUSTOMER_ID, BRANCH_ID);

		assertEquals(200, response.getStatus());
		assertNotNull(response.getEntity());
	}

	@Test
	public void getCustomerSucceedWithNullResult() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.getCustomer(CUSTOMER_ID, BRANCH_ID);

		assertEquals(404, response.getStatus());
		assertNotNull(response.getEntity());
		assertTrue(response.getEntity().toString().contains(CUSTOMER_ID));
	}

	@Test
	public void getCustomerNotPermitted() {
		Response response = classUnderTest.getCustomer(CUSTOMER_ID, BRANCH_ID);

		assertEquals(401, response.getStatus());
		assertNotNull(response.getEntity());
		assertTrue(response.getEntity().toString().contains(Resource.CUSTOMERS.toString()));
		assertTrue(response.getEntity().toString().contains(Permission.Action.READ.toString()));
		assertTrue(response.getEntity().toString().contains(BRANCH_ID));
	}

	@Test
	public void getCustomerWithInvalidObjectId() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(customerService.getCustomer(anyString())).willThrow(IllegalArgumentException.class);
		Response response = classUnderTest.getCustomer(CUSTOMER_ID, BRANCH_ID);

		assertEquals(400, response.getStatus());
		assertNotNull(response.getEntity());
		assertTrue(response.getEntity().toString().contains(CUSTOMER_ID));
	}

	@Test
	public void getCustomerFailed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		given(customerService.getCustomer(anyString())).willThrow(MongoTimeoutException.class);
		assertThrows(ServerException.class, () ->classUnderTest.getCustomer(CUSTOMER_ID, BRANCH_ID));
	}

	@Test
	public void deleteSucceed() {
		given(subject.isPermitted(anyString())).willReturn(true);
		Response response = classUnderTest.delete(CUSTOMER_ID, BRANCH_ID);
		assertEquals(200, response.getStatus());
	}

	@Test
	public void deleteFailedNotPermitted() {
		Response response = classUnderTest.delete(CUSTOMER_ID, BRANCH_ID);

		assertEquals(401, response.getStatus());
	}

	@Test
	public void deleteFailedNotFound() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(NotFoundException.class).given(customerService).remove(anyString());
		Response response = classUnderTest.delete(CUSTOMER_ID, BRANCH_ID);

		assertEquals(404, response.getStatus());
	}

	@Test
	public void deleteSomethingWentWrong() {
		given(subject.isPermitted(anyString())).willReturn(true);
		willThrow(RuntimeException.class).given(customerService).remove(anyString());
		assertThrows(ServerException.class, () -> classUnderTest.delete(CUSTOMER_ID, BRANCH_ID));
	}
}