FROM openjdk:11
MAINTAINER ssbifoodservice.com
COPY target/adapp-pos-backend-*.jar adapp-pos-backend.jar
ENTRYPOINT ["java","-jar","adapp-pos-backend.jar"]
